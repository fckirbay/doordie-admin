<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/privacy-policy', [ 'uses' => 'Theme\IndexController@getPrivacyPolicy' ]);


Route::get('/send-notify', [ 'uses' => 'Crons\SendNotificationsController@setSendNotification' ]);

Route::get('/buy-vip', [ 'uses' => 'Theme\IndexController@setBuyVip' ]);

Route::get('/buy-vip', [ 'uses' => 'Theme\IndexController@setBuyVip' ]);



// Firebase
Route::post('firebase', [ 'uses' => 'Theme\IndexController@setFirebase' ]);


Route::group(['middleware' => ['ip']], function () {
    Route::get('/postback/adgate', [ 'uses' => 'Theme\IndexController@setPostback' ]);
    Route::get('/postback/adgate_video', [ 'uses' => 'Theme\IndexController@setPostbackVideo' ]);
});

Route::group(['middleware' => ['language']], function () {
    
    Route::group(['middleware' => ['sentinel.auth']], function () {
            
            Route::get('verify', [ 'uses' => 'Theme\RegisterController@getVerify' ]);
            Route::post('verify', [ 'uses' => 'Theme\RegisterController@setVerify' ]);
            Route::post('verify-complete', [ 'uses' => 'Theme\RegisterController@setVerifyComplete' ]);
            
    });
    
    Route::group(['middleware' => ['verification']], function () {

        /* YENİLER */
            // Mainpage
            Route::get('/', [ 'uses' => 'Theme\IndexController@getIndex' ]);
            // Login
            Route::get('login', [ 'uses' => 'Theme\LoginController@getLogin' ]);
            Route::post('login', [ 'uses' => 'Theme\LoginController@setLogin' ]);
            // Signup
            Route::get('signup', [ 'uses' => 'Theme\RegisterController@getRegister' ]);
            Route::post('signup', [ 'uses' => 'Theme\RegisterController@setRegister' ]);
            //Forgot Password
            Route::get('forgot-password', [ 'uses' => 'Theme\LoginController@getForgotPassword' ]);
            Route::post('forgot-password', [ 'uses' => 'Theme\LoginController@setForgotPassword' ]);
        /* YENİLER */

        Route::get('/update', [ 'uses' => 'Theme\IndexController@getUpdate' ]);
        
        // Rewarded Video
        Route::post('click-rewarded', [ 'uses' => 'Theme\IndexController@setClickRewarded' ]);
        Route::get('watch-rewarded', [ 'uses' => 'Theme\IndexController@setWatchRewarded' ]);

        
        
        Route::get('index', [ 'uses' => 'Theme\IndexController@getIndex' ]);
        
       
        
        Route::get('giris-yap/twitter', 'Theme\LoginController@redirectToTwitter');
        Route::get('giris-yap/twitter/callback', 'Theme\LoginController@handleTwitterCallback');
        
        Route::get('giris-yap/facebook', 'Theme\LoginController@redirectToFacebook');
        Route::get('giris-yap/facebook/callback', 'Theme\LoginController@handleFacebookCallback');
        
        
        
        
        
        // My Cart
        Route::get('sepetim', [ 'uses' => 'Theme\MyCartController@getMyCart' ]);
        // Add to Cart    
        Route::post('sepete-ekle', [ 'uses' => 'Theme\MyCartController@setAddtoCart' ]);
        Route::get('sepetten-cikar/{rowId}', [ 'uses' => 'Theme\MyCartController@setRemoveCart' ]);
        Route::post('sepeti-guncelle', [ 'uses' => 'Theme\MyCartController@setUpdateCart' ]);
        
        // Checkout & Payment
        Route::get('odeme', [ 'uses' => 'Theme\CheckoutController@getCheckout' ]);
            
            
        // LOGGED USERS
        Route::group(['middleware' => ['sentinel.auth']], function () {



            /* YENİLER */
                // My Account
                Route::get('my-account', [ 'uses' => 'Theme\MyAccountController@getMyAccount' ]);
                Route::post('my-account', [ 'uses' => 'Theme\MyAccountController@setMyAccount' ]);

                // History
                Route::get('history', [ 'uses' => 'Theme\HistoryController@getHistory' ]);

                // MyReferences
                Route::get('my-references', [ 'uses' => 'Theme\MyReferencesController@getMyReferences' ]);

                // Bank
                Route::get('bank', [ 'uses' => 'Theme\BankController@getBank' ]);

                // Buy Bonus
                Route::get('buy-bonus', [ 'uses' => 'Theme\BuyBonusController@getBuyBonus' ]);

                // Play
                Route::get('play', [ 'uses' => 'Theme\PlayController@getPlay' ]);
                Route::post('play-pass', [ 'uses' => 'Theme\PlayController@setPlayPass' ]);
                Route::post('play-show-result', [ 'uses' => 'Theme\PlayController@setPlayShowResult' ]);

                // Settings
                Route::get('settings', [ 'uses' => 'Theme\SettingsController@getSettings' ]);

                // Opportuninites
                Route::get('opportunities', [ 'uses' => 'Theme\OpportunitiesController@getOpportunities' ]);

                // Prizes
                Route::get('prizes', [ 'uses' => 'Theme\PrizesController@getPrizes' ]);
                Route::get('prize/{name}', [ 'uses' => 'Theme\PrizesController@getPrize' ]);
                Route::post('prize', [ 'uses' => 'Theme\PrizesController@setPrize' ]);

                // Logout
                Route::get('logout', [ 'uses' => 'Theme\LoginController@setLogout' ]);
            /* YENİLER */


            
                

                // My Safe
                Route::get('my-messages', [ 'uses' => 'Theme\MyMessagesController@getMyMessages' ]);
                
                
                
                // My Safe
                Route::get('my-safe', [ 'uses' => 'Theme\MySafeController@getMySafe' ]);

                // Prize List
                Route::get('prize-list', [ 'uses' => 'Theme\PrizeListController@getPrizeList' ]);

                // Exchange
                Route::get('exchange', [ 'uses' => 'Theme\ExchangeController@getExchange' ]);
                Route::post('exchange', [ 'uses' => 'Theme\ExchangeController@setExchange' ]);
                
                // Transactions
                Route::get('transactions', [ 'uses' => 'Theme\TransactionsController@getTransactions' ]);
                
                // OpenBoxes
                Route::get('opened-boxes', [ 'uses' => 'Theme\OpenBoxesController@getOpenBoxes' ]);

                

                // MyOfferwall
                Route::get('my-offerwall', [ 'uses' => 'Theme\MyOfferwallController@getMyOfferwall' ]);

                // Premium Membership
                Route::get('premium-membership', [ 'uses' => 'Theme\PremiumMembershipController@getPremiumMembership' ]);
                Route::get('premium-membership-advantages/{membership}', [ 'uses' => 'Theme\PremiumMembershipController@getAdvantages' ]);
            
                Route::post('open-box', [ 'uses' => 'Theme\RoomController@setOpenBox' ]);

                // Support
                Route::get('support', [ 'uses' => 'Theme\SupportController@getSupport' ]);
                Route::post('support', [ 'uses' => 'Theme\SupportController@setSupport' ]);

                // Offerwall
                Route::get('awarded-tasks', [ 'uses' => 'Theme\FreeTicketsController@getAwardedTasks' ]);

                // Free Tickets
                Route::get('awarded-videos', [ 'uses' => 'Theme\FreeTicketsController@getFreeTickets' ]);

                Route::post('free-bonus', [ 'uses' => 'Theme\FreeTicketsController@setFreeBonus' ]);

                // Scratch Win
                Route::get('scratch-win', [ 'uses' => 'Theme\ScratchWinController@getScratchWin' ]);

                // Click Scratch
                Route::post('click-scratch', [ 'uses' => 'Theme\ScratchWinController@setClickScratch' ]);
                Route::get('check-watch', [ 'uses' => 'Theme\ScratchWinController@setCheckWatch' ]);
                //Route::post('start-scratch', [ 'uses' => 'Theme\SpinWinController@setStartScratch' ]);
                Route::post('complete-scratch', [ 'uses' => 'Theme\ScratchWinController@setCompleteScratch' ]);
                
            
        });
    
        // Contact Us
        Route::get('bize-ulasin', [ 'uses' => 'Theme\ContactUsController@getContactUs' ]);
        Route::post('bize-ulasin', [ 'uses' => 'Theme\ContactUsController@setContactUs' ]);

        // Withdraw Money
        Route::get('withdraw-money', [ 'uses' => 'Theme\WithdrawMoneyController@getWithdrawMoney' ]);

        
        

        
        
        // Rooms
        Route::get('turkey-rooms', [ 'uses' => 'Theme\IndexController@getTurkeyRooms' ]);
        Route::get('world-rooms', [ 'uses' => 'Theme\IndexController@getAbroadRooms' ]);
        Route::get('europe-rooms', [ 'uses' => 'Theme\IndexController@getEuropeRooms' ]);
        
        // Room
        Route::get('room/{city}', [ 'uses' => 'Theme\RoomController@getRoom' ]);
        
        // Fell Lucky
        Route::get('feel-lucky', [ 'uses' => 'Theme\RoomController@getFeelLucky' ]);
        
        // Spin Win
        Route::get('spin-win', [ 'uses' => 'Theme\SpinWinController@getSpinWin' ]);

        
        
        // Walkthrough
        Route::get('walkthrough', [ 'uses' => 'Theme\IndexController@getWalkthrough' ]);


        
        //Blog
        Route::get('blog', [ 'uses' => 'Theme\BlogController@getBlog' ]);
        Route::get('blog/{slug}', [ 'uses' => 'Theme\BlogController@getBlogDetail' ]);
        
        
        //Rewards
        Route::get('rewards', [ 'uses' => 'Theme\RewardsController@getRewards' ]);
        
        //Default Page
        Route::get('{slug}', [ 'uses' => 'Theme\PageController@getPage' ]);
    });
});

/* ADMIN ROUTES */

// Admin Login
Route::get('admin/login', [ 'uses' => 'Admin\LoginController@getLogin' ]);
Route::post('admin/login', [ 'uses' => 'Admin\LoginController@setLogin' ]);

Route::group(['middleware' => ['admin']], function () {
    // Main Page
    Route::get('admin/dashboard', [ 'uses' => 'Admin\DashboardController@getDashboard' ]);

    // Sales
    Route::get('admin/sales', [ 'uses' => 'Admin\SalesController@getSales' ]);
    Route::get('admin/cancel-sale/{id}', [ 'uses' => 'Admin\SalesController@setCancelSale' ]);

    // Payments
    Route::get('admin/payments', [ 'uses' => 'Admin\PaymentsController@getPayments' ]);
    Route::get('admin/complete-payment/{id}', [ 'uses' => 'Admin\PaymentsController@setCompletePayment' ]);

    // Payment Methods
    Route::get('admin/payment-methods', [ 'uses' => 'Admin\PaymentMethodsController@getPaymentMethods' ]);
    Route::get('admin/payment-method/{id}', [ 'uses' => 'Admin\PaymentMethodsController@getPaymentMethod' ]);
    Route::post('admin/new-payment-method', [ 'uses' => 'Admin\PaymentMethodsController@setNewPaymentMethod' ]);
    Route::get('admin/delete-payment-method/{id}', [ 'uses' => 'Admin\PaymentMethodsController@setDeletePaymentMethod' ]);

    // Winners
    Route::get('admin/winners', [ 'uses' => 'Admin\WinnersController@getWinners' ]);
    Route::post('admin/winners', [ 'uses' => 'Admin\WinnersController@setWinners' ]);

    // Settings
    Route::get('admin/settings', [ 'uses' => 'Admin\SettingsController@getSettings' ]);
    Route::post('admin/settings', [ 'uses' => 'Admin\SettingsController@setSettings' ]);

    // Rewardeds
    Route::get('admin/rewardeds', [ 'uses' => 'Admin\RewardedsController@getRewardeds' ]);

    // Offerwall
    Route::get('admin/offerwall', [ 'uses' => 'Admin\OfferwallController@getOfferwall' ]);

    // Exchanges
    Route::get('admin/exchanges', [ 'uses' => 'Admin\ExchangesController@getExchanges' ]);

    // Support
    Route::get('admin/support', [ 'uses' => 'Admin\SupportController@getSupport' ]);
    Route::get('admin/delete-support/{id}', [ 'uses' => 'Admin\SupportController@setDeleteSupport' ]);

    // Ready Answers
    Route::get('admin/ready-answers', [ 'uses' => 'Admin\ReadyAnswersController@getReadyAnswers' ]);
    Route::get('admin/delete-ready-answer/{id}', [ 'uses' => 'Admin\ReadyAnswersController@setDeleteReadyAnswer' ]);
    Route::post('admin/ready-answers', [ 'uses' => 'Admin\ReadyAnswersController@setReadyAnswers' ]);

    // Errors
    Route::get('admin/errors', [ 'uses' => 'Admin\ErrorsController@getErrors' ]);
    Route::get('admin/delete-errors', [ 'uses' => 'Admin\ErrorsController@setDeleteErrors' ]);

    // Send Notification
    Route::get('admin/send-notification', [ 'uses' => 'Admin\SendNotificationController@getSendNotification' ]);
    Route::post('admin/send-notification', [ 'uses' => 'Admin\SendNotificationController@setSendNotification' ]);
    // Send Message (Bulk)
    Route::post('admin/send-message', [ 'uses' => 'Admin\SendNotificationController@setSendMessage' ]);
    
    // User Management
    Route::get('admin/user-management/users', [ 'uses' => 'Admin\UserManagement\UsersController@getUsers' ]);
    Route::get('admin/user-management/user/{id}', [ 'uses' => 'Admin\UserManagement\UsersController@getUser' ]);
    Route::post('admin/user-management/user', [ 'uses' => 'Admin\UserManagement\UsersController@setUser' ]);
    Route::post('admin/user-management/user-jokers', [ 'uses' => 'Admin\UserManagement\UsersController@setUserJokers' ]);
    Route::post('admin/user-management/send-message', [ 'uses' => 'Admin\UserManagement\UsersController@setSendMessage' ]);
    Route::post('admin/user-management/send-notification', [ 'uses' => 'Admin\UserManagement\UsersController@setSendNotification' ]);
    Route::post('admin/user-management/send-ticket', [ 'uses' => 'Admin\UserManagement\UsersController@setSendTicket' ]);
    Route::post('admin/user-management/premium-membership', [ 'uses' => 'Admin\UserManagement\UsersController@setPremiumMembership' ]);
    Route::post('admin/user-management/payment', [ 'uses' => 'Admin\UserManagement\UsersController@setPayment' ]);
    Route::post('admin/user-management/new-note', [ 'uses' => 'Admin\UserManagement\UsersController@setNewNote' ]);

    // Site Management
        // Page Management
        Route::get('admin/site-management/pages', [ 'uses' => 'Admin\SiteManagement\PagesController@getPages' ]);
        Route::get('admin/site-management/new-page', [ 'uses' => 'Admin\SiteManagement\PagesController@getNewPage' ]);
        Route::post('admin/site-management/new-page', [ 'uses' => 'Admin\SiteManagement\PagesController@setNewPage' ]);
        // Slider Management
        Route::get('admin/site-management/slides', [ 'uses' => 'Admin\SiteManagement\SlidesController@getSlides' ]);
        Route::post('admin/site-management/new-slide', [ 'uses' => 'Admin\SiteManagement\SlidesController@setNewSlide' ]);
        Route::post('admin/site-management/delete-slide', [ 'uses' => 'Admin\SiteManagement\SlidesController@setDeleteSlide' ]);
        // Banner Management
        Route::get('admin/site-management/ads', [ 'uses' => 'Admin\SiteManagement\AdsController@getAds' ]);
        
    // Product Management
        // Products
        Route::get('admin/product-management/products', [ 'uses' => 'Admin\ProductManagement\ProductsController@getProducts' ]);
        Route::get('admin/product-management/new-product', [ 'uses' => 'Admin\ProductManagement\ProductsController@getNewProduct' ]);
        Route::post('admin/product-management/new-product', [ 'uses' => 'Admin\ProductManagement\ProductsController@setNewProduct' ]);
        // Brands
        Route::get('admin/product-management/product-brands', [ 'uses' => 'Admin\ProductManagement\ProductBrandsController@getProductBrands' ]);
        Route::post('admin/product-management/new-product-brand', [ 'uses' => 'Admin\ProductManagement\ProductBrandsController@setNewProductBrand' ]);
        Route::post('admin/product-management/delete-product-brand', [ 'uses' => 'Admin\ProductManagement\ProductBrandsController@setDeleteProductBrand' ]);
        // Categories
        Route::get('admin/product-management/product-categories', [ 'uses' => 'Admin\ProductManagement\ProductCategoriesController@getProductCategories' ]);
        Route::post('admin/product-management/new-product-category', [ 'uses' => 'Admin\ProductManagement\ProductCategoriesController@setNewProductCategory' ]);
        Route::post('admin/product-management/delete-product-category', [ 'uses' => 'Admin\ProductManagement\ProductCategoriesController@setDeleteProductCategory' ]);
        // Attributes
        Route::get('admin/product-management/product-attributes', [ 'uses' => 'Admin\ProductManagement\ProductAttributesController@getProductAttributes' ]);
        Route::post('admin/product-management/new-product-attribute', [ 'uses' => 'Admin\ProductManagement\ProductAttributesController@setNewProductAttribute' ]);
        Route::post('admin/product-management/delete-product-attribute', [ 'uses' => 'Admin\ProductManagement\ProductAttributesController@setDeleteProductAttribute' ]);
    
    // Logout
    Route::get('admin/cikis-yap', [ 'uses' => 'Admin\LoginController@setLogout' ]);
});


