<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="page-content header-clear-medium" style="padding-top: 50px;">
	<div class="content-boxed" style="margin: 0 0 0 !important; border-radius: 0px !important;">
		<div class="content">
			<h3 class="bolder" style="text-align: center;"><?php echo app('translator')->getFromJson('general.my_account'); ?></h3>
			<p style="text-align: center">
				<?php echo app('translator')->getFromJson('general.you_can_edit_your_account_information'); ?>
			</p>
			<?php echo Form::open(['url'=>'my-account', 'method'=>'post', 'class'=>'register-form outer-top-xs']); ?>

				<div class="input-style input-style-2 input-required">
					<em><i class="fa fa-angle-down"></i></em>
					<select name="lang">
						<option value="en" <?php if(Sentinel::getUser()->lang === "en"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.english'); ?></option>
	                    <option value="de" <?php if(Sentinel::getUser()->lang === "de"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.german'); ?></option>
	                    <option value="tr" <?php if(Sentinel::getUser()->lang === "tr"): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('general.turkish'); ?></option>
					</select>
				</div>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon fa fa-at"></i>
					<input type="email" name="email" placeholder="<?php echo app('translator')->getFromJson('general.email'); ?>" value="<?php echo e(Sentinel::getUser()->email_2); ?>" disabled>
				</div>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon fa fa-user"></i>
					<input type="name" name="username" placeholder="<?php echo app('translator')->getFromJson('general.username'); ?>" value="<?php echo e(Sentinel::getUser()->first_name); ?>" disabled>
				</div>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon fas fa-phone"></i>
					<input type="name" name="phone" placeholder="<?php echo app('translator')->getFromJson('general.phone'); ?>" value="<?php echo e(Sentinel::getUser()->phone); ?>" disabled>
				</div>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon fas fa-asterisk"></i>
					<input type="password" name="password" placeholder="<?php echo app('translator')->getFromJson('general.password'); ?>">
				</div>
				<div class="input-style input-style-2 has-icon input-required">
					<i class="input-icon fas fa-asterisk"></i>
					<input type="password" name="password_confirm" placeholder="<?php echo app('translator')->getFromJson('general.password'); ?> (<?php echo app('translator')->getFromJson('general.again'); ?>)">
				</div>
				<button type="submit" class="back-button button button-full button-m shadow-large button-round-small bg-highlight top-30 bottom-0" style="width:100%"><?php echo app('translator')->getFromJson('general.save'); ?></button>
			<?php echo e(Form::close()); ?>

		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>