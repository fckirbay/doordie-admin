<?php $__env->startSection('styles'); ?>
    <style>
        .inlineinput div {
    display: inline;
}
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <div class="page-login bottom-20">
                <h3 class="uppercase ultrabold top-30 bottom-0 center-text"><?php echo app('translator')->getFromJson('general.verify_your_account'); ?></h3>
                <!--<p class="smaller-text bottom-30" style="margin-bottom:10px !important">Ücretsiz 10 kutu açmak için şimdi kaydolun!</p>-->
                <?php echo Form::open(['url'=>'verify-complete', 'method'=>'post', 'class'=>'register-form outer-top-xs']); ?>

                <a class="forgot center-text"><?php echo app('translator')->getFromJson('general.write_last_5_chars_of_missed_call'); ?></a>
                    <input type="hidden" name="keymatch" value="<?php echo e($json['keymatch']); ?>"/>
                    <input type="hidden" name="otp_start" value="<?php echo e($json['otp_start']); ?>"/>
                    <input type="hidden" name="mobile" value="<?php echo e($json['mobile']); ?>"/>
                    <div class="page-login-field top-15">
                        <i class="fas fa-asterisk"></i>
                        <input type="number" name="otp_code" class="phone" placeholder="<?php echo app('translator')->getFromJson('general.last_5_digits'); ?>" maxlength="15" required>
                        <em>(required)</em>
                    </div>
                    <button type="submit" class="button bg-highlight button-full button-rounded button-s uppercase ultrabold"><?php echo app('translator')->getFromJson('general.verify'); ?></button>
                <?php echo Form::close(); ?>

            </div>
            
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript" src="<?php echo e(asset('assets/mobile/scripts/jquery.mask.js')); ?>"></script>
    <script>
       $(document).ready(function(){
            $('.phone').mask('000000000000');
       });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>