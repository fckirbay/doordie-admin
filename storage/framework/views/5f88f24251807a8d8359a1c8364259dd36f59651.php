<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ödeme Yöntemleri</h3>
              <div class="box-tools">

                <div class="row">
                  <div class="col-xs-12">
                    <a href="#addNew" data-toggle="modal" class="btn btn-default"><i class="fa fa-plus"></i> Yeni Ekle</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Logo</th>
                  <th>Başlık</th>
                  <th class="orta">Açıklama</th>
                  <th class="orta">Ödül</th>
                  <th class="orta">Sıra</th>
                  <th class="orta">Durum</th>
                  <th class="orta">Sil</th>
                </tr>
                <?php $__empty_1 = true; $__currentLoopData = $paymentMethods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr>
                  <td class="orta"><img src="<?php echo e(asset($val->photo)); ?>" style="width:50px"/></td>
                  <td><a href="<?php echo e(url('admin/payment-method', $val->id)); ?>"><?php echo e($val->name); ?></a></td>
                  <td class="orta"><?php echo e($val->subtitle); ?></td>
                  <td class="orta"><?php echo e($val->coin); ?></td>
                  <td class="orta"><?php echo e($val->orders); ?></td>
                  <td class="orta"><?php if($val->status == 1): ?> TR <?php else: ?> OTHERS <?php endif; ?></td>
                  <td class="orta"><a href="<?php echo e(url('admin/delete-payment-method', $val->id)); ?>" class="btn btn-danger btn-xs">Sil</a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                <?php endif; ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
    	<div class="modal fade" id="addNew">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Yeni Ödeme Yöntemi</h4>
              </div>
              <?php echo Form::open(['url'=>'admin/new-payment-method', 'method' => 'post', 'files' => 'true']); ?>

              <div class="modal-body">
              	<div class="row">
                	<div class="col-md-6">
                        <div class="form-group">
                          <label>Başlık</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Başlık" name="title">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Açıklama</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Açıklama" name="description">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Ödül Miktarı</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Ödül Miktarı" name="prize">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
		                  <label for="exampleInputFile">Logo</label>
		                  <input type="file" id="exampleInputFile" name="logo">
		                </div>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Kapat</button>
                <button type="submit" class="btn btn-primary">Kaydet</button>
              </div>
              <?php echo Form::close(); ?>

            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>