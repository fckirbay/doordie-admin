        <div class="header header-light header-static header-logo-app">
            <a href="<?php echo e(url('/')); ?>" class="header-logo" style="text-align:center !important"></a>
            <a href="#" class="header-icon header-icon-1 back-button" style="left:30px !important"><i class="fas fa-arrow-left"></i></a>
            <a data-menu="menu-1" href="#" class="header-icon header-icon-3" style="left:0px !important"><i class="fas fa-bars"></i></a>
            <?php if(Sentinel::check()): ?>
            <a data-menu="menu-2" href="#" class="header-icon header-icon-4" style="right:00px !important"><i class="fas fa-user"></i></a>
            <a data-menu="menu-2" href="#" id="ticket" class="header-icon header-icon-4" style="right:40px !important; font-weight:900; font-size:14px"><?php echo e(Sentinel::getUser()->ticket); ?></a>
            <?php else: ?>
            <a href="<?php echo e(url('login')); ?>" class="header-icon header-icon-4" style="right:0px !important"><i class="fas fa-sign-in-alt"></i></a>
            <?php endif; ?>
        </div>