			<!-- class="active-nav" -->
			<div id="footer-menu" class="footer-menu-5-icons footer-menu-style-1">
				<a href="<?php echo e(url('/')); ?>"><i class="fas fa-bars"></i><span><?php echo app('translator')->getFromJson('general.home'); ?></span></a>
				<a href="<?php echo e(url('/bank')); ?>"><i class="fas fa-dollar-sign"></i><span><?php echo app('translator')->getFromJson('general.safe'); ?></span></a>
				<a href="<?php echo e(url('/play')); ?>"><i class="far fa-play-circle"></i><span><?php echo app('translator')->getFromJson('general.new_game'); ?></span></a>
				<a href="<?php echo e(url('opportunities')); ?>"><i class="fas fa-dice"></i><span><?php echo app('translator')->getFromJson('general.opportunities'); ?></span></a>
				<a href="<?php echo e(url('/prizes')); ?>" data-menu="menu-settings"><i class="fas fa-gift"></i><span><?php echo app('translator')->getFromJson('general.prizes'); ?></span></a>
				<div class="clear"></div>
			</div>