<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Kullanıcılar</h3>
                
              <?php echo Form::open(['url'=>'admin/user-management/users', 'method' => 'get', 'style' => 'float:right', 'id'=>'search-form']); ?>

              <div class="box-tools">

                <div class="row">
                  <div class="col-xs-5">
                    <select class="form-control" name="order">
                      <option value="">Seçiniz</option>
                      <option value="balance">Bakiye</option>
                      <option value="ticket">Bilet Sayısı</option>
                      <option value="reference_count">Referans Sayısı</option>
                      <option value="clicks">Tıklamalar</option>
                    </select>
                  </div>
                  <div class="col-xs-5">
                    <input type="text" name="search" class="form-control pull-right" placeholder="Ara">
                  </div>
                  <div class="col-xs-2">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
              <?php echo Form::close(); ?>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:5%">Ülke</th>
                  <th style="width:3%">OS</th>
                  <th class="orta" style="width:5%">E/D</th>
                  <th style="width:22%">Kullanıcı Adı</th>
                  <th class="orta" style="width:10%">Telefon Numarası</th>
                  <th class="orta" style="width:10%">Bilet Sayısı</th>
                  <th class="orta" style="width:10%">Referans Sayısı</th>
                  <th class="orta" style="width:10%">Bakiye</th>
                  <th class="orta" style="width:5%">Deneme Sayısı</th>
                  <th class="orta" style="width:5%">#</th>
                </tr>
                <?php $__empty_1 = true; $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr>
                  <td><?php if($val->country != null): ?><img src="<?php echo e(asset('assets/cardgame/images/flags/'.strtolower($val->country).'.svg')); ?>" style="width:30px; height:20px"/><?php else: ?><img src="<?php echo e(asset('assets/cardgame/images/flags/unknown.png')); ?>" style="width:30px; height:20px"/><?php endif; ?></td>
                  <td><?php if($val->os == 'android'): ?><i class="fa fa-android" style="color: #99CC00"></i><?php elseif($val->os == 'ios'): ?><i class="fa fa-apple" style="color: #A6B1B7"></i><?php endif; ?></td>
                  <td class="orta"><?php if($val->blocked == 1): ?><i class="fa fa-times" style="color:red"></i><?php else: ?><i class="fa fa-check" style="color:green"></i><?php endif; ?> <?php if($val->verification == 0): ?><i class="fa fa-times" style="color:red"></i><?php else: ?><i class="fa fa-check" style="color:green"></i><?php endif; ?></td>
                  <td><a href="<?php echo e(url('admin/user-management/user', $val->id)); ?>"><?php echo e($val->username); ?></a></td>
                  <!--<td><?php if($val->country != null): ?><img src="<?php echo e(asset('assets/mobile/images/flags/'.trim(strstr($val->country, '(', true)).'.png')); ?>" style="width:30px; height:20px"/><?php endif; ?> <?php echo e(strstr($val->country, '(', true)); ?></td>-->
                  <td class="orta"><?php echo e($val->phone); ?></td>
                  <td class="orta"><?php echo e($val->ticket); ?></td>
                  <td class="orta"><?php echo e($val->reference_count); ?></td>
                  <td class="orta"><?php echo e(number_format($val->balance / 100, 2, ',', '.')); ?> <?php if($val->currency == "eur"): ?>€<?php else: ?>₺<?php endif; ?></td>
                  <td class="orta"><?php if($val->verification_tries >= 3): ?><span style="font-weight:bold; color:red"><?php echo e($val->verification_tries); ?></span><?php else: ?><?php echo e($val->verification_tries); ?><?php endif; ?></td>
                  <td class="orta"><a href="<?php echo e(url('admin/user-management/user', $val->id)); ?>" class="btn btn-primary btn-xs">Detay</a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                <?php endif; ?>
              </table>
            </div>
            <?php echo $users->appends(Request::capture()->except('page'))->render(); ?>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <script>
    $('#search-form').submit(function() {
        $(this).find(":input").filter(function(){return !this.value;}).attr("disabled", "disabled");
    });
    // Un-disable form fields when page loads, in case they click back after submission or client side validation
    $('#search-form').find(":input").prop("disabled", false);
  </script>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>