<?php $__env->startSection('styles'); ?>
<style>
.custom-radio {
  padding: 10px 10px 10px 25px;
  border: 1px solid #cccccc;
  border-radius: 4px;
  width: 100%;
  margin-bottom: 15px;
  position: relative;
  overflow: hidden;
}
.custom-radio input[type="radio"] {
  position: absolute;
  z-index: -9999;
  visibility: hidden;
  opacity: 0;
}
.custom-radio input[type="radio"] + label {
  flex: 1 1 100%;
  position: relative;
  cursor: pointer;
  display: flex;
  flex-flow: row nowrap;
  width: 100%;
  justify-content: space-between;
  margin: 0;
}
.custom-radio input[type="radio"] + label:hover span:before {
  background: rgba(57, 139, 211, 0.15);
  border-color: rgba(57, 139, 211, 0.7);
}
.custom-radio input[type="radio"] + label span,
.custom-radio input[type="radio"] + label img {
  align-self: center;
}
.custom-radio input[type="radio"] + label span {
  display: flex;
  align-items: center;
}
.custom-radio input[type="radio"] + label span:before {
  content: "";
  display: inline-block;
  background: white;
  width: 24px;
  height: 24px;
  border: 1px solid #cccccc;
  margin-right: 10px;
  border-radius: 2px;
  transition: all 0.1s;
  align-self: center;
  vertical-align: center;
}
.custom-radio input[type="radio"] + label span:after {
  opacity: 0;
  content: "\f00c";
  font-family: FontAwesome;
  display: block;
  color: #fff;
  position: absolute;
  left: 4px;
  top: 50%;
  transform: scale(0.5) translateY(-50%);
  transition: all 0.25s cubic-bezier(0.75, 0.1, 0.1, 0.5);
}
.custom-radio input[type="radio"]:focus + label span:before {
  border-color: rgba(57, 139, 211, 0.7);
  background: rgba(57, 139, 211, 0.15);
}
.custom-radio input[type="radio"]:checked + label span:before {
  background: #398bd3;
  border-color: #398bd3;
}
.custom-radio input[type="radio"]:checked + label span:after {
  opacity: 1;
  transform: scale(1) translateY(-50%);
}
.custom-radio input[type="radio"]:checked ~ .extra-info {
  display: block;
}
.custom-radio .extra-info {
  flex: 1 1 100%;
  width: 100%;
  display: none;
  border-top: 1px solid rgba(204, 204, 204, 0.5);
  padding: 10px 0;
  margin-top: 10px;
}

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="checkout-box ">
   <div class="row">
       <?php if(Sentinel::check()): ?>
      <div class="col-md-12">
         <div class="panel-group checkout-steps" id="accordion">
            <!-- checkout-step-01  -->
            <div class="panel panel-default checkout-step-01">
               <!-- panel-heading -->
               <div class="panel-heading">
                  <h4 class="unicase-checkout-title">
                     <a data-toggle="collapse" class="" data-parent="#accordion" href="#collapseOne">
                     <span>1</span>Adresinizi Seçin
                     </a>
                  </h4>
               </div>
               <!-- panel-heading -->
               <div id="collapseOne" class="panel-collapse collapse in">
                  <!-- panel-body  -->
                  <div class="panel-body">
                     <div class="row">
                        <!-- guest-login -->			
                        <div class="col-md-12 col-sm-12 guest-login">
                           <div class="custom-radio-list row">
                             <form>
                                 <div class="form-group">
                                    <?php $__empty_1 = true; $__currentLoopData = $addresses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                     <div class="custom-radio">
                                         <input type="radio" id="radio<?php echo e($key); ?>" name="radio-demo" <?php if($val->default == 1): ?> checked <?php endif; ?>>
                                         <label for="radio<?php echo e($key); ?>">
                                             <span><?php echo e($val->title); ?></span>
                                         </label>
                                         <div class="extra-info">
                                             <?php echo e($val->address); ?> <?php echo e($val->district); ?> <?php echo e($val->town); ?> / <?php echo e($val->city); ?>

                                         </div>
                                     </div>
                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                     
                                     <?php endif; ?>
                                 </div>
                                 <hr>
                             </form>
                         </div>    
                           <br /><br />
                           <button type="submit" class="btn-upper btn btn-primary checkout-page-button checkout-continue " style="floar:left">Yeni Adres Ekle</button>
                           <button type="submit" class="btn-upper btn btn-primary checkout-page-button checkout-continue next-step" data-step="2" style="float:right">Devam Et</button>
                        </div>
                        <!-- guest-login -->		
                     </div>
                  </div>
                  <!-- panel-body  -->
               </div>
               <!-- row -->
            </div>
            <!-- checkout-step-01  -->
            <!-- checkout-step-02  -->
            <div class="panel panel-default checkout-step-02">
               <div class="panel-heading">
                  <h4 class="unicase-checkout-title">
                     <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseTwo" id="step-2" style="pointer-events: none">
                     <span>2</span>Ödeme Yöntemi
                     </a>
                  </h4>
               </div>
               <div id="collapseTwo" class="panel-collapse collapse">
                  <div class="panel-body">
                     Ödeme yönteminizi seçin...
                     <br /><br />
                     <button type="submit" class="btn-upper btn btn-primary checkout-page-button checkout-continue next-step" data-step="3" style="float:right">Devam Et</button>
                  </div>
               </div>
            </div>
            <!-- checkout-step-02  -->
            <!-- checkout-step-03  -->
            <div class="panel panel-default checkout-step-03">
               <div class="panel-heading">
                  <h4 class="unicase-checkout-title">
                     <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseThree" id="step-3" style="pointer-events: none">
                     <span>3</span>Ödemeyi Yapın
                     </a>
                  </h4>
               </div>
               <div id="collapseThree" class="panel-collapse collapse">
                  <div class="panel-body">
                     Ödemenizi yapın...
                     <br /><br />
                     <button type="submit" class="btn-upper btn btn-primary checkout-page-button checkout-continue " style="float:right">Ödemeyi Tamamla</button>
                  </div>
               </div>
            </div>
            <!-- checkout-step-03  -->
            
         </div>
         <!-- /.checkout-steps -->
      </div>
      <?php else: ?>
      
      <?php endif; ?>
   </div>
   <!-- /.row -->
</div>
<!-- /.checkout-box -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
    $( ".next-step" ).click(function() {
      var step = $(this).data('step');
      $('#step-'+step).css("pointer-events", "auto").click();
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>