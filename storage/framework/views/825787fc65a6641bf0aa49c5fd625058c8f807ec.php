<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              
              <h3 class="profile-username text-center"><?php echo e($user->first_name); ?></h3>

              <p class="text-muted text-center"><?php echo e(Carbon\Carbon::parse($user->created_at)->format('H:i:s d/m/Y')); ?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Bakiye</b> <a class="pull-right"><?php echo e(number_format($user->balance, 2, ',', '.')); ?> ₺</a>
                </li>
                <li class="list-group-item">
                  <b>Toplam Kazanç</b> <a class="pull-right"><?php echo e(number_format($user->earnings, 2, ',', '.')); ?> ₺</a>
                </li>
                <li class="list-group-item">
                  <b>Bilet</b> <a class="pull-right"><?php echo e($user->ticket); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Açtığı Kutu</b> <a class="pull-right"><?php echo e($user->clicks); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Kazanan</b> <a class="pull-right"><?php echo e($user->won); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Kaybeden</b> <a class="pull-right"><?php echo e($user->lost); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Ref. Sayısı</b> <a class="pull-right"><?php echo e($user->reference_count); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Ref. ID</b> <a class="pull-right"><?php echo e($user->reference_id); ?></a>
                </li>
              </ul>

              <a href="#" class="btn btn-danger btn-block"><b>Engelle</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Bilgiler</a></li>
              <li><a href="#timeline" data-toggle="tab">Açtığı Kutular</a></li>
              <li><a href="#settings" data-toggle="tab">İşlem Geçmişi</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <?php echo Form::open(['url'=>'admin/user-management/user', 'method'=>'post', 'autocomplete' => 'off']); ?>

              <div class="box-body">
                <div class="row">
                    <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>"/>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>İsim Soyisim</label>
                          <input type="text" class="form-control" id="inputName" placeholder="İsim Soyisim" name="first_name" value="<?php echo e($user->first_name); ?>" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>TC No.</label>
                          <input type="text" class="form-control" id="inputName" placeholder="TC No." name="tc_number" value="<?php echo e($user->tc_number); ?>" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Doğum Tarihi</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Doğum Tarihi" name="birthday" value="<?php echo e(Carbon\Carbon::parse($user->birthday)->format('d.m.Y')); ?>" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>E-Posta Adresi</label>
                          <input type="text" class="form-control" id="inputName" placeholder="E-Posta Adresi" name="email_2" value="<?php echo e($user->email_2); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Telefon</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Telefon" name="phone" value="<?php echo e($user->phone); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Telefon 2</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Telefon 2" name="phone_2" value="<?php echo e($user->phone_2); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Bilet</label>
                          <input type="number" class="form-control" id="inputName" placeholder="Bilet" name="ticket" value="<?php echo e($user->ticket); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Durum</label>
                          <select class="form-control" name="blocked" required>
                            <option value="0" <?php if($user->blocked == 0): ?> selected <?php endif; ?>>Aktif</option>
                            <option value="1" <?php if($user->blocked == 1): ?> selected <?php endif; ?>>Engelli</option>
                          </select>
                        </div>
                    </div>
                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Kaydet</button>
              </div>
            <?php echo Form::close(); ?>

              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                <table class="table table-striped">
                  <tr>
                      <th>Şehir</th>
                      <th>Numara</th>
                      <th>Kazanç</th>
                      <th>Tarih</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $openBoxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><?php echo e($val->city); ?></td>
                      <td><?php echo e($val->number); ?></td>
                      <td><?php if($val->prize > 0): ?><?php echo e(number_format($val->prize, 2, ',', '.')); ?> ₺<?php else: ?> - <?php endif; ?></td>
                      <td><?php echo e(Carbon\Carbon::parse($val->created_at)->format('d M H:i')); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç kutu açmadı!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <table class="table table-striped">
                  <tr>
                      <th>İşlem Türü</th>
                      <th>Tutar</th>
                      <th>Durum</th>
                      <th>İşlem Tarihi</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $transactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><?php if($val->type == 1): ?> Para Yatırma <?php else: ?> Para Çekme <?php endif; ?></td>
                      <td><?php echo e(number_format($val->amount, 2, ',', '.')); ?> ₺</td>
                      <td><?php if($val->status == 0): ?><em class="color-yellow-dark">Bekliyor <i class="fa fa-circle"></i> </em><?php elseif($val->status == 1): ?><em class="color-green-dark">Onaylandı <i class="fa fa-circle"></i> </em><?php elseif($val->status == 2): ?><em class="color-orange-dark">Reddedildi <i class="fa fa-circle"></i> </em><?php else: ?><em class="color-blue-dark">Hatalı <i class="fa fa-circle"></i> </em><?php endif; ?></td>
                      <td>İşlem Tarihi: <?php echo e(Carbon\Carbon::parse($val->created_at)->format('H:i d M')); ?> <?php if($val->confirm_date != null): ?>Onay Tarihi: <?php echo e(Carbon\Carbon::parse($val->confirm_date)->format('H:i d M')); ?><?php endif; ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç işlem yok!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>