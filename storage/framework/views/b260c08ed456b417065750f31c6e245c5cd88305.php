<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ayarlar</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <?php echo Form::open(['url'=>'admin/settings', 'method'=>'post', 'autocomplete' => 'off']); ?>

              <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>Satın Alma</label>
                          <select class="form-control" name="purchasing" required>
                            <option value="0">Kapalı</option>
                            <option value="1">Açık</option>
                          </select>
                        </div>
                    </div>
                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Kaydet</button>
              </div>
            <?php echo Form::close(); ?>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>