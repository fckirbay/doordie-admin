<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php if(Sentinel::getUser()->photo == null): ?><?php echo e(asset('assets/admin/img/no-profile.png')); ?><?php else: ?> <?php echo e(asset(Sentinel::getUser()->photo)); ?><?php endif; ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><a href="<?php echo e(url(config('app.admin_path').'/my-profile')); ?>" style="color:#fff"><?php echo e(Sentinel::getUser()->first_name); ?> <?php echo e(Sentinel::getUser()->last_name); ?></a></p>
          <a href="#"><i class="fa fa-circle text-success"></i>
              Admin
          </a>
        </div>
      </div>
      <!-- search form -->
<?php echo Form::open(['url'=>'admin/user-management/users', 'method' => 'get', 'class'=>'sidebar-form']); ?>

        <div class="input-group">
          <input type="text" name="search" class="form-control" placeholder="Ara..." minlength="4" required>
              <span class="input-group-btn">
                <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      <?php echo Form::close(); ?>

      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menü</li>
        <li class="<?php echo e(Request::segment(2) === 'dashboard' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/dashboard')); ?>">
            <i class="fa fa-dashboard"></i> <span> Gösterge Paneli</span>
          </a>
        </li>
        <li class="<?php echo e(Request::segment(3) === 'users' ? 'active' : null); ?>

        <?php echo e(Request::segment(3) === 'user' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/user-management/users')); ?>">
            <i class="fa fa-users"></i> <span> Kullanıcılar</span>
          </a>
        </li>
        <li class="<?php echo e(Request::segment(2) === 'support' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/support')); ?>">
            <i class="fa fa-life-ring"></i> <span> Destek</span>
          </a>
        </li>
        <li class="<?php echo e(Request::segment(2) === 'rewardeds' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/rewardeds')); ?>">
            <i class="fa fa-usd"></i> <span> Ödüllü Reklamlar</span>
          </a>
        </li>
        <li class="<?php echo e(Request::segment(2) === 'offerwall' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/offerwall')); ?>">
            <i class="fa fa-check"></i> <span> Tamamlanan Görevler</span>
          </a>
        </li>
        <!--<li class="<?php echo e(Request::segment(2) === 'sales' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/sales')); ?>">
            <i class="fa fa-eur"></i> <span> Üyelik Satışları</span>
          </a>
        </li>-->
        <li class="<?php echo e(Request::segment(2) === 'payments' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/payments')); ?>">
            <i class="fa fa-credit-card"></i> <span> Ödemeler</span>
          </a>
        </li>
        <li class="<?php echo e(Request::segment(2) === 'send-notification' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/send-notification')); ?>">
            <i class="fa fa-bell"></i> <span> Bildirim & Mesaj Gönder</span>
          </a>
        </li>
        <!--
        <li class="<?php echo e(Request::segment(2) === 'exchanges' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/exchanges')); ?>">
            <i class="fa fa-exchange"></i> <span> Dönüşümler</span>
          </a>
        </li>
        -->
        <li class="<?php echo e(Request::segment(2) === 'ready-answers' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/ready-answers')); ?>">
            <i class="fa fa-comments"></i> <span> Hazır Cevaplar</span>
          </a>
        </li>
        <li class="<?php echo e(Request::segment(2) === 'payment-methods' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/payment-methods')); ?>">
            <i class="fa fa-money"></i> <span> Ödeme Yöntemleri</span>
          </a>
        </li>
        <!--
        <li class="<?php echo e(Request::segment(2) === 'winners' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/winners')); ?>">
            <i class="fa fa-trophy"></i> <span> Kazananlar</span>
          </a>
        </li>
        <li class="<?php echo e(Request::segment(2) === 'settings' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/settings')); ?>">
            <i class="fa fa-cog"></i> <span> Ayarlar</span>
          </a>
        </li>
        -->
        <li class="<?php echo e(Request::segment(2) === 'errors' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/errors')); ?>">
            <i class="fa fa-ban"></i> <span> Hatalar</span>
          </a>
        </li>
        <!--
        <li class="treeview <?php echo e(Request::segment(3) === 'pages' ? 'active' : null); ?>

        <?php echo e(Request::segment(3) === 'new-page' ? 'active' : null); ?>

        <?php echo e(Request::segment(3) === 'slides' ? 'active' : null); ?>">
          <a href="#">
            <i class="fa fa-sitemap"></i>
            <span>Site Yönetimi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="<?php if(Request::segment(3) === 'pages' || Request::segment(3) === 'new-page'): ?> active <?php endif; ?>"><a href="<?php echo e(url('admin/site-management/pages')); ?>"><i class="fa fa-circle-o"></i> Sayfa Yönetimi</a></li>
              <li class="<?php if(Request::segment(3) === 'slides' || Request::segment(3) === 'slides'): ?> active <?php endif; ?>"><a href="<?php echo e(url('admin/site-management/slides')); ?>"><i class="fa fa-circle-o"></i> Slider Yönetimi</a></li>
              <li class="<?php if(Request::segment(3) === 'ads'): ?> active <?php endif; ?>"><a href="<?php echo e(url('admin/site-management/ads')); ?>"><i class="fa fa-circle-o"></i> Reklam Yönetimi</a></li>
          </ul>
        </li>
        <li class="treeview <?php echo e(Request::segment(3) === 'products' ? 'active' : null); ?>

        <?php echo e(Request::segment(3) === 'new-product' ? 'active' : null); ?>

        <?php echo e(Request::segment(3) === 'product-categories' ? 'active' : null); ?>

        <?php echo e(Request::segment(3) === 'product-brands' ? 'active' : null); ?>

        <?php echo e(Request::segment(3) === 'product-attributes' ? 'active' : null); ?>

        <?php echo e(Request::segment(3) === 'product-options' ? 'active' : null); ?>">
          <a href="#">
            <i class="fa fa-tags"></i>
            <span>Ürün Yönetimi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="<?php if(Request::segment(3) === 'products' || Request::segment(3) === 'new-product'): ?> active <?php endif; ?>"><a href="<?php echo e(url('admin/product-management/products')); ?>"><i class="fa fa-circle-o"></i> Ürünler</a></li>
              <li class="<?php if(Request::segment(3) === 'product-categories'): ?> active <?php endif; ?>"><a href="<?php echo e(url('admin/product-management/product-categories')); ?>"><i class="fa fa-circle-o"></i> Ürün Kategorileri</a></li>
              <li class="<?php if(Request::segment(3) === 'product-brands'): ?> active <?php endif; ?>"><a href="<?php echo e(url('admin/product-management/product-brands')); ?>"><i class="fa fa-circle-o"></i> Ürün Markaları</a></li>
              <li class="<?php if(Request::segment(3) === 'product-attributes'): ?> active <?php endif; ?>"><a href="<?php echo e(url('admin/product-management/product-attributes')); ?>"><i class="fa fa-circle-o"></i> Ürün Özellikleri</a></li>
              <li class="<?php if(Request::segment(3) === 'product-options'): ?> active <?php endif; ?>"><a href="<?php echo e(url('admin/product-management/product-options')); ?>"><i class="fa fa-circle-o"></i> Ürün Seçenekleri</a></li>
          </ul>
        </li>
        <li class="<?php echo e(Request::segment(3) === 'orders' ? 'active' : null); ?>">
          <a href="<?php echo e(url('admin/order-management/orders')); ?>">
            <i class="fa fa-try"></i> <span> Siparişler</span>
          </a>
        </li>
      -->
      </ul>
    </section>
    <!-- /.sidebar -->
    <!--
      <small class="label pull-right bg-yellow">12</small>
      <small class="label pull-right bg-green">16</small>
      <small class="label pull-right bg-red">5</small>
     -->
  </aside>