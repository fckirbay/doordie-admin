<!DOCTYPE HTML>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
      <title><?php echo app('translator')->getFromJson('general.lucky_box'); ?> || <?php echo app('translator')->getFromJson('general.new_game'); ?> </title>
      <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/mobile/styles/style.css')); ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/mobile/fonts/css/fontawesome-all.min.css')); ?>">
      <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/mobile/styles/framework.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(asset('assets/theme/plugins/sweetalert/dist/sweetalert.css')); ?>">
      <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
      <?php if(session('browser') == "android"): ?><script>android.fontSizeNormal();</script><?php endif; ?>
   </head>
   <body>
      <div id="page-transitions" class="page-build highlight-red">
         <div class="page-content page-content-full">
            <div class="coverpage coverpage-full walkthrough-buttons">
               <a href="#" class="next-slide-custom"><?php echo app('translator')->getFromJson('general.next'); ?></a>
               <a href="<?php echo e(url('/?walkthrough=1')); ?>" class="skip-slide-custom"><?php echo app('translator')->getFromJson('general.skip'); ?></a>
               <div class="cover-walkthrough-slider owl-carousel">
                  <div class="cover-item">
                     <div class="cover-content cover-content-center">
                        <img class="cover-icon" src="<?php echo e(asset('assets/mobile/images/icons/1.png')); ?>">
                        <h2 class="color-black regular center-text bottom-20 bolder uppercase">
                           <span class="font-24"><?php echo app('translator')->getFromJson('general.completely'); ?> <span class="color-red-dark"><?php echo app('translator')->getFromJson('general.free'); ?></span></span>
                        </h2>
                        <p class="color-black center-text opacity-50 bottom-30 font-14">
                           <?php echo app('translator')->getFromJson('general.create_your_account_for_free'); ?>
                        </p>
                     </div>
                     <div class="cover-overlay overlay bg-white"></div>
                  </div>
                  <div class="cover-item">
                     <div class="cover-content cover-content-center">
                        <img class="cover-icon" src="<?php echo e(asset('assets/mobile/images/icons/2.png')); ?>">
                        <h2 class="color-black regular center-text bottom-20 bolder uppercase"><?php echo app('translator')->getFromJson('general.get_your_tickets'); ?></h2>
                        <p class="color-black center-text opacity-60 bottom-30">
                           <?php echo app('translator')->getFromJson('general.get_your_free_10_tickets_now'); ?>
                        </p>
                     </div>
                     <div class="cover-overlay overlay bg-white"></div>
                  </div>
                  <div class="cover-item">
                     <div class="cover-content cover-content-center">
                        <img class="cover-icon" src="<?php echo e(asset('assets/mobile/images/icons/3.png')); ?>">
                        <h2 class="color-black regular center-text bottom-20 bolder uppercase"><?php echo app('translator')->getFromJson('general.choose_your_favorite_city'); ?></h2>
                        <p class="color-black center-text opacity-60 bottom-30">
                           <?php echo app('translator')->getFromJson('general.choose_from_hundreds_of_world_capitals'); ?>
                        </p>
                     </div>
                     <div class="cover-overlay overlay bg-white"></div>
                  </div>
                  <div class="cover-item">
                     <div class="cover-content cover-content-center">
                        <img class="cover-icon" src="<?php echo e(asset('assets/mobile/images/icons/4.png')); ?>">
                        <h2 class="color-black regular center-text bottom-20 bolder uppercase"><?php echo app('translator')->getFromJson('general.open_your_lucky_box'); ?></h2>
                        <p class="color-black center-text opacity-50 bottom-30">
                           <?php echo app('translator')->getFromJson('general.choose_your_lucky_numbered_boxes'); ?>
                        </p>
                     </div>
                     <div class="cover-overlay overlay bg-white"></div>
                  </div>
                  <div class="cover-item">
                     <div class="cover-content cover-content-center">
                        <img class="cover-icon" src="<?php echo e(asset('assets/mobile/images/icons/5.png')); ?>">
                        <h2 class="color-black regular center-text bottom-20 bolder uppercase"><?php echo app('translator')->getFromJson('general.win_real_prizes'); ?></h2>
                        <p class="color-black center-text opacity-50 bottom-30">
                           <?php echo app('translator')->getFromJson('general.earn_close_to_1_million_euro'); ?>
                        </p>
                     </div>
                     <div class="cover-overlay overlay bg-white"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript" src="<?php echo e(asset('assets/mobile/scripts/jquery.js')); ?>"></script>
      <script type="text/javascript" src="<?php echo e(asset('assets/mobile/scripts/plugins.js')); ?>"></script>
      <script type="text/javascript" src="<?php echo e(asset('assets/mobile/scripts/custom.js')); ?>"></script>
   </body>