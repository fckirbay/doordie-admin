<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
			<div class="page-content header-clear-medium" style="padding-top: 50px; padding-bottom: 0px">
				<div class="content-boxed" style="margin: 0px 0px 0px; border-radius: 0px !important">
					<div class="content bottom-5">
						<div class="link-list link-list-2 link-list-long-border">
							<a href="#" class="no-border">
								<i class="fa fa-times color-red2-dark"></i>
								<span><?php echo app('translator')->getFromJson('general.lost'); ?> (8 <?php echo app('translator')->getFromJson('general.lap'); ?>)</span>
								<em class="bg-red2-dark">0 Coin</em>
								<strong>05.04.2019 18:34:43</strong>
								<i class="fa fa-angle-right"></i>
							</a>
							<a href="#" class="no-border">
								<i class="fa fa-check color-green2-dark"></i>
								<span><?php echo app('translator')->getFromJson('general.won'); ?> (4 <?php echo app('translator')->getFromJson('general.lap'); ?>)</span>
								<em class="bg-green2-dark">8 <?php echo app('translator')->getFromJson('general.coin'); ?></em>
								<strong>05.04.2019 18:34:43</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>