<!DOCTYPE HTML>
<html lang="en">
	<head>
    <?php echo $__env->make('partials.cardgame.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('styles'); ?>
    </head>
    <body class="theme-light" data-background="none" data-highlight="red2">
    	<div id="page">
			<div id="page-preloader">
				<div class="loader-main"><div class="preload-spinner border-highlight"></div></div>
			</div>
			<?php echo $__env->make('partials.cardgame.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<?php echo $__env->make('partials.cardgame.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        	<?php echo $__env->yieldContent('content'); ?>
            <div class="page-bg" style="background-color: #fff">
                <div></div>
            </div>
        </div>
        <div class="menu-hider"></div>
        <?php echo $__env->make('partials.cardgame.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('scripts'); ?>
    </body>
</html>