<div id="menu-2" class="menu-sidebar menu-dark menu-sidebar-right menu-sidebar-reveal">
            <div class="menu-scroll">
               <div class="sidebar-icons">
                  <?php if(Sentinel::check()): ?>
                     <a href="#" style="width:80%; float:left; text-align:left"><span style="margin-left:20px"><?php echo app('translator')->getFromJson('general.reference'); ?> ID: <?php echo e(Sentinel::getUser()->id); ?></span></a>
                  <?php endif; ?>
                  <a href="#" class="close-menu" style="float:right"><i class="font-10 fa-fw fa fa-times"></i></a>
               </div>
               <div class="sidebar-separator"><?php echo app('translator')->getFromJson('general.your_informations'); ?></div>
               <div class="menu-items menu-icons">
                  <a href="<?php echo e(url('my-account')); ?>" class="<?php echo e(Request::segment(1) === 'my-account' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-user"></i><?php echo app('translator')->getFromJson('general.my_account'); ?></em></a>
                  <a href="<?php echo e(url('my-safe')); ?>" class="<?php echo e(Request::segment(1) === 'my-safe' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-university"></i><?php echo app('translator')->getFromJson('general.my_safe'); ?></em></a>
                  <a href="<?php echo e(url('transactions')); ?>" class="<?php echo e(Request::segment(1) === 'transactions' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-exchange-alt"></i><?php echo app('translator')->getFromJson('general.transactions'); ?></em></a>
                  <a href="<?php echo e(url('opened-boxes')); ?>" class="<?php echo e(Request::segment(1) === 'opened-boxes' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-box-open"></i><?php echo app('translator')->getFromJson('general.opened_boxes'); ?></em></a>
                  <!--
                  <a href="<?php echo e(url('adreslerim')); ?>"><em><i class="fas fa-map-marker-alt"></i>Adreslerim</em></a>
                  <a href="<?php echo e(url('siparislerim')); ?>"><em><i class="fas fa-box"></i>Siparişlerim</em></a>
                  <a href="<?php echo e(url('favorilerim')); ?>"><em><i class="fas fa-heart"></i>Favorilerim</em></a>
                  -->
                  <a href="<?php echo e(url('logout')); ?>"><em><i class="fas fa-sign-out-alt"></i><?php echo app('translator')->getFromJson('general.logout'); ?></em></a>
               </div>
               <!--
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">Bize Ulaşın</div>
               <div class="menu-items menu-icons">
                  <a href="tel:+1 234 567 890"><em><i class="fa fa-phone"></i>İletişim</em></a>
                  <a href="sms:+1 234 567 890"><em><i class="fa fa-comment"></i>Mesaj</em></a>
                  <a href="mailto:info@bydukkan.com"><em><i class="fa fa-envelope"></i>E-Posta</em></a>
               </div>
               -->
               <!--
               <div class="sidebar-divider"></div>
               <p class="sidebar-copyright" style="text-align:center">Copyright <span class="copyright-year"></span> byDukkan.</p>
               -->
            </div>
         </div>