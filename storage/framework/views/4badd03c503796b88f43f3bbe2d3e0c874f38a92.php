        <div id="menu-1" class="menu-sidebar menu-dark menu-sidebar-left menu-sidebar-reveal">
            <div class="menu-scroll">
               <div class="sidebar-icons">
                  <a href="#"><i class="font-10 fa-fw fab fa-facebook-f" style="color:<?php echo e(session('color', '#fff')); ?>"></i></a>
                  <a href="https://twitter.com/luckyboxfun"><i class="font-10 fa-fw fab fa-twitter" style="color:<?php echo e(session('color', '#fff')); ?>"></i></a>
                  <a href="https://instagram.com/luckybox.fun"><i class="font-10 fa-fw fab fa-instagram" style="color:<?php echo e(session('color', '#fff')); ?>"></i></a>
                  <a href="mailto:info@luckybox.fun"><i class="font-11 fa-fw far fa-envelope" style="color:<?php echo e(session('color', '#fff')); ?>"></i></a>
                  <a href="#" class="close-menu"><i class="font-10 fa-fw fa fa-times" style="color:<?php echo e(session('color', '#fff')); ?>"></i></a>
               </div>
               <!--
               <div class="sidebar-logo">
                  <a href="#"></a>
                  <em>Powerful Mobile Solutions</em>
               </div>
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">Menü</div>
               -->
               <div class="menu-items menu-icons">
                  <?php if(Sentinel::check() && Sentinel::getUser()->purchasing == 1 && Sentinel::getUser()->country != "Turkey (+90)"): ?>
                  <?php $deposit = App\Transactions::where('user_id', Sentinel::getUser()->id)->where('type', 2)->where('status', 1)->sum('amount');
                  $withdraw = App\Transactions::where('user_id', Sentinel::getUser()->id)->where('type', 1)->where('status', 1)->sum('amount'); ?>
                     <?php if($deposit - $withdraw <= 30 && Sentinel::getUser()->ticket <= 11 && Sentinel::getUser()->balance <= 20): ?>
                        <a href="<?php echo e(url('/premium-membership')); ?>" class="<?php echo e(Request::segment(1) == 'premium-membership' ? 'menu-item-active' : null); ?>"><i class="fas fa-hand-holding-usd" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.premium_membership'); ?></a>
                     <?php endif; ?>
                  <?php endif; ?>
                  <a href="<?php echo e(url('/')); ?>" class="<?php echo e(Request::segment(1) == null ? 'menu-item-active' : null); ?>"><i class="fa fa-home" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.homepage'); ?></a>
                  <a href="<?php echo e(url('/feel-lucky')); ?>" class="<?php echo e(Request::segment(1) === 'room' ? 'menu-item-active' : null); ?>"><i class="fa fa-star" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.feel_lucky'); ?></a>
                  <?php if(Sentinel::check() && Sentinel::getUser()->currency == "try"): ?>
                  <a href="<?php echo e(url('/turkey-rooms')); ?>" class="<?php echo e(Request::segment(1) === 'turkey-rooms' ? 'menu-item-active' : null); ?>"><i class="far fa-moon" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.turkey_rooms'); ?></a>
                  <?php endif; ?>
                  <a href="<?php echo e(url('/europe-rooms')); ?>" class="<?php echo e(Request::segment(1) === 'europe-rooms' ? 'menu-item-active' : null); ?>"><i class="fas fa-chess-king" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.europe_rooms'); ?></a>
                  <a href="<?php echo e(url('/world-rooms')); ?>" class="<?php echo e(Request::segment(1) === 'world-rooms' ? 'menu-item-active' : null); ?>"><i class="fas fa-globe" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.world_rooms'); ?></a>
                  <?php if((session('browser', 'unknown') == "android" || session('browser', 'unknown') == "ios") && Sentinel::check()): ?>
                  <a href="<?php echo e(url('/awarded-videos')); ?>" class="<?php echo e(Request::segment(1) === 'awarded-videos' ? 'menu-item-active' : null); ?>"><i class="fas fa-ticket-alt" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.award_winning_videos'); ?></a>
                  <a href="<?php echo e(url('/awarded-tasks')); ?>" class="<?php echo e(Request::segment(1) === 'awarded-tasks' ? 'menu-item-active' : null); ?>"><i class="fas fa-tasks" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.award_winning_tasks'); ?></a>
                  <!--<a href="<?php echo e(url('/spin-win')); ?>" class="<?php echo e(Request::segment(1) === 'spin-win' ? 'menu-item-active' : null); ?>"><i class="fas fa-adjust" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.spin_win'); ?></a>-->
                  <a href="<?php echo e(url('/scratch-win')); ?>" class="<?php echo e(Request::segment(1) === 'scratch-win' ? 'menu-item-active' : null); ?>"><i class="fas fa-adjust" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.scratch_win'); ?></a>
                  <?php endif; ?>
                  <a href="<?php echo e(url('/rewards')); ?>" class="<?php echo e(Request::segment(1) === 'rewards' ? 'menu-item-active' : null); ?>"><i class="fas fa-trophy" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.rewards'); ?></a>
                  <a href="<?php echo e(url('/walkthrough')); ?>" class="<?php echo e(Request::segment(1) === 'walkthrough' ? 'menu-item-active' : null); ?>"><i class="fas fa-question" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.help'); ?></a>
                  <!--<a href="<?php echo e(url('/')); ?>"><i class="fa fa-envelope"></i>İletişim</a>-->
               </div>
               <!--
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">İletişim</div>
               <div class="menu-items menu-icons">
                  <a href="tel:+1 234 567 890"><em><i class="fa fa-phone"></i>Arayın</em></a>
                  <a href="sms:+1 234 567 890"><em><i class="fa fa-comment"></i>Mesaj</em></a>
                  <a href="mailto:yourname@domain.com"><em><i class="fa fa-envelope"></i>E-Posta</em></a>
               </div>
               -->
               <div class="sidebar-divider"></div>
               <p class="sidebar-copyright" style="text-align:center">Copyright <span class="copyright-year"></span> <?php echo app('translator')->getFromJson('general.luckybox'); ?></p>
            </div>
         </div>