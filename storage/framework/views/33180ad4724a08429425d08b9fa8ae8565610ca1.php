<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <table class="table-borders-dark">
        <tr>
            <th><?php echo app('translator')->getFromJson('general.username'); ?></th>
            <th><?php echo app('translator')->getFromJson('general.date'); ?></th>
        </tr>
        <?php $__empty_1 = true; $__currentLoopData = $references; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <tr>
            <td><?php echo e($val->first_name); ?></td>
            <td><?php echo e(Carbon\Carbon::parse($val->created_at)->format('d M H:i')); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <tr>
            <td colspan ="4"><?php echo app('translator')->getFromJson('general.you_have_no_reference_members'); ?></td>
        </tr>
        <?php endif; ?>
    </table>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>