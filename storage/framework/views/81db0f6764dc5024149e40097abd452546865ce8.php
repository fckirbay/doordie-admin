<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
         <div class="page-content header-clear-larger">
            <div id="page-404">
               <h1 class="center-text bold">500</h1>
               <h4 class="center-text uppercase ultrabold bottom-30">Error</h4>
               <!--<h1 class="center-text bold">-</h1>
               <h4 class="center-text uppercase ultrabold bottom-30"><?php echo app('translator')->getFromJson('general.sorry'); ?></h4>-->

               <p class="center-text bottom-30">
                  Internal server error... How about going back home?
                  <!--<?php echo app('translator')->getFromJson('general.the_system_is_being_updated'); ?>-->
               </p>
               <a href="<?php echo e(url('/')); ?>" class="button bg-highlight button-s button-center button-rounded uppercase ultrabold">Home</a>
            </div>
         </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>