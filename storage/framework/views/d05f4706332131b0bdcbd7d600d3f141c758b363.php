<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="page-content header-clear-medium">
	<div data-height="130" class="caption caption-margins round-medium shadow-huge" style="height: 130px;">
		<div class="caption-center right-15 top-15 text-right">
			<a href="#" class="back-button button button-xs button-round-huge bg-highlight">Back Home</a>
		</div>
		<div class="caption-center left-15 text-left">
			<h1 class="color-white bolder">Inputs</h1>
			<p class="under-heading color-white opacity-90 bottom-0">
				Text, Email, Password and More.
			</p>
		</div>
		<div class="caption-overlay bg-black opacity-70"></div>
		<div class="caption-bg bg-18"></div>
	</div>
	<div class="content-boxed">
		<p class="content bottom-25">
			Input fileds for text, email, password, website, text area, range sliders and much more.
		</p>
	</div>
	<div class="content-boxed">
<div class="content">
<h3 class="bolder">Checkboxes</h3>
<p>Default Checkboxes, styled with FontAwesome</p>
<div class="one-half">
<div class="checkboxes-demo">
<div class="fac fac-checkbox fac-default"><span></span>
<input id="box1-fac-checkbox" type="checkbox" value="1" checked="">
<label for="box1-fac-checkbox">Simple</label>
</div>
<div class="fac fac-checkbox fac-blue"><span></span>
<input id="box2-fac-checkbox" type="checkbox" value="1" checked="">
<label for="box2-fac-checkbox">Primary</label>
</div>
<div class="fac fac-checkbox fac-green"><span></span>
<input id="box3-fac-checkbox" type="checkbox" value="1" checked="">
<label for="box3-fac-checkbox">Success</label>
</div>
<div class="fac fac-checkbox fac-orange"><span></span>
<input id="box5-fac-checkbox" type="checkbox" value="1" checked="">
<label for="box5-fac-checkbox">Warning</label>
</div>
<div class="fac fac-checkbox fac-red"><span></span>
<input id="box6-fac-checkbox" type="checkbox" value="1" checked="">
<label for="box6-fac-checkbox">Danger</label>
</div>
</div>
</div>
<div class="one-half last-column">
<div class="checkboxes-demo">
<div class="fac fac-checkbox-round fac-default"><span></span>
<input id="box1-fac-checkbox-round" type="checkbox" value="1" checked="">
<label for="box1-fac-checkbox-round">Simple</label>
</div>
<div class="fac fac-checkbox-round fac-blue"><span></span>
<input id="box2-fac-checkbox-round" type="checkbox" value="1" checked="">
<label for="box2-fac-checkbox-round">Primary</label>
</div>
<div class="fac fac-checkbox-round fac-green"><span></span>
<input id="box3-fac-checkbox-round" type="checkbox" value="1" checked="">
<label for="box3-fac-checkbox-round">Success</label>
</div>
<div class="fac fac-checkbox-round fac-orange"><span></span>
<input id="box5-fac-checkbox-round" type="checkbox" value="1" checked="">
<label for="box5-fac-checkbox-round">Warning</label>
</div>
<div class="fac fac-checkbox-round fac-red"><span></span>
<input id="box6-fac-checkbox-round" type="checkbox" value="1" checked="">
<label for="box6-fac-checkbox-round">Danger</label>
</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>
	
	
	
	<div class="clear"></div>
	<div class="content-boxed">
		<div class="content">
			<h3 class="bolder">Modern Fields</h3>
			<p>
				These boxes will react to them when you type or select a value.
			</p>
			<div class="input-style input-style-2 has-icon input-required">
				<i class="input-icon fa fa-user"></i>
				<span>Name</span>
				<em>(required)</em>
				<input type="name" placeholder="">
			</div>
			<div class="input-style input-style-2 input-required">
				<span>Email</span>
				<em>(required)</em>
				<input type="email" placeholder="">
			</div>
			<div class="input-style input-style-2 input-required">
				<span>Password</span>
				<em>(required)</em>
				<input type="password" placeholder="">
			</div>
			<div class="input-style input-style-2 input-required">
				<span>Website</span>
				<em>(required)</em>
				<input type="url" placeholder="">
			</div>
			<div class="input-style input-style-2 input-required">
				<span>Phone</span>
				<em>(required)</em>
				<input type="tel" placeholder="">
			</div>
			<div class="input-style input-style-2 input-required">
				<span>Select a Value</span>
				<em><i class="fa fa-angle-down"></i></em>
				<select>
					<option value="default" disabled="" selected="">Select a Value</option>
					<option value="iOS">iOS</option>
					<option value="Linux">Linux</option>
					<option value="MacOS">MacOS</option>
					<option value="Android">Android</option>
					<option value="Windows">Windows</option>
				</select>
			</div>
			<div class="input-style input-style-2 input-required">
				<span>Enter your Message</span>
				<em>(required)</em>
				<textarea placeholder=""></textarea>
			</div>
		</div>
	</div>
	<div class="content-boxed">
		<div class="content">
			<h3 class="bolder">Minimalist Fields</h3>
			<p>
				These boxes will react to them when you type or select a value.
			</p>
			<div class="input-style has-icon input-style-1 input-required">
				<i class="input-icon fa fa-user"></i>
				<span class="input-style-1-inactive">Name</span>
				<em>(required)</em>
				<input type="name" placeholder="Name">
			</div>
			<div class="input-style input-style-1 input-required">
				<span>Email</span>
				<em>(required)</em>
				<input type="email" placeholder="Email">
			</div>
			<div class="input-style input-style-1 input-required">
				<span>Password: Minimum 8 Characters and 1 Number</span>
				<em>(required)</em>
				<input type="password" placeholder="Password">
			</div>
			<div class="input-style input-style-1 input-required">
				<span>Website</span>
				<em>(required)</em>
				<input type="url" placeholder="Website">
			</div>
			<div class="input-style input-style-1 input-required">
				<span>Phone</span>
				<em>(required)</em>
				<input type="tel" placeholder="Phone">
			</div>
			<div class="input-style input-style-1 input-required">
				<span>Select a Value</span>
				<em><i class="fa fa-angle-down"></i></em>
				<select>
					<option value="default" disabled="" selected="">Select a Value</option>
					<option value="iOS">iOS</option>
					<option value="Linux">Linux</option>
					<option value="MacOS">MacOS</option>
					<option value="Android">Android</option>
					<option value="Windows">Windows</option>
				</select>
			</div>
			<div class="input-style input-style-1 input-required">
				<span>Enter your message</span>
				<em>(required)</em>
				<textarea placeholder="Enter your message"></textarea>
			</div>
			<div class="input-style input-style-1 input-required">
				<span>Select your Birthday</span>
				<em><i class="fa fa-angle-down"></i></em>
				<input type="date" value="1980-08-26">
			</div>
		</div>
	</div>
	<div class="content-boxed">
		<div class="content">
			<h3 class="bolder">Range Sliders</h3>
			<p>
				Slide and increase or decrease their value.
			</p>
			<form class="range-slider bottom-15 range-slider-icons">
				<i class="fa fa-range-icon-1 fa-volume-down color-theme"></i>
				<i class="fa fa-range-icon-2 fa-volume-up color-theme"></i>
				<input class="ios-slider" type="range" value="50">
			</form>
			<form class="range-slider bottom-15 ">
				<input class="classic-slider" type="range" value="50">
			</form>
			<form class="range-slider">
				<input class="material-slider" type="range" value="50">
			</form>
		</div>
	</div>
	<div class="content-boxed">
		<div class="footer">
			<a href="#" class="footer-title"><span class="color-highlight">StickyMobile</span></a>
			<p class="footer-text"><span>Made with <i class="fa fa-heart color-highlight font-16 left-10 right-10"></i> by Enabled</span><br><br>Powered by the best Mobile Website Developer on the Envato Marketplaces. Elite Quality. Elite Products.</p>
			<div class="footer-socials">
				<a href="#" class="round-tiny shadow-medium bg-facebook"><i class="fab fa-facebook-f"></i></a>
				<a href="#" class="round-tiny shadow-medium bg-twitter"><i class="fab fa-twitter"></i></a>
				<a href="#" class="round-tiny shadow-medium bg-phone"><i class="fa fa-phone"></i></a>
				<a href="#" data-menu="menu-share" class="round-tiny shadow-medium bg-red2-dark"><i class="fa fa-share-alt"></i></a>
				<a href="#" class="back-to-top round-tiny shadow-medium bg-dark1-light"><i class="fa fa-angle-up"></i></a>
			</div>
			<div class="clear"></div>
			<p class="footer-copyright">Copyright © Enabled <span id="copyright-year">2019</span>. All Rights Reserved.</p>
			<p class="footer-links"><a href="#" class="color-highlight">Privacy Policy</a> | <a href="#" class="color-highlight">Terms and Conditions</a> | <a href="#" class="back-to-top color-highlight"> Back to Top</a></p>
			<div class="clear"></div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.cardgame.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>