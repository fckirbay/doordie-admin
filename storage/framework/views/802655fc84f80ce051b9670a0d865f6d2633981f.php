        <div id="menu-1" class="menu-sidebar menu-dark menu-sidebar-left menu-sidebar-reveal">
            <div class="menu-scroll">
               <div class="sidebar-icons">
                  <a href="#"><i class="font-10 fa-fw fab fa-facebook-f"></i></a>
                  <a href="https://twitter.com/luckyboxfun"><i class="font-10 fa-fw fab fa-twitter"></i></a>
                  <a href="https://instagram.com/luckyboxfun"><i class="font-10 fa-fw fab fa-instagram"></i></a>
                  <a href="mailto:info@luckybox.fun"><i class="font-11 fa-fw far fa-envelope"></i></a>
                  <a href="#" class="close-menu"><i class="font-10 fa-fw fa fa-times"></i></a>
               </div>
               <!--
               <div class="sidebar-logo">
                  <a href="#"></a>
                  <em>Powerful Mobile Solutions</em>
               </div>
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">Menü</div>
               -->
               <div class="menu-items menu-icons">
                  
                  <a href="<?php echo e(url('/')); ?>" class="<?php echo e(Request::segment(1) == null ? 'menu-item-active' : null); ?>"><i class="fa fa-home"></i><?php echo app('translator')->getFromJson('general.homepage'); ?></a>
                  <a href="<?php echo e(url('/feel-lucky')); ?>" class="<?php echo e(Request::segment(1) === 'room' ? 'menu-item-active' : null); ?>"><i class="fa fa-star"></i><?php echo app('translator')->getFromJson('general.feel_lucky'); ?></a>
                  <?php if(Sentinel::check() && Sentinel::getUser()->currency == "try"): ?>
                  <a href="<?php echo e(url('/turkey-rooms')); ?>" class="<?php echo e(Request::segment(1) === 'turkey-rooms' ? 'menu-item-active' : null); ?>"><i class="far fa-moon"></i><?php echo app('translator')->getFromJson('general.turkey_rooms'); ?></a>
                  <?php endif; ?>
                  <a href="<?php echo e(url('/europe-rooms')); ?>" class="<?php echo e(Request::segment(1) === 'europe-rooms' ? 'menu-item-active' : null); ?>"><i class="fas fa-chess-king"></i><?php echo app('translator')->getFromJson('general.europe_rooms'); ?></a>
                  <a href="<?php echo e(url('/world-rooms')); ?>" class="<?php echo e(Request::segment(1) === 'world-rooms' ? 'menu-item-active' : null); ?>"><i class="fas fa-globe"></i><?php echo app('translator')->getFromJson('general.world_rooms'); ?></a>
                  <a href="<?php echo e(url('/rewards')); ?>" class="<?php echo e(Request::segment(1) === 'rewards' ? 'menu-item-active' : null); ?>"><i class="fas fa-trophy"></i><?php echo app('translator')->getFromJson('general.rewards'); ?></a>
                  <a href="<?php echo e(url('/spin-win')); ?>" class="<?php echo e(Request::segment(1) === 'spin-win' ? 'menu-item-active' : null); ?>"><i class="fas fa-chess-queen"></i><?php echo app('translator')->getFromJson('general.spin_win'); ?></a>
                  <!--<a href="<?php echo e(url('/')); ?>"><i class="fa fa-envelope"></i>İletişim</a>-->
               </div>
               <!--
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">İletişim</div>
               <div class="menu-items menu-icons">
                  <a href="tel:+1 234 567 890"><em><i class="fa fa-phone"></i>Arayın</em></a>
                  <a href="sms:+1 234 567 890"><em><i class="fa fa-comment"></i>Mesaj</em></a>
                  <a href="mailto:yourname@domain.com"><em><i class="fa fa-envelope"></i>E-Posta</em></a>
               </div>
               -->
               <div class="sidebar-divider"></div>
               <p class="sidebar-copyright" style="text-align:center">Copyright <span class="copyright-year"></span> <?php echo app('translator')->getFromJson('general.luckybox'); ?></p>
            </div>
         </div>