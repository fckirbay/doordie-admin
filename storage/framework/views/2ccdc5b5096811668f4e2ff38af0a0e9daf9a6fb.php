<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-android"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="#" style="color:#000">Android</a></span>
              <span class="info-box-number"><?php if(isset($os['android'])): ?><?php echo e($os['android']); ?><?php else: ?> 0 <?php endif; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-apple"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="#" style="color:#000">iOS</a></span>
              <span class="info-box-number"><?php if(isset($os['ios'])): ?><?php echo e($os['ios']); ?><?php else: ?> 0 <?php endif; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-question"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="#" style="color:#000">Unknown</a></span>
              <span class="info-box-number"><?php if(isset($os[''])): ?><?php echo e($os['']); ?><?php else: ?> 0 <?php endif; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="<?php echo e(url('panel/users')); ?>" style="color:#000">Onaylı</a></span>
              <span class="info-box-number"><?php if(isset($users[1])): ?><?php echo e($users[1]); ?><?php else: ?> 0 <?php endif; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-times"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="<?php echo e(url('panel/application/bets')); ?>" style="color:#000">Onaysız</a></span>
              <span class="info-box-number"><?php if(isset($users[0])): ?><?php echo e($users[0]); ?><?php else: ?> 0 <?php endif; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-black"><i class="fa fa-ban"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="#" style="color:#000">Engellenen</a></span>
              <span class="info-box-number"><?php echo e($blockeds); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ülkeler</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>#</th>
                  <th style="width:40%">Ülke</th>
                  <th class="orta" style="width:50%">Üye Sayısı</th>
                </tr>
                <?php $__empty_1 = true; $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr>
                  <td><?php if($val->country != null): ?><img src="<?php echo e(asset('assets/cardgame/images/flags/'.strtolower($val->country).'.svg')); ?>" style="width:30px; height:20px"/><?php else: ?><img src="<?php echo e(asset('assets/mobile/images/flags/un.png')); ?>"/><?php endif; ?></td>
                  <td><?php echo e($val->country); ?></td>
                  <td class="orta"><?php echo e($val->total); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                <?php endif; ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

      
    </section>
    <!-- /.content -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<!-- Sparkline -->
<script src="<?php echo e(asset('assets/admin/plugins/jquery-sparkline/dist/jquery.sparkline.min.js')); ?>"></script>
<!-- jvectormap  -->
<script src="<?php echo e(asset('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo e(asset('assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- ChartJS -->
<script src="<?php echo e(asset('assets/admin/plugins/chart.js/Chart.js')); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo e(asset('assets/admin/js/pages/dashboard2.js')); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(asset('assets/admin/js/demo.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>