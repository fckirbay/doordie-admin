<!DOCTYPE html>
<html lang="tr">
    <head>
    <?php echo $__env->make('partials.admin.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('styles'); ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php echo $__env->make('partials.admin.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('partials.admin.side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="content-wrapper">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
            <?php echo $__env->make('partials.admin.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="control-sidebar-bg"></div>
        </div>
        <?php echo $__env->make('partials.admin.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('scripts'); ?>
        
        <?php if(session('permission_denied')): ?>
        <script>
        	swal("Yetkiniz yok!", "", "error");
        </script>
        <?php endif; ?>
        
    </body>
</html>