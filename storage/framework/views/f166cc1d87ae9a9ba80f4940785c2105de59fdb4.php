<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Destek</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:5%">Kullanıcı ID</th>
                  <th style="width:20%">Mesaj</th>
                  <th class="orta" style="width:10%">Tarih</th>
                  <th class="orta" style="width:10%">#</th>
                </tr>
                <?php $__empty_1 = true; $__currentLoopData = $supports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr <?php if($val->is_viewed == 1): ?> style="color:gray" <?php endif; ?>>
                  <td><?php if($val->country != null): ?><img src="<?php echo e(asset('assets/cardgame/images/flags/'.strtolower($val->country).'.svg')); ?>" style="width:30px; height:20px"/><?php endif; ?></td>
                  <td><a href="<?php echo e(url('admin/user-management/user/'.$val->user_id.'?chat=1')); ?>" <?php if($val->is_viewed == 1): ?> style="color:gray" <?php else: ?> style="color:black" <?php endif; ?>><?php echo e($val->username); ?></a></td>
                  <td><?php echo e($val->message); ?></td>
                  <td class="orta"><?php echo e(Carbon\Carbon::parse($val->date)->format('d M - H:i:s')); ?></td>
                  <td class="orta"><?php if($val->is_viewed == 0): ?><a class="btn btn-danger btn-xs" href="<?php echo e(url('admin/delete-support', $val->id)); ?>">Sil</a><?php endif; ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                <?php endif; ?>
              </table>
            </div>
            <?php echo e($supports->links()); ?>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>