<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tamamlanan Görevler</h3>
              <?php echo Form::open(['url'=>'admin/offerwall', 'method' => 'get', 'style' => 'float:right', 'id'=>'search-form']); ?>

              <div class="box-tools">

                <div class="row">
                  <div class="col-xs-5">
                    <select class="form-control" name="order">
                      <option value="sum">Kazanç</option>
                      <option value="total">Sayı</option>
                      <option value="complete_date">Tarih</option>
                    </select>
                  </div>
                  <div class="col-xs-5">
                    <select class="form-control" name="days">
                      <option value="1095">Tüm Zamanlar</option>
                      <option value="3">Son 3 Gün</option>
                      <option value="7" selected>Son 1 Hafta</option>
                      <option value="14">Son 2 Hafta</option>
                      <option value="30">Son 1 Ay</option>
                      <option value="60">Son 2 Ay</option>
                      <option value="90">Son 3 Ay</option>
                      <option value="180">Son 6 Ay</option>
                      <option value="365">Son 1 Yıl</option>
                    </select>
                  </div>
                  <div class="col-xs-2">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
              <?php echo Form::close(); ?>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:25%">Kullanıcı ID</th>
                  <th class="orta" style="width:10%">Görevler</th>
                  <th class="orta" style="width:10%">Toplam ($)</th>
                </tr>
                <?php $__empty_1 = true; $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr>
                  <td><a href="<?php echo e(url('admin/user-management/user', $val->user_id)); ?>"><?php echo e($val->username); ?></a></td>
                  <td class="orta"><?php echo e($val->total); ?></td>
                  <td class="orta">$<?php echo e($val->sum); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="3" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                <?php endif; ?>
              </table>
            </div>
            <?php echo $offers->appends(Request::capture()->except('page'))->render(); ?>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>