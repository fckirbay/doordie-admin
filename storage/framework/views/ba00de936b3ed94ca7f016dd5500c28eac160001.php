<?php $__env->startSection('styles'); ?>
<link href="<?php echo e(asset('assets/theme/css/lightbox.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class='row single-product'>
   <div class='col-md-3 sidebar'>
      <div class="sidebar-module-container">
         <div class="home-banner outer-top-n">
            <img src="<?php echo e(asset('assets/theme/images/banners/LHS-banner.jpg')); ?>" alt="Image">
         </div>
         <br />
         <?php echo $__env->make('partials.theme.month-deals', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
   </div>
   <!-- /.sidebar -->
   <div class='col-md-9'>
      <div class="detail-block">
         <div class="row  wow fadeInUp">
            <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
               <div class="product-item-holder size-big single-product-gallery small-gallery">
                  <div id="owl-single-product">
                     <?php $__empty_1 = true; $__currentLoopData = $photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                     <div class="single-product-gallery-item" id="slide<?php echo e($key + 1); ?>">
                        <a data-lightbox="image-1" data-title="Gallery" href="<?php echo e(asset($val->photo)); ?>">
                        <img class="img-responsive" alt="" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" data-echo="<?php echo e(asset($val->photo)); ?>" />
                        </a>
                     </div>
                     <!-- /.single-product-gallery-item -->
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                     <?php endif; ?>
                  </div>
                  <!-- /.single-product-slider -->
                  <div class="single-product-gallery-thumbs gallery-thumbs">
                     <div id="owl-single-product-thumbnails">
                        <?php $__empty_1 = true; $__currentLoopData = $photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="item">
                           <a class="horizontal-thumb <?php if($key == 0): ?> active <?php endif; ?>" data-target="#owl-single-product" data-slide="<?php echo e($key + 1); ?>" href="#slide<?php echo e($key + 1); ?>">
                           <img class="img-responsive" width="85" alt="" src="<?php echo e(asset('assets/theme/images/blank.gif')); ?>" data-echo="<?php echo e(asset($val->photo)); ?>" />
                           </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <?php endif; ?>
                     </div>
                     <!-- /#owl-single-product-thumbnails -->
                  </div>
                  <!-- /.gallery-thumbs -->
               </div>
               <!-- /.single-product-gallery -->
            </div>
            <!-- /.gallery-holder -->        			
            <div class='col-sm-6 col-md-7 product-info-block'>
               <div class="product-info">
                  <h1 class="name"><?php echo e($product->title); ?></h1>
                  <div class="rating-reviews m-t-20">
                     <div class="row">
                        <div class="col-sm-3">
                           <div class="rating rateit-small" data-rateit-value="<?php echo e(intval($rate)); ?>"></div>
                        </div>
                        <div class="col-sm-8">
                           <div class="reviews">
                              <a href="#" class="lnk">(<?php echo e(count($comments)); ?> Yorum)</a>
                           </div>
                        </div>
                     </div>
                     <!-- /.row -->		
                  </div>
                  <!-- /.rating-reviews -->
                  <div class="stock-container info-container m-t-10">
                     <div class="row">
                        <div class="col-sm-2">
                           <div class="stock-box">
                              <span class="label">Marka :</span>
                           </div>
                           <br />
                        </div>
                        <div class="col-sm-9">
                           <div class="stock-box">
                              <span class="value"><?php echo e($brand->brand); ?></span>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-2">
                           <div class="stock-box">
                              <span class="label">Stok :</span>
                           </div>
                           <br />
                        </div>
                        <div class="col-sm-9">
                           <div class="stock-box">
                              <span class="value"><?php if($product->stock > 0): ?><?php echo e($product->stock); ?><?php else: ?> Tükendi <?php endif; ?></span>
                           </div>
                        </div>
                     </div>
                     <!-- /.row -->	
                  </div>
                  <!-- /.stock-container -->
                  <div class="description-container m-t-20">
                     <?php echo e($product->subtitle); ?>

                  </div>
                  <!-- /.description-container -->
                  <div class="price-container info-container m-t-20">
                     <div class="row">
                        <div class="col-sm-6">
                           <div class="price-box">
                              <span class="price"><?php echo e(number_format($product->price, 2, ',', '.')); ?>₺</span>
                              <?php if(Sentinel::check() && Sentinel::inRole('admin')): ?>
                              <span class="price" style="color:red"><?php echo e(number_format($product->purchase_price, 2, ',', '.')); ?>₺</span>
                              <?php endif; ?>
                              <?php if($product->old_price > 0): ?>
                              <span class="price-strike"><?php echo e(number_format($product->old_price, 2, ',', '.')); ?>₺</span>
                              <?php endif; ?>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="favorite-button m-t-10">
                              <a class="btn btn-primary add-wishlist-button" data-id="<?php echo e($product->id); ?>" data-toggle="tooltip" data-placement="right" title="Favorilerime Ekle">
                              <i class="fa fa-heart"></i>
                              </a>
                              <a class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Paylaş" href="#">
                              <i class="fa fa-share-alt"></i>
                              </a>
                           </div>
                        </div>
                     </div>
                     <!-- /.row -->
                  </div>
                  <!-- /.price-container -->
                  <div class="quantity-container info-container">
                     <div class="row">
                        <div class="col-sm-2">
                           <span class="label">Adet :</span>
                        </div>
                        <div class="col-sm-2">
                           <div class="cart-quantity">
                              <div class="quant-input">
                                 <div class="arrows">
                                    <div class="arrow plus gradient"><span class="ir"><i class="icon fa fa-sort-asc"></i></span></div>
                                    <div class="arrow minus gradient"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></div>
                                 </div>
                                 <input type="text" id="qty" name="qty" value="1">
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-7">
                           <a class="btn btn-primary add-cart-button" data-id="<?php echo e($product->id); ?>"><i class="fa fa-shopping-cart inner-right-vs"></i> SEPETE EKLE</a>
                        </div>
                     </div>
                     <!-- /.row -->
                  </div>
                  <!-- /.quantity-container -->
               </div>
               <!-- /.product-info -->
            </div>
            <!-- /.col-sm-7 -->
         </div>
         <!-- /.row -->
      </div>
      <div class="product-tabs inner-bottom-xs  wow fadeInUp">
         <div class="row">
            <div class="col-sm-3">
               <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                  <li class="active"><a data-toggle="tab" href="#description">AÇIKLAMA</a></li>
                  <li><a data-toggle="tab" href="#installments">TAKSİTLER</a></li>
                  <li><a data-toggle="tab" href="#review">YORUMLAR</a></li>
               </ul>
               <!-- /.nav-tabs #product-tabs -->
            </div>
            <div class="col-sm-9">
               <div class="tab-content">
                  <div id="description" class="tab-pane in active">
                     <div class="product-tab">
                        <p class="text"><?php echo $product->description; ?></p>
                     </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div id="review" class="tab-pane">
                     <div class="product-tab">
                        <div class="product-reviews">
                           <h4 class="title">Son Yorumlar</h4>
                           <?php $__empty_1 = true; $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                           <?php $rate = intval(($val->quality + $val->price + $val->shipping) / 3); ?>
                           <div class="reviews">
                              <div class="review">
                                 <div class="review-title"><span class="summary"><?php for($i = 0; $i < $rate; $i++): ?><i class="fa fa-star"></i><?php endfor; ?></span><span class="date"><i class="fa fa-calendar"></i><span><?php Carbon\Carbon::setLocale('tr'); ?> <?php echo e(Carbon\Carbon::parse($val->created_at)->diffForHumans()); ?></span></span></div>
                                 <div class="text"><?php echo e($val->comment); ?></div>
                              </div>
                           </div>
                           <!-- /.reviews -->
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                           <div class="reviews" style="text-align:center">
                              <div class="review">
                                 Bu ürün hakkında henüz yorum yapılmadı.
                              </div>
                           </div>
                           <?php endif; ?>
                        </div>
                        <!-- /.product-reviews -->
                        <div class="product-add-review">
                           <h4 class="title">Ürün hakkında yorum yapın!</h4>
                           <div class="review-table">
                              <div class="table-responsive">
                                 <table class="table">
                                    <thead>
                                       <tr>
                                          <th class="cell-label">&nbsp;</th>
                                          <th><i class="fa fa-star"></i></th>
                                          <th><i class="fa fa-star"></i><i class="fa fa-star"></i></th>
                                          <th><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></th>
                                          <th><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></th>
                                          <th><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td class="cell-label">Ürün Kalitesi</td>
                                          <td><input type="radio" name="quality" class="radio" value="1"></td>
                                          <td><input type="radio" name="quality" class="radio" value="2"></td>
                                          <td><input type="radio" name="quality" class="radio" value="3"></td>
                                          <td><input type="radio" name="quality" class="radio" value="4"></td>
                                          <td><input type="radio" name="quality" class="radio" value="5"></td>
                                       </tr>
                                       <tr>
                                          <td class="cell-label">Ürün Fiyatı</td>
                                          <td><input type="radio" name="price" class="radio" value="1"></td>
                                          <td><input type="radio" name="price" class="radio" value="2"></td>
                                          <td><input type="radio" name="price" class="radio" value="3"></td>
                                          <td><input type="radio" name="price" class="radio" value="4"></td>
                                          <td><input type="radio" name="price" class="radio" value="5"></td>
                                       </tr>
                                       <tr>
                                          <td class="cell-label">Kargolama</td>
                                          <td><input type="radio" name="shipping" class="radio" value="1"></td>
                                          <td><input type="radio" name="shipping" class="radio" value="2"></td>
                                          <td><input type="radio" name="shipping" class="radio" value="3"></td>
                                          <td><input type="radio" name="shipping" class="radio" value="4"></td>
                                          <td><input type="radio" name="shipping" class="radio" value="5"></td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <!-- /.table .table-bordered -->
                              </div>
                              <!-- /.table-responsive -->
                           </div>
                           <!-- /.review-table -->
                           <div class="review-form">
                              <div class="form-container">
                                 <form role="form" class="cnt-form">
                                    <div class="row">
                                       <div class="col-md-23d">
                                          <div class="form-group">
                                             <label for="exampleInputReview">Yorumunuz <span class="astk">*</span></label>
                                             <textarea class="form-control txt txt-review" id="exampleInputReview" rows="4" placeholder="" minlength="5"></textarea>
                                          </div>
                                          <!-- /.form-group -->
                                       </div>
                                    </div>
                                    <!-- /.row -->
                                    <div class="action text-right">
                                       <button class="btn btn-primary btn-upper">YORUMU GÖNDER</button>
                                    </div>
                                    <!-- /.action -->
                                 </form>
                                 <!-- /.cnt-form -->
                              </div>
                              <!-- /.form-container -->
                           </div>
                           <!-- /.review-form -->
                        </div>
                        <!-- /.product-add-review -->										
                     </div>
                     <!-- /.product-tab -->
                  </div>
                  <!-- /.tab-pane -->
                  <div id="installments" class="tab-pane">
                     <div class="product-tag">
                        <h4 class="title">Taksit Tablosu</h4>
                     </div>
                     <!-- /.product-tab -->
                  </div>
                  <!-- /.tab-pane -->
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.product-tabs -->
      <!-- ============================================== UPSELL PRODUCTS ============================================== -->
      <section class="section featured-product wow fadeInUp">
         <h3 class="section-title">Benzer Ürünler</h3>
         <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">
            <?php $__empty_1 = true; $__currentLoopData = $similars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="item item-carousel">
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image">
                           <a href="<?php echo e(url($val->slug)); ?>"><img  src="<?php echo e(asset($val->photo)); ?>" class="manual" alt=""></a>
                        </div>
                        <!-- /.image -->			
                        <?php if($val->old_price > 0): ?>
                        <div class="tag sale" style="font-size:14px; color:#fff; background-color:#ff7878"><span>%<?php echo e(intval(($val->old_price - $val->price) / $val->old_price * 100)); ?></span></div>
                        <?php endif; ?>
                     </div>
                     <!-- /.product-image -->
                     <div class="product-info text-left center">
                        <h3 class="name"><a href="<?php echo e(url($val->slug)); ?>"><?php echo e($val->title); ?></a></h3>
                        <div class="rating rateit-small"></div>
                        <div class="description"></div>
                        <div class="product-price">	
                           <span class="price">
                           <?php echo e(number_format($val->price, 2, ',', '.')); ?>₺				</span>
                           <?php if($val->old_price > 0): ?>
                           <span class="price-before-discount"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
                           <?php endif; ?>
                        </div>
                        <!-- /.product-price -->
                     </div>
                     <!-- /.product-info -->
                     <div class="cart clearfix animate-effect">
                        <div class="action">
                           <ul class="list-unstyled">
                              <li class="lnk wishlist">
                                 <a data-toggle="tooltip" class="add-to-cart" href="<?php echo e(url($val->slug)); ?>">
                                 <i class="icon fa fa-search"></i> Ürünü İncele
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <!-- /.action -->
                     </div>
                     <!-- /.cart -->
                  </div>
                  <!-- /.product -->
               </div>
               <!-- /.products -->
            </div>
            <!-- /.item -->
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <?php endif; ?>
         </div>
         <!-- /.home-owl-carousel -->
      </section>
      <!-- /.section -->
      <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
   </div>
   <!-- /.col -->
   <div class="clearfix"></div>
</div>
<!-- /.row -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
   $( ".add-wishlist-button" ).click(function() {
         <?php if(!Sentinel::check()): ?>
       	swal({
   	       title: "Üzgünüz!",
   	       text: "Öncelikle giriş yapmalısınız!",
   	       timer: 1000,
   	       showConfirmButton: false,
   	       type: "error"
   	    });
   	    return false;
   	  <?php endif; ?>
         var id = $(this).data('id');
         $.ajax({
           url: 'favorilerime-ekle',
           method: 'POST',
           data:{
             id: id,
             _token: "<?php echo e(csrf_token()); ?>"
           },
           dataType: 'html',
           success: function(data) {
               var obj = jQuery.parseJSON( data);
               if(obj.status == "success") {
               	swal({
   	              title: "Tebrikler!",
   	              text: obj.msg,
   	              timer: 1000,
   	              showConfirmButton: false,
   	              type: "success"
   	            });
               } else {
               	swal({
   	              title: "Bilgi!",
   	              text: obj.msg,
   	              timer: 1000,
   	              showConfirmButton: false,
   	              type: "info"
               	});
               }
               
           }
         });
       });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>