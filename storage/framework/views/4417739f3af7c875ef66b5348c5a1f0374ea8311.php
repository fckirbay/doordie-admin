<header class="main-header">

    <!-- Logo -->
    <a href="<?php echo e(url('admin/dashboard')); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>DoD</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>DoD</b>Panel</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <?php $messages = App\Support::where('is_viewed', 0)->where('owner', 1)->count();
        $payments = App\Transactions::where('type', 1)->where('payment_status', 0)->count();
        $errors = App\Errors::count(); ?>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="<?php echo e(url('admin/support')); ?>">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success"><?php echo e($messages); ?></span>
            </a>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="<?php echo e(url('admin/payments')); ?>">
              <i class="fa fa-credit-card"></i>
              <span class="label label-warning"><?php echo e($payments); ?></span>
            </a>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="<?php echo e(url('admin/errors')); ?>">
              <i class="fa fa-ban"></i>
              <span class="label label-danger"><?php echo e($errors); ?></span>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php if(Sentinel::getUser()->photo == null): ?><?php echo e(asset('assets/admin/img/no-profile.png')); ?><?php else: ?> <?php echo e(asset(Sentinel::getUser()->photo)); ?><?php endif; ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo e(Sentinel::getUser()->first_name . ' ' . Sentinel::getUser()->last_name); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php if(Sentinel::getUser()->photo == null): ?><?php echo e(asset('assets/admin/img/no-profile.png')); ?><?php else: ?> <?php echo e(asset(Sentinel::getUser()->photo)); ?><?php endif; ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo e(Sentinel::getUser()->first_name . ' ' . Sentinel::getUser()->last_name); ?>

                  <small>Admin</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo e(url('admin/profilim')); ?>" class="btn btn-default btn-flat">Profilim</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo e(url('admin/cikis-yap')); ?>" class="btn btn-default btn-flat">Çıkış Yap</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>