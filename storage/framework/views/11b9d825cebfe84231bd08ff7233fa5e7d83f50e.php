                    
    				<div class="profile-sidebar">
    				<!-- SIDEBAR USERPIC -->
    				<div class="profile-userpic">
    					<img src="<?php if(Sentinel::getUser()->photo == null): ?><?php echo e(asset('assets/admin/img/no-profile.png')); ?><?php else: ?> <?php echo e(asset(Sentinel::getUser()->photo)); ?><?php endif; ?>" class="img-responsive" alt="">
    				</div>
    				<!-- END SIDEBAR USERPIC -->
    				<!-- SIDEBAR USER TITLE -->
    				<div class="profile-usertitle">
    					<div class="profile-usertitle-name">
    						<?php echo e(Sentinel::getUser()->first_name); ?>

    					</div>
    					<div class="profile-usertitle-job">
    					<?php echo e(Carbon\Carbon::parse(Sentinel::getUser()->created_at)->format('d/m/Y')); ?>

    					</div>
    				</div>
    				<!-- END SIDEBAR USER TITLE -->
    				<!-- SIDEBAR BUTTONS -->
    				<?php if(Sentinel::inRole('admin')): ?>
    				<div class="profile-userbuttons">
    					<button type="button" class="btn btn-success btn-sm">E-Posta Gönder</button>
    					<button type="button" class="btn btn-danger btn-sm">SMS Gönder</button>
    				</div>
    				<?php endif; ?>
    				<!-- END SIDEBAR BUTTONS -->
    				<!-- SIDEBAR MENU -->
    				<div class="profile-usermenu">
    					<ul class="nav">
    						<li class="<?php echo e(Request::segment(1) === 'hesabim' ? 'active' : null); ?>">
    							<a href="<?php echo e(url('hesabim')); ?>">
    							<i class="fa fa-home"></i>
    							Hesabım </a>
    						</li>
    						<li class="<?php echo e(Request::segment(1) === 'adreslerim' ? 'active' : null); ?>">
    							<a href="<?php echo e(url('adreslerim')); ?>">
    							<i class="fa fa-map-pin"></i>
    							Adreslerim </a>
    						</li>
    						<li class="<?php echo e(Request::segment(1) === 'siparislerim' ? 'active' : null); ?>">
    							<a href="<?php echo e(url('siparislerim')); ?>">
    							<i class="fa fa-try"></i>
    							Siparişlerim </a>
    						</li>
    						<li class="<?php echo e(Request::segment(1) === 'favorilerim' ? 'active' : null); ?>">
    							<a href="<?php echo e(url('favorilerim')); ?>">
    							<i class="fa fa-heart"></i>
    							Favorilerim </a>
    						</li>
    					</ul>
    				</div>
    				<!-- END MENU -->
    			</div>