  <div id="container-floating">


<a href="<?php echo e(url('admin/payments')); ?>">
	  <div class="nd5 nds" data-toggle="tooltip" data-placement="left" data-original-title="Payments"><p class="letter"><i class="fa fa-credit-card" style="font-size:18px"></i></p></div>
	</a>

  <a href="<?php echo e(url('admin/sales')); ?>">
	  <div class="nd4 nds" data-toggle="tooltip" data-placement="left" data-original-title="Sales"><p class="letter"><i class="fa fa-euro" style="font-size:18px"></i></p></div>
	</a>

  <a href="<?php echo e(url('admin/support')); ?>">
	  <div class="nd3 nds" data-toggle="tooltip" data-placement="left" data-original-title="Support"><p class="letter"><i class="fa fa-life-ring" style="font-size:18px"></i></p></div>
	</a>

  <a href="<?php echo e(url('admin/user-management/users')); ?>">
	  <div class="nd1 nds" data-toggle="tooltip" data-placement="left" data-original-title="Users"><img class="reminder">
	    <p class="letter"><i class="fa fa-users" style="font-size:18px"></i></p>
	  </div>
	</a>

  <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" onclick="newmail()">
    <p class="plus">+</p>
    <span class="edit" src="https://ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/1x/bt_compose2_1x.png">
  </div>

</div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b><?php echo e(config('app.version')); ?></b>
    </div>
    <strong>Copyright &copy; 2020 <a href="<?php echo e(url('/')); ?>">Do & Die</a></strong>
  </footer>