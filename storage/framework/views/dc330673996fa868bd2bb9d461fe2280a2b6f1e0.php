<?php $__env->startSection('styles'); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <div class="content" style="margin-top:20px">
                 <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>1 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>25.000 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>10 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>10.000 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>25 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>5.000 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>50 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>2.500 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>75 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>1.000 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>100 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>500 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>250 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>250 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>500 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>100 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>750 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>50 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>1000 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>25 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>2500 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>10 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>10000 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>5 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>25000 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>3 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>50000 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>2 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?> data-src="<?php echo e(asset('assets/mobile/images/euro.png')); ?>" <?php else: ?> data-src="<?php echo e(asset('assets/mobile/images/lira.png')); ?>" <?php endif; ?> alt="img" style="width:35px; height:35px">
                    <strong>100000 x</strong>
                    <span><?php echo app('translator')->getFromJson('general.europe-world'); ?></span>
                    <em>1 <?php if((!Sentinel::check() && session('lang') != "tr") || (Sentinel::check() && Sentinel::getUser()->currency == "eur")): ?>€<?php else: ?>₺<?php endif; ?> <!--<del> Was $500</del>--></em>
               </div>
            </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
     
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>