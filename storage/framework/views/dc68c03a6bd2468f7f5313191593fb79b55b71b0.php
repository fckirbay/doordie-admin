<!-- ================================== TOP NAVIGATION ================================== -->
            <div class="side-menu animate-dropdown outer-bottom-xs">
                <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Kategoriler</div>        
                <nav class="yamm megamenu-horizontal" role="navigation">
                    <ul class="nav">
                        <!--
                        <!-- /.menu-item -->
            
                        
            
            <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <li class="menu-item">
                    <a href="<?php echo e(url('kategori', $val->slug)); ?>" class="dropdown-toggle"><i class="icon fa fa-<?php echo e($val->icon); ?>"></i><?php echo e($val->name); ?></a>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <li class="menu-item">
                    <a href="<?php echo e(url('kategori', $category->slug)); ?>" class="dropdown-toggle"><i class="icon fa fa-<?php echo e($category->icon); ?>"></i><?php echo e($category->name); ?></a>
                </li>
            <?php endif; ?>
                    </ul><!-- /.nav -->
                </nav><!-- /.megamenu-horizontal -->
            </div><!-- /.side-menu -->
<!-- ================================== TOP NAVIGATION : END ================================== -->