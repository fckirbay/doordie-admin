<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <p class="text-center" style="text-align:center"><?php if($user->country != null): ?><img src="<?php echo e(asset('assets/cardgame/images/flags/'.strtolower($user->country).'.svg')); ?>" style="width:30px; height:20px"/><?php endif; ?></p>
              
              <h3 class="profile-username text-center"><?php echo e($user->username); ?></h3>

              <p class="text-muted text-center"><?php echo e($user->country); ?> <br /> <?php if($user->ip_address != null): ?>IP Adresi: <a href="https://tools.keycdn.com/geo?host=<?php echo e($user->ip_address); ?>"><?php echo e($user->ip_address); ?></a> <br /><?php if(isset($userLocations[0])): ?><?php echo e($userLocations[0]->city . ' / ' . $userLocations[0]->country_name); ?><?php endif; ?> <?php endif; ?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Bakiye</b> <a class="pull-right"><?php if($user->country_code != "90"): ?>€<?php endif; ?><?php echo e(number_format($user->balance/100, 2, ',', '.')); ?> <?php if($user->country_code == "90"): ?>₺<?php endif; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Toplam Kazanç</b> <a class="pull-right"><?php if($user->country_code != "90"): ?>€<?php endif; ?><?php echo e(number_format($user->earnings, 2, ',', '.')); ?> <?php if($user->country_code == "90"): ?>₺<?php endif; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Bilet</b> <a class="pull-right"><?php echo e($user->ticket); ?></a>
                </li>
                <!--<li class="list-group-item">
                  <b>Açtığı Kutu</b> <a class="pull-right"><?php echo e($user->clicks); ?></a>
                </li>-->
                <li class="list-group-item">
                  <b>Kazanan</b> <a class="pull-right"><?php echo e($won); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Kaybeden</b> <a class="pull-right"><?php echo e($lost); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Ref. Sayısı</b> <a class="pull-right"><?php echo e(count($references)); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Video İzleme</b> <a class="pull-right"><?php echo e(count($rewardeds)); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Offerwall</b> <a class="pull-right">$<?php echo e($offerwall_total); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Ref. ID</b> <a href="<?php echo e(url('admin/user-management/user', $user->reference_id)); ?>" class="pull-right"><?php echo e($user->reference_id); ?></a>
                </li>
                <li class="list-group-item">
                  <b>İşletim Sistemi</b> <a class="pull-right"><?php echo e($user->os); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Versiyon</b> <a class="pull-right"><?php echo e($user->app_version); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Üyelik Tarihi</b> <a class="pull-right"><?php echo e(Carbon\Carbon::parse($user->createdAt)->format('d M Y - H:i:s')); ?></a>
                </li>
                
              </ul>

              
              <?php if($user->blocked == 1): ?>
                <span style="text-align:center">ENGELLENME NEDENİ:<br />
                <?php if($user->bloked_reason == null): ?>
                  Belirsiz
                <?php else: ?>
                  <?php echo e($user->blocked_reason); ?>

                <?php endif; ?>
                </span>
              <?php endif; ?>
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li><a href="#activity" data-toggle="tab"><i class="fa fa-info"></i></a></li>
              <li><a href="#jokers" data-toggle="tab"><i class="fa fa-diamond"></i></a></li>
              <li <?php if(isset($_GET['chat'])): ?> class="active" <?php endif; ?>><a href="#message" data-toggle="tab"><i class="fa fa-envelope"></i></a></li>
              <li><a href="#references" data-toggle="tab"><i class="fa fa-user-plus"></i></a></li>
              <li><a href="#rewardeds" data-toggle="tab"><i class="fa fa-video-camera"></i></a></li>
              <li><a href="#offerwall" data-toggle="tab"><i class="fa fa-th"></i></a></li>
              <li><a href="#timeline" data-toggle="tab"><i class="fa fa-chevron-up"></i></a></li>
              <!--<li><a href="#locations" data-toggle="tab"><i class="fa fa-map-marker"></i></a></li>-->
              <li><a href="#notification" data-toggle="tab"><i class="fa fa-bell"></i></a></li>
              <li><a href="#settings" data-toggle="tab"><i class="fa fa-th-list"></i></a></li>
              <!--<li><a href="#scratches" data-toggle="tab"><i class="fa fa-pencil-square"></i></a></li>-->
              <li><a href="#bonuses" data-toggle="tab"><i class="fa fa-life-ring"></i></a></li>
              <!--<li><a href="#exchanges" data-toggle="tab"><i class="fa fa-exchange"></i></a></li>-->
              <li><a href="#sendticket" data-toggle="tab"><i class="fa fa-ticket"></i></a></li>
              <li><a href="#otheraccounts" data-toggle="tab"><i class="fa fa-users"></i></a></li>
              <!--<li><a href="#premiummembership" data-toggle="tab"><i class="fa fa-usd"></i></a></li>-->
              <li><a href="#payment" data-toggle="tab"><i class="fa fa-money"></i></a></li>
              <!--<li><a href="#recovers" data-toggle="tab"><i class="fa fa-asterisk"></i></a></li>-->
              <li><a href="#notes" data-toggle="tab"><i class="fa fa-edit"></i></a></li>
            </ul>
            <div class="tab-content">

              <div class="<?php if(isset($_GET['chat'])): ?> active <?php endif; ?> tab-pane" id="message">


            <!-- DIRECT CHAT -->
                <!-- /.box-header -->
                  <!-- Conversations are loaded here -->
                  <!-- /.box-body -->
                <div class="box-footer">
                  <?php echo Form::open(['url'=>'admin/user-management/send-message', 'method'=>'post', 'autocomplete' => 'off']); ?>


                  <select class="form-control" id="drafts">
                        <option>Bir hazır mesaj seçin...</option>
                        <?php $__empty_1 = true; $__currentLoopData = $readyAnswers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                          <option value="<?php echo e($val->answer); ?>"><?php echo e($val->question); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <?php endif; ?>
                      </select>
                      <br />
                    <div class="input-group">
                      <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>"/>
                      
                      <textarea type="text" name="message" placeholder="Mesaj yazın..." id="message-text" class="form-control" rows="3"></textarea>
                      <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning btn-flat" style="height:75px">Gönder</button>
                          </span>
                    </div>
                  <?php echo Form::close(); ?>

                </div>
                <!-- /.box-footer-->
                  <div class="direct-chat-messages">
                    <!-- Message. Default to the left -->

                    <?php $__empty_1 = true; $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                      <?php if($val->owner == 1): ?>
                        <div class="direct-chat-msg">
                          <div class="direct-chat-info clearfix">
                            <span class="direct-chat-timestamp pull-right"><?php echo e(Carbon\Carbon::parse($val->date)->format('d M - H:i')); ?></span>
                          </div>
                          <!-- /.direct-chat-info -->
                          <div class="direct-chat-text">
                            <?php echo e($val->message); ?> <span style="float:right"><i class="fa fa-check"></i><?php if($val->is_viewed == 1): ?><i class="fa fa-check" style="margin-left:-7px"></i><?php endif; ?></span>
                          </div>
                          <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->
                      <?php else: ?>
                        <!-- Message to the right -->
                        <div class="direct-chat-msg right">
                          <div class="direct-chat-info clearfix">
                            <span class="direct-chat-timestamp pull-left"><?php echo e(Carbon\Carbon::parse($val->date)->format('d M H:i')); ?></span>
                          </div>
                          <!-- /.direct-chat-info -->
                          <div class="direct-chat-text" style="background-color:#f39c12; background:#f39c12; color:#fff">
                            <?php echo e($val->message); ?> <span style="float:right"><i class="fa fa-check"></i><?php if($val->is_viewed == 1): ?><i class="fa fa-check" style="margin-left:-7px"></i><?php endif; ?>
                          </div>
                          <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->
                      <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>

                    <?php endif; ?>
                    

                  </div>
                  <!--/.direct-chat-messages-->
                
              <!--/.direct-chat -->







              </div>
              <!-- /.tab-pane -->


              <div class="tab-pane" id="notification">
                <!-- /.box-header -->
                  <!-- Conversations are loaded here -->
                  <!-- /.box-body -->
                <div class="box-footer">
                  <?php echo Form::open(['url'=>'admin/user-management/send-notification', 'method'=>'post', 'autocomplete' => 'off']); ?>

                    <div class="input-group">
                      <input type="text" name="firebase" value="<?php echo e($user->firebase); ?>" class="form-control" disabled>
                      <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>"/>
                      <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                      <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning btn-flat">Gönder</button>
                          </span>
                    </div>
                  <?php echo Form::close(); ?>

                </div>
              </div>
              <!-- /.tab-pane -->


              <div class="<?php if(!isset($_GET['chat'])): ?> active <?php endif; ?> tab-pane" id="activity">
                <?php echo Form::open(['url'=>'admin/user-management/user', 'method'=>'post', 'autocomplete' => 'off']); ?>

              <div class="box-body">
                <div class="row">
                    <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>"/>
                    <!--<div class="col-md-6">
                        <div class="form-group">
                          <label>İsim Soyisim</label>
                          <input type="text" class="form-control" id="inputName" placeholder="İsim Soyisim" name="first_name" value="<?php echo e($user->first_name); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>TC No.</label>
                          <input type="text" class="form-control" id="inputName" placeholder="TC No." name="tc_number" value="<?php echo e($user->tc_number); ?>" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Doğum Tarihi</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Doğum Tarihi" name="birthday" value="<?php echo e(Carbon\Carbon::parse($user->birthday)->format('d.m.Y')); ?>" disabled>
                        </div>
                    </div>-->
                    <!--<div class="col-md-6">
                        <div class="form-group">
                          <label>Yeni Şifre</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Yeni Şifre" name="new_password">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>E-Posta Adresi</label>
                          <input type="text" class="form-control" id="inputName" placeholder="E-Posta Adresi" name="email_2" value="<?php echo e($user->email_2); ?>">
                        </div>
                    </div>-->
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Telefon</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Telefon" name="phone" value="<?php echo e($user->phone); ?>">
                        </div>
                    </div>
                    <!--<div class="col-md-6">
                        <div class="form-group">
                          <label>Telefon 2</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Telefon 2" name="phone_2" value="<?php echo e($user->phone_2); ?>">
                        </div>
                    </div>-->
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Bilet</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Bilet" name="ticket" value="<?php echo e($user->ticket); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Bakiye</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Bakiye" name="balance" value="<?php echo e($user->balance); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>İşletim Sistemi</label>
                          <input type="text" class="form-control" id="inputName" placeholder="İşletim Sistemi" name="os" value="<?php echo e($user->os); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label><strong>WİNNER</strong></label>
                          <select class="form-control" name="is_winner" required>
                            <option value="0" <?php if($user->is_winner == 0): ?> selected <?php endif; ?>>Hayır</option>
                            <option value="1" <?php if($user->is_winner == 1): ?> selected <?php endif; ?>>Evet</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Durum</label>
                          <select class="form-control" name="blocked" required>
                            <option value="0" <?php if($user->blocked == 0): ?> selected <?php endif; ?>>Aktif</option>
                            <option value="1" <?php if($user->blocked == 1): ?> selected <?php endif; ?>>Engelli</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Destek Durumu</label>
                          <select class="form-control" name="support" required>
                            <option value="0" <?php if($user->support == 0): ?> selected <?php endif; ?>>Kapalı</option>
                            <option value="1" <?php if($user->support == 1): ?> selected <?php endif; ?>>Açık</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Engellenme Nedeni</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Engellenme Nedeni" name="blocked_reason" value="<?php echo e($user->blocked_reason); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Doğrulama Denemesi</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Doğrulama Denemesi" name="verification_tries" value="<?php echo e($user->verification_tries); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Onaylı Üye</label>
                          <select class="form-control" name="verification" required>
                            <option value="0" <?php if($user->verification == 0): ?> selected <?php endif; ?>>Onaysız</option>
                            <option value="1" <?php if($user->verification == 1): ?> selected <?php endif; ?>>Onaylı</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Dil</label>
                          <select class="form-control" name="lang" required>
                            <option value="en" <?php if($user->verification == 0 || $user->lang == "en"): ?> selected <?php endif; ?>>İngilizce</option>
                            <option value="tr" <?php if($user->lang == "tr"): ?> selected <?php endif; ?>>Türkçe</option>
                            <option value="it" <?php if($user->lang == "it"): ?> selected <?php endif; ?>>İtalyanca</option>
                            <option value="fr" <?php if($user->lang == "fr"): ?> selected <?php endif; ?>>Fransızca</option>
                            <option value="de" <?php if($user->lang == "de"): ?> selected <?php endif; ?>>Almanca</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Para Birimi</label>
                          <select class="form-control" name="currency" required>
                            <option value="eur" <?php if($user->verification == 0 || $user->country_code != "90"): ?> selected <?php endif; ?>>Euro</option>
                            <option value="try" <?php if($user->country_code == "90"): ?> selected <?php endif; ?>>Lira</option>
                          </select>
                        </div>
                    </div>
                    <!--<div class="col-md-6">
                        <div class="form-group">
                          <label>Satın Alma</label>
                          <select class="form-control" name="purchasing" required>
                            <option value="0" <?php if($user->purchasing == 0): ?> selected <?php endif; ?>>Kapalı</option>
                            <option value="1" <?php if($user->purchasing == 1): ?> selected <?php endif; ?>>Açık</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Video İzleme Sıfırla</label>
                          <select class="form-control" name="clear_rewardeds" required>
                            <option value="0">Hayır</option>
                            <option value="1">Evet</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Versiyon</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Version" name="app_version" value="<?php echo e($user->app_version); ?>">
                        </div>
                    </div>-->
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>OneSignal</label>
                          <input type="text" class="form-control" id="inputName" placeholder="OneSignal" name="firebase" value="<?php echo e($user->firebase); ?>">
                        </div>
                    </div>
                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Kaydet</button>
              </div>
            <?php echo Form::close(); ?>

              </div>

              <div class="tab-pane" id="jokers">
              <?php echo Form::open(['url'=>'admin/user-management/user-jokers', 'method'=>'post', 'autocomplete' => 'off']); ?>

              <div class="box-body">
                <div class="row">
                    <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>"/>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Çifte Kazanç</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Çifte Kazanç" name="double_earnings" value="<?php echo e($jokers->double_earnings); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Pas</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Pas" name="pass" value="<?php echo e($jokers->pass); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Kartı Göster</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Kartı Göster" name="show_result" value="<?php echo e($jokers->show_result); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Devam Et</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Devam Et" name="continue_left" value="<?php echo e($jokers->continue_left); ?>">
                        </div>
                    </div>
                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Kaydet</button>
              </div>
            <?php echo Form::close(); ?>

              </div>

              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                <table class="table table-striped">
                  <tr>
                      <th>ID</th>
                      <th>Tur</th>
                      <th>Coin</th>
                      <th>Durum</th>
                      <th>Tarih</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $games; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><?php echo e($val->id); ?></td>
                      <td><?php echo e($val->lap); ?></td>
                      <td><?php echo e($val->coins); ?></td>
                      <td><?php if($val->is_completed == 1): ?> <?php if($val->coins > 0): ?> <i class="fa fa-check" style="color: green"></i> <?php else: ?> <i class="fa fa-times" style="color: red"></i> <?php endif; ?> <?php else: ?> <i class="fa fa-spinner fa-pulse" style="color: gray"></i> <?php endif; ?></td>
                      <td><?php echo e(Carbon\Carbon::parse($val->createdAt)->format('d M Y - H:i:s')); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç kutu açmadı!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <table class="table table-striped">
                  <tr>
                      <th>İşlem Türü</th>
                      <th>Tutar</th>
                      <th>Durum</th>
                      <th>İşlem Tarihi</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $transactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><?php if($val->type == 1): ?> Para Çekme <?php else: ?> Para Yatırma <?php endif; ?></td>
                      <td><?php if($user->currency == "eur"): ?>€<?php endif; ?><?php echo e(number_format($val->amount, 2, ',', '.')); ?> <?php if($user->currency == "try"): ?>₺<?php endif; ?></td>
                      <td><?php if($val->status == 0): ?><em class="color-yellow-dark">Bekliyor <i class="fa fa-circle"></i> </em><?php elseif($val->status == 1): ?><em class="color-green-dark">Onaylandı <i class="fa fa-circle"></i> </em><?php elseif($val->status == 2): ?><em class="color-orange-dark">Reddedildi <i class="fa fa-circle"></i> </em><?php else: ?><em class="color-blue-dark">Hatalı <i class="fa fa-circle"></i> </em><?php endif; ?></td>
                      <td>İşlem Tarihi: <?php echo e(Carbon\Carbon::parse($val->created_date)->format('H:i d M')); ?> <?php if($val->confirm_date != null): ?> <br />Onay Tarihi: <?php echo e(Carbon\Carbon::parse($val->confirm_date)->format('H:i d M')); ?><?php endif; ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç işlem yok!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>

              <div class="tab-pane" id="references">
                <table class="table table-striped">
                  <tr>
                      <th>Referans ID</th>
                      <th>İsim Soyisim</th>
                      <th>Telefon Numarası</th>
                      <th>Durum</th>
                      <th>Engel</th>
                      <th>İşlem Tarihi</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $references; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><a href="<?php echo e(url('admin/user-management/user', $val->id)); ?>"><?php echo e($val->id); ?></a></td>
                      <td><?php echo e($val->username); ?></td>
                      <td><?php echo e($val->phone); ?></td>
                      <td><?php if($val->verification == 0): ?><i class="fa fa-times" style="color:red"></i><?php elseif($val->verification == 1): ?><i class="fa fa-check" style="color:green"></i><?php endif; ?></td>
                      <td><?php if($val->blocked == 1): ?><i class="fa fa-times" style="color:red"></i><?php else: ?><i class="fa fa-check" style="color:green"></i><?php endif; ?></td>
                      <td><?php echo e(Carbon\Carbon::parse($val->createdAt)->format('d M Y - H:i:s')); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="6" style="text-align:center">Hiç referans yok!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>

              <div class="tab-pane" id="rewardeds">
                <table class="table table-striped">
                  <tr>
                      <th>Tıklama Tarihi</th>
                      <th>Tamamlama Tarihi</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $rewardeds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><?php echo e(Carbon\Carbon::parse($val->click_date)->format('d M Y - H:i:s')); ?></td>
                      <td><?php echo e(Carbon\Carbon::parse($val->complete_date)->format('d M Y - H:i:s')); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç izleme yok!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>

              <div class="tab-pane" id="bonuses">
                <table class="table table-striped">
                  <tr>
                      <th>Bonus Miktarı</th>
                      <th>Nedeni</th>
                      <th>Tarih</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $bonuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><?php echo e($val->bonus_amount); ?></td>
                      <td><?php echo e($val->reason); ?></td>
                      <td><?php echo e(Carbon\Carbon::parse($val->date)->format('d/m/Y H:i:s')); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç izleme yok!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>

              
              <div class="tab-pane" id="offerwall">
                <table class="table table-striped">
                  <tr>
                      <th>Transaction ID</th>
                      <th>Kazanılan Bilet</th>
                      <th>Kâr ($)</th>
                      <th>Görev Başlığı</th>
                      <th>Tarih</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $offerwall; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><?php echo e($val->tx_id); ?></td>
                      <td><?php echo e($val->point_value); ?></td>
                      <td><?php echo e($val->usd_value); ?></td>
                      <td><?php echo e($val->offer_title); ?></td>
                      <td><?php echo e(Carbon\Carbon::parse($val->complete_date)->format('d M Y - H:i:s')); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç görev yok!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>

              <div class="tab-pane" id="otheraccounts">
                <table class="table table-striped">
                  <tr>
                      <th>User ID</th>
                      <th>Kullanıcı Adı</th>
                      <th>Üyelik Tarihi</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $otherAccounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><a href="<?php echo e(url('admin/user-management/user', $val->id)); ?>"><?php echo e($val->id); ?></a></td>
                      <td><?php echo e($val->first_name); ?></td>
                      <td><?php echo e(Carbon\Carbon::parse($val->createdAt)->format('d/m/Y H:i:s')); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="4" style="text-align:center">Başka bir üyeliği yok!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>

              <div class="tab-pane" id="locations">
                <table class="table table-striped">
                  <tr>
                      <th>IP Address</th>
                      <th>Ülke</th>
                      <th>Bölge Adı</th>
                      <th>Şehir</th>
                      <th>Tarih</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $userLocations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><?php echo e($val->ip); ?></td>
                      <td><?php echo e($val->country_name); ?></td>
                      <td><?php echo e($val->region_name); ?></td>
                      <td><?php echo e($val->city); ?></td>
                      <td><?php echo e(Carbon\Carbon::parse($val->checkDate)->format('d/m/Y H:i:s')); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="4" style="text-align:center">Konum bilgisi yok!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>

              <div class="tab-pane" id="recovers">
                <table class="table table-striped">
                  <tr>
                      <th>Tarih</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $recovers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><?php echo e(Carbon\Carbon::parse($val->created_at)->format('d/m/Y H:i:s')); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="1" style="text-align:center">Şifre sıfırlama bilgisi yok!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>

              <div class="tab-pane" id="sendticket">
                <?php echo Form::open(['url'=>'admin/user-management/send-ticket', 'method'=>'post', 'autocomplete' => 'off']); ?>

                  <div class="box-body">
                    <div class="row">
                        <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>"/>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Bilet Miktarı</label>
                              <textarea type="text" class="form-control" id="inputName" placeholder="Bilet Miktarı" name="tickets"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Veriliş Nedeni</label>
                              <textarea type="text" class="form-control" id="inputName" placeholder="Veriliş Nedeni" name="reason"></textarea>
                            </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Gönder</button>
                  </div>
                <?php echo Form::close(); ?>

              </div>

              <div class="tab-pane" id="premiummembership">
                <?php echo Form::open(['url'=>'admin/user-management/premium-membership', 'method'=>'post', 'autocomplete' => 'off']); ?>

                  <div class="box-body">
                    <div class="row">
                        <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>"/>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Üyelik Türünü Seçin</label>
                              <select class="form-control" name="membership" required>
                                <option value="bronze_tr">Bronze (TR)</option>
                                <option value="bronze_tr">Bronze (EN)</option>
                                <option value="silver_tr">Silver (TR)</option>
                                <option value="silver_en">Silver (EN)</option>
                                <option value="gold_tr">Gold (TR)</option>
                                <option value="gold_en">Gold (EN)</option>
                                <option value="platinum_tr">Platinum (TR)</option>
                                <option value="platinum_en">Platinum (EN)</option>
                                <option value="vip_tr">VIP(TR)</option>
                                <option value="vip_en">VIP(EN)</option>
                              </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Transaction ID</label>
                              <input type="text" class="form-control" id="inputName" placeholder="GPA-..." name="trans_id">
                            </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Gönder</button>
                  </div>
                <?php echo Form::close(); ?>

              </div>

              <div class="tab-pane" id="payment">
                <?php echo Form::open(['url'=>'admin/user-management/payment', 'method'=>'post', 'autocomplete' => 'off']); ?>

                  <div class="box-body">
                    <div class="row">
                        <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>"/>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Ödeme Tutarı</label>
                              <input type="number" class="form-control" id="inputName" placeholder="Ödeme Tutarı" name="amount" value="50">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Ödeme Türü</label>
                              <input type="text" class="form-control" id="inputName" placeholder="Papara, Turkcell, Vodafone, Migros..." name="payment_type" maxlength="50">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Ödeme Notu</label>
                              <input type="text" class="form-control" id="inputName" placeholder="Ödeme bilgilerini girin..." name="payment_notes">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Üyelik Durumu</label>
                              <select class="form-control" name="payment_status" required>
                                <option value="0">Ödeme Bekliyor</option>
                                <option value="1">Ödeme Yapıldı</option>
                              </select>
                            </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Gönder</button>
                  </div>
                <?php echo Form::close(); ?>

              </div>


              <div class="tab-pane" id="notes">

                <?php echo Form::open(['url'=>'admin/user-management/new-note', 'method'=>'post', 'autocomplete' => 'off']); ?>

                  <div class="box-body">
                    <div class="row">
                        <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>"/>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Not</label>
                              <input type="text" class="form-control" id="inputName" placeholder="Bir not girin..." name="note">
                            </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Gönder</button>
                  </div>
                <?php echo Form::close(); ?>



                <table class="table table-striped">
                  <tr>
                      <th>Yönetici</th>
                      <th>Not</th>
                      <th>Tarih</th>
                  </tr>
                  <?php $__empty_1 = true; $__currentLoopData = $notes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                      <td><?php echo e($val->admin_id); ?></td>
                      <td><?php echo e($val->note); ?></td>
                      <td><?php echo e(Carbon\Carbon::parse($val->created_at)->format('d/m/Y H:i:s')); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr>
                      <td colspan="3" style="text-align:center">Hiç not yok!</td>
                  </tr>
                  <?php endif; ?>
                </table>
              </div>

              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <script>
    $("#drafts").change(function() {
      $("#message-text").val($(this).val());
    });
    $("#message-text").keypress(function (e) {
    if(e.which == 13 && !e.shiftKey) {        
        $(this).closest("form").submit();
        e.preventDefault();
        return false;
    }
});
  </script>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>