<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Üyelik Satışları</h3>
                
              <?php echo Form::open(['url'=>'admin/sales', 'method' => 'get', 'style' => 'float:right']); ?>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="search" class="form-control pull-right" placeholder="Ara">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
              <?php echo Form::close(); ?>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:10%">Kullanıcı ID</th>
                  <th class="orta" style="width:20%">Üyelik Paketi</th>
                  <th class="orta" style="width:10%">İşlem ID</th>
                  <th class="orta" style="width:5%">Tarih</th>
                  <th class="orta" style="width:5%">#</th>
                </tr>
                <?php $__empty_1 = true; $__currentLoopData = $sales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr>
                  <td><a href="<?php echo e(url('admin/user-management/user', $val->user_id)); ?>"><?php echo e($val->username); ?></a></td>
                  <td class="orta"><?php echo e($val->membership_id); ?></td>
                  <td class="orta"><?php echo e($val->trans_id); ?></td>
                  <td class="orta"><?php echo e(Carbon\Carbon::parse($val->expiry_date)->format('d/m/Y H:i:s')); ?></td>
                  <td class="orta"><a href="<?php echo e(url('admin/cancel-sale', $val->id)); ?>" class="btn btn-danger btn-xs">İptal Et</a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                <?php endif; ?>
              </table>
            </div>
            <?php echo e($sales->links()); ?>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>