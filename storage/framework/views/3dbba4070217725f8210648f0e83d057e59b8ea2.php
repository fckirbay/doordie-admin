<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <?php $__empty_1 = true; $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="store-slide-2">
               <a href="<?php echo e(url($val->slug)); ?>" class="store-slide-image">
               <img class="preload-image" src="<?php echo e(asset('assets/mobile/images/empty.png')); ?>" data-src="<?php echo e(asset($val->photo)); ?>" alt="<?php echo e($val->title); ?>" style="max-height:70px">
               </a>
               <div class="store-slide-title">
                  <strong><?php echo e($val->title); ?></strong>
                  <em class="color-gray-dark"><?php echo e($val->subtitle); ?></em>
               </div>
               <div class="store-slide-button">
                  <strong><?php if($val->old_price > 0): ?>
                            <del class="color-red-light font-10"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</del>
                          <?php endif; ?> <?php echo e(number_format($val->price, 2, ',', '.')); ?>₺</strong>
                  <a href="<?php echo e(url($val->slug)); ?>"><i class="fa fa-search color-black"></i></a>
                  <a href="#"><i class="fa fa-heart color-highlight"></i></a>
               </div>
            </div>
            <div class="decoration bottom-0"></div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <div style="text-align:center; margin-top:20px">Ürün bulunamadı!</div>
            <?php endif; ?>
            <div class="pagination" style="margin-top:20px">
                <?php echo e($products->links('pagination.mobile')); ?>

                <!--<a href="#"><i class="fa fa-angle-left"></i></a>
                <a href="#" class="no-border">1</a>
                <a href="#" class="no-border">2</a>
                <a href="#" class="bg-blue-dark color-white">3</a>
                <a href="#" class="no-border">4</a>
                <a href="#" class="no-border">5</a>
                <a href="#"><i class="fa fa-angle-right"></i></a>-->
            </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>