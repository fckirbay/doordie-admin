<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Main content -->
    <section class="content">
      <!-- Main content -->
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Yeni Ürün</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if(count($errors) > 0): ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <?php echo Form::open(['url'=>'panel/product-management/new-product', 'autocomplete' => 'off']); ?>

              <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Kategori</label>
                          <select class="form-control" name="category_id" required>
                            <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <option value="<?php echo e($val->id); ?>"><?php echo e($val->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <?php endif; ?>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Marka</label>
                          <select class="form-control" name="brand_id" required>
                            <?php $__empty_1 = true; $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <option value="<?php echo e($val->id); ?>"><?php echo e($val->brand); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <?php endif; ?>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Marka</label>
                          <select class="form-control" name="seller_id" required>
                            <?php $__empty_1 = true; $__currentLoopData = $sellers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <option value="<?php echo e($val->id); ?>"><?php echo e($val->first_name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <?php endif; ?>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Başlık</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Başlık" name="title">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Slug</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Slug" name="slug">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Alt Başlık</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Alt Başlık" name="subtitle">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Ürün Kodu</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Ürün Kodu" name="code">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Fiyat</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Fiyat" name="price">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>İndirimsiz Fiyat</label>
                          <input type="text" class="form-control" id="inputName" placeholder="İndirimsiz Fiyat" name="old_price">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>Açıklama</label>
                          <textarea name="description" class="form-control my-editor" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Stok</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Stok" name="stock">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Kargo</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Kargo" name="shipping">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Durum</label>
                          <select class="form-control" name="is_active" required>
                            <option value="0">Pasif</option>
                            <option value="1" selected>Aktif</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Seo Başlığı</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Seo Başlığı" name="seo_title">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Seo Açıklaması</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Seo Açıklaması" name="seo_description">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Seo Anahtar Kelimeleri</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Seo Anahtar Kelimeleri" name="seo_keywords">
                        </div>
                    </div>
                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Kaydet</button>
              </div>
            <?php echo Form::close(); ?>

          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern bootstrap"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | bootstrap",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
<!--
<script>
  var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
</script>
<script type="text/javascript">
  $(function () {
     CKEDITOR.replace( 'content', options);
   });
</script>
-->
  <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>