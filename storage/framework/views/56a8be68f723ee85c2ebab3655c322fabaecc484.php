				<script type="text/javascript" src="<?php echo e(asset('assets/cardgame/scripts/jquery.js')); ?>"></script>
				<script type="text/javascript" src="<?php echo e(asset('assets/cardgame/scripts/plugins.js')); ?>"></script>
				<script type="text/javascript" src="<?php echo e(asset('assets/cardgame/scripts/custom.js')); ?>"></script>
				<!-- Sweetalert -->
				<script src="<?php echo e(asset('assets/admin/plugins/sweetalert/dist/sweetalert.min.js')); ?>"></script>
				<?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>