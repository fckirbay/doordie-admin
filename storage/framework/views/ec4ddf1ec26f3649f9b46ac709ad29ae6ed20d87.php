        <!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
	    <meta name="keywords" content="Hediyelik, oyuncak, peluş">
	    <meta name="robots" content="all">

	    <title>byDukkan - Türkiye'nin en farklı dükkanı!</title>

	    <!-- Bootstrap Core CSS -->
	    <link rel="stylesheet" href="<?php echo e(asset('assets/theme/css/bootstrap.min.css')); ?>">
	    
	    <!-- Customizable CSS -->
	    <link rel="stylesheet" href="<?php echo e(asset('assets/theme/css/main.css')); ?>">
	    <link rel="stylesheet" href="<?php echo e(asset('assets/theme/css/blue.css')); ?>">
	    <link rel="stylesheet" href="<?php echo e(asset('assets/theme/css/owl.carousel.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/theme/css/owl.transitions.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/theme/css/animate.min.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/theme/css/rateit.css')); ?>">
		<link rel="stylesheet" href="<?php echo e(asset('assets/theme/css/bootstrap-select.min.css')); ?>">
		<link href="<?php echo e(asset('assets/theme/css/lightbox.css')); ?>" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo e(asset('assets/theme/plugins/sweetalert/dist/sweetalert.css')); ?>">

		<!-- Icons/Glyphs -->
		<link rel="stylesheet" href="<?php echo e(asset('assets/theme/css/font-awesome.css')); ?>">

        <!-- Fonts --> 
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>