<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <div class="content" style="margin-top:10px">
                <?php $__empty_1 = true; $__currentLoopData = $transactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="system-box"><strong><?php if($val->type == 1): ?> <?php echo app('translator')->getFromJson('general.withdraw'); ?> (<?php if(Sentinel::getUser()->currency == "eur"): ?>€<?php endif; ?><?php echo e(number_format($val->amount, 2, ',', '.')); ?> <?php if(Sentinel::getUser()->currency == "try"): ?>₺<?php endif; ?>)<?php else: ?> <?php echo app('translator')->getFromJson('general.premium_membership'); ?> (<?php if(Sentinel::getUser()->currency == "eur"): ?>€<?php endif; ?><?php echo e(number_format($val->amount, 2, ',', '.')); ?> <?php if(Sentinel::getUser()->currency == "try"): ?>₺<?php endif; ?>)<?php endif; ?> </strong><?php if($val->status == 0): ?><em class="color-yellow-dark"><?php echo app('translator')->getFromJson('general.pending'); ?> <i class="fa fa-circle"></i> </em><?php elseif($val->status == 1): ?><em class="color-green-dark"><?php echo app('translator')->getFromJson('general.confirmed'); ?> <i class="fa fa-circle"></i> </em><?php elseif($val->status == 2): ?><em class="color-orange-dark"><?php echo app('translator')->getFromJson('general.rejected'); ?> <i class="fa fa-circle"></i> </em><?php else: ?><em class="color-blue-dark"><?php echo app('translator')->getFromJson('general.error'); ?> <i class="fa fa-circle"></i> </em><?php endif; ?><br /> <span style="font-size:10px"><?php echo app('translator')->getFromJson('general.transaction_date'); ?>: <?php echo e(Carbon\Carbon::parse($val->created_at)->format('H:i d M')); ?> <?php if($val->confirm_date != null): ?><?php echo app('translator')->getFromJson('general.confirmation_date'); ?>: <?php echo e(Carbon\Carbon::parse($val->confirm_date)->format('H:i d M')); ?><?php endif; ?></span></div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div style="text-align:center; margin-top:25px; font-size:16px; font-weight:bold"><?php echo app('translator')->getFromJson('general.you_dont_have_any_transaction'); ?></div>
                <?php endif; ?>
                <div class="clear"></div>
            </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>