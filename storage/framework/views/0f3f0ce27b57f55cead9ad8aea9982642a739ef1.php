<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class='row'>
   <div class='col-md-3 sidebar'>
      <?php echo $__env->make('partials.theme.category-navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="sidebar-module-container">
         <?php echo $__env->make('partials.theme.filter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>	
      </div>
      <!-- /.sidebar-module-container -->
   </div>
   <!-- /.sidebar -->
   <div class='col-md-9'>
      <!-- ========================================== SECTION – HERO ========================================= -->
      <div id="category" class="category-carousel hidden-xs">
         <div class="item">
            <div class="image">
               <img src="<?php echo e(asset('assets/theme/images/banners/cat-banner-1.jpg')); ?>" alt="" class="img-responsive">
            </div>
            <div class="container-fluid">
               <div class="caption vertical-top text-left">
                  <div class="big-text">
                     Big Sale
                  </div>
                  <div class="excerpt hidden-sm hidden-md">
                     Save up to 49% off
                  </div>
                  <div class="excerpt-normal hidden-sm hidden-md">
                     Lorem ipsum dolor sit amet, consectetur adipiscing elit
                  </div>
               </div>
               <!-- /.caption -->
            </div>
            <!-- /.container-fluid -->
         </div>
      </div>
      <!-- ========================================= SECTION – HERO : END ========================================= -->
      <div class="clearfix filters-container m-t-10">
         <div class="row">
            <div class="col col-sm-6 col-md-2">
               <div class="filter-tabs">
                  <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                     <li class="active">
                        <a data-toggle="tab" href="#grid-container"><i class="icon fa fa-th-large"></i>Grid</a>
                     </li>
                     <li><a data-toggle="tab" href="#list-container"><i class="icon fa fa-th-list"></i>List</a></li>
                  </ul>
               </div>
               <!-- /.filter-tabs -->
            </div>
            <!-- /.col -->
            <div class="col col-sm-12 col-md-6">
               <div class="col col-sm-3 col-md-6 no-padding">
                  <div class="lbl-cnt">
                     <span class="lbl">Sort by</span>
                     <div class="fld inline">
                        <div class="dropdown dropdown-small dropdown-med dropdown-white inline">
                           <button data-toggle="dropdown" type="button" class="btn dropdown-toggle">
                           Sırala <span class="caret"></span>
                           </button>
                           <ul role="menu" class="dropdown-menu">
                              <li role="presentation"><a href="<?php echo e(url(url()->full().'?order=en-yeniler')); ?>">En Yeniler</a></li>
                              <li role="presentation"><a href="<?php echo e(url(url()->full().'?order=dusuk-fiyat')); ?>">Fiyat: Düşükten Yükseğe</a></li>
                              <li role="presentation"><a href="<?php echo e(url(url()->full().'?order=yuksek-fiyat')); ?>">Fiyat: Yüksekten Düğüşe</a></li>
                              <li role="presentation"><a href="<?php echo e(url(url()->full().'?order=a-z')); ?>">Ürün Adı: A-Z</a></li>
                           </ul>
                        </div>
                     </div>
                     <!-- /.fld -->
                  </div>
                  <!-- /.lbl-cnt -->
               </div>
               <!-- /.col -->
            </div>
            <!-- /.col -->
            <div class="col col-sm-6 col-md-4 text-right">
               <div class="pagination-container">
                  <?php echo e($products->links('pagination.default')); ?>

               </div>
               <!-- /.pagination-container -->		
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <div class="search-result-container ">
         <div id="myTabContent" class="tab-content category-list">
            <div class="tab-pane active " id="grid-container">
               <div class="category-product">
                  <div class="row">
                     <?php $__empty_1 = true; $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                     <div class="col-sm-6 col-md-3 wow fadeInUp">
                        <div class="products">
                           <div class="product">
                              <div class="product-image">
                                 <div class="image">
                                    <a href="<?php echo e(url($val->slug)); ?>"><img  src="<?php echo e(asset($val->photo)); ?>" class="manual" alt="" style="max-width:180px"></a>
                                 </div>
                                 <!-- /.image -->			
                                 <?php if($val->old_price > 0): ?>
                                 <div class="tag new"><span>%<?php echo e(intval(($val->old_price - $val->price) / $val->old_price * 100)); ?></span></div>
                                 <?php endif; ?>
                              </div>
                              <!-- /.product-image -->
                              <div class="product-info text-left center">
                                 <h3 class="name center" style="height:36px"><a href="<?php echo e(url($val->slug)); ?>"><?php echo e($val->title); ?></a></h3>
                                 <div class="rating rateit-small center"></div>
                                 <div class="description center"></div>
                                 <div class="product-price center">	
                                    <span class="price">
                                    <?php echo e(number_format($val->price, 2, ',', '.')); ?> ₺				</span>
                                    <?php if($val->old_price > 0): ?>
                                       <span class="price-before-discount"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
                                    <?php endif; ?>
                                 </div>
                                 <!-- /.product-price -->
                              </div>
                              <!-- /.product-info -->
                              <div class="cart clearfix animate-effect">
                                 <div class="action">
                                    <ul class="list-unstyled">
                                       <li class="lnk wishlist">
                       							<a data-toggle="tooltip" class="add-to-cart" href="<?php echo e(url($val->slug)); ?>">
                       								 <i class="icon fa fa-search"></i> Ürünü İncele
                       							</a>
                       						</li>
                                    </ul>
                                 </div>
                                 <!-- /.action -->
                              </div>
                              <!-- /.cart -->
                           </div>
                           <!-- /.product -->
                        </div>
                        <!-- /.products -->
                     </div>
                     <!-- /.item -->
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                     <?php endif; ?>
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.category-product -->
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane "  id="list-container">
               <div class="category-product">
                  <?php $__empty_1 = true; $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <div class="category-product-inner wow fadeInUp">
                     <div class="products">
                        <div class="product-list product">
                           <div class="row product-list-row">
                              <div class="col col-sm-4 col-lg-4">
                                 <div class="product-image">
                                    <div class="image" style="border:0px">
                                       <img src="<?php echo e(asset($val->photo)); ?>" alt="" style="width:auto; height:200px; display:block; margin:auto">
                                    </div>
                                 </div>
                                 <!-- /.product-image -->
                              </div>
                              <!-- /.col -->
                              <div class="col col-sm-8 col-lg-8">
                                 <div class="product-info">
                                    <h3 class="name"><a href="<?php echo e(url($val->slug)); ?>"><?php echo e($val->title); ?></a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="product-price">	
                                       <span class="price">
                                       <?php echo e(number_format($val->price, 2, ',', '.')); ?>₺				</span>
                                       <?php if($val->old_price > 0): ?>
                                          <span class="price-before-discount"><?php echo e(number_format($val->old_price, 2, ',', '.')); ?>₺</span>
                                       <?php endif; ?>
                                    </div>
                                    <!-- /.product-price -->
                                    <div class="description m-t-10"><?php echo e($val->subtitle); ?></div>
                                    <div class="cart clearfix animate-effect">
                                       <div class="action">
                                          <ul class="list-unstyled">
                                             <li class="lnk wishlist">
                                                <a data-toggle="tooltip" class="add-to-cart" href="<?php echo e(url($val->slug)); ?>">
                                                <i class="icon fa fa-search"></i> Ürünü İncele
                                                </a>
                                             </li>
                                          </ul>
                                       </div>
                                       <!-- /.action -->
                                    </div>
                                    <!-- /.cart -->
                                 </div>
                                 <!-- /.product-info -->	
                              </div>
                              <!-- /.col -->
                           </div>
                           <!-- /.product-list-row -->
                           <div class="tag new"><span>new</span></div>
                        </div>
                        <!-- /.product-list -->
                     </div>
                     <!-- /.products -->
                  </div>
                  <!-- /.category-product-inner -->
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <?php endif; ?>
               </div>
               <!-- /.category-product -->
            </div>
            <!-- /.tab-pane #list-container -->
         </div>
         <!-- /.tab-content -->
         <div class="clearfix filters-container">
            <div class="text-right">
               <div class="pagination-container">
                  <?php echo e($products->links('pagination.default')); ?>

               </div>
               <!-- /.pagination-container -->						    
            </div>
            <!-- /.text-right -->
         </div>
         <!-- /.filters-container -->
      </div>
      <!-- /.search-result-container -->
   </div>
   <!-- /.col -->
</div>
<!-- /.row -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>