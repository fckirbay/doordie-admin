<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">


    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Bize Ulaşın</h4>
                    </div><!-- /.module-heading -->

                    <div class="module-body">
        <ul class="toggle-footer" style="">
            <li class="media">
                <div class="pull-left">
                     <span class="icon fa-stack fa-lg">
                            <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <p>Adalet Mahallesi, Folkart Towers No:9, Kat:4</p>
                </div>
            </li>

              <li class="media">
                <div class="pull-left">
                     <span class="icon fa-stack fa-lg">
                      <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <p>+(555) 555 55 55<br>+(532) 532 32 32</p>
                </div>
            </li>

              <li class="media">
                <div class="pull-left">
                     <span class="icon fa-stack fa-lg">
                      <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <span><a href="#">info@bydukkan.com</a></span>
                </div>
            </li>
              
            </ul>
    </div><!-- /.module-body -->
                </div><!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Müşteri Hizmetleri</h4>
                    </div><!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="<?php echo e(url('hesabim')); ?>" title="Hesabım">Hesabım</a></li>
                            <li><a href="<?php echo e(url('siparis-gecmisi')); ?>" title="Sipariş Geçmişi">Sipariş Geçmişi</a></li>
                            <li><a href="<?php echo e(url('sss')); ?>" title="Sık Sorulan Sorular">SSS</a></li>
                            <li><a href="<?php echo e(url('ozel-teklifler')); ?>" title="Özel Teklifler">Özel Teklifler</a></li>
                            <li class="last"><a href="<?php echo e(url('yardim-merkezi')); ?>" title="Yardım Merkezi">Yardım Merkezi</a></li>
                        </ul>
                    </div><!-- /.module-body -->
                </div><!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Hakkımızda</h4>
                    </div><!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                          <li class="first"><a title="Hakkımızda" href="<?php echo e(url('hakkimizda')); ?>">Hakkımızda</a></li>
                          <li><a title="Müşteri Hizmetleri" href="<?php echo e(url('musteri-hizmetleri')); ?>">Müşteri Hizmetleri</a></li>
                          <li><a title="Vizyon & Misyon" href="<?php echo e(url('vizyon-ve-misyon')); ?>">Vizyon & Misyon</a></li>
                          <li><a title="Yatırımcı İlişkileri" href="<?php echo e(url('yatirimci-iliskileri')); ?>">Yatırımcı İlişkileri</a></li>
                          <li class="last"><a title="Gelişmiş Arama" href="<?php echo e(url('gelismis-arama')); ?>">Gelişmiş Arama</a></li>
                        </ul>
                    </div><!-- /.module-body -->
                </div><!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="module-heading">
                        <h4 class="module-title">Neden byDukkan?</h4>
                    </div><!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="<?php echo e(url('alisveris-rehberi')); ?>" title="Alışveriş Rehberi">Alışveriş Rehberi</a></li>
                            <li><a href="<?php echo e(url('guvenli-alisveris')); ?>" title="Güvenli Alışveriş">Güvenli Alışveriş</a></li>
                            <li><a href="<?php echo e(url('hizli-destek')); ?>" title="Hızlı Destek">Hızlı Destek</a></li>
                            <li><a href="<?php echo e(url('blog')); ?>" title="Blog">Blog</a></li>
                            <li class=" last"><a href="<?php echo e(url('bize-ulasin')); ?>" title="Bize Ulaşın">Bize Ulaşın</a></li>
                        </ul>
                    </div><!-- /.module-body -->
                </div>
            </div>
        </div>
    </div>

    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-padding social">
                <ul class="link">
                  <li class="fb pull-left"><a target="_blank" rel="nofollow" href="#" title="Facebook"></a></li>
                  <li class="tw pull-left"><a target="_blank" rel="nofollow" href="#" title="Twitter"></a></li>
                  <!--<li class="googleplus pull-left"><a target="_blank" rel="nofollow" href="#" title="GooglePlus"></a></li>
                  <li class="rss pull-left"><a target="_blank" rel="nofollow" href="#" title="RSS"></a></li>
                  <li class="pintrest pull-left"><a target="_blank" rel="nofollow" href="#" title="PInterest"></a></li>
                  <li class="linkedin pull-left"><a target="_blank" rel="nofollow" href="#" title="Linkedin"></a></li>-->
                  <li class="youtube pull-left"><a target="_blank" rel="nofollow" href="#" title="Youtube"></a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="clearfix payment-methods">
                    <ul>
                        <li><img src="<?php echo e(asset('assets/theme/images/payments/visa-master-iyzico.png')); ?>" alt="visa master iyzico" style="height:25px"></li>
                        <!--<li><img src="<?php echo e(asset('assets/theme/images/payments/iyzico.png')); ?>" alt=""></li>-->
                        <!--<li><img src="<?php echo e(asset('assets/theme/images/payments/troy.png')); ?>" alt=""></li>-->
                        <!--<li><img src="<?php echo e(asset('assets/theme/images/payments/visa.png')); ?>" alt=""></li>-->
                        <!--<li><img src="<?php echo e(asset('assets/theme/images/payments/mastercard.png')); ?>" alt=""></li>-->
                    </ul>
                </div><!-- /.payment-methods -->
            </div>
        </div>
    </div>
</footer>
<!-- ============================================================= FOOTER : END============================================================= -->