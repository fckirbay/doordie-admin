<!-- For demo purposes – can be removed on production -->
	
	
	<!-- For demo purposes – can be removed on production : End -->

	<!-- JavaScripts placed at the end of the document so the pages load faster -->
	<script src="<?php echo e(asset('assets/theme/js/jquery-1.11.1.min.js')); ?>"></script>
	
	<script src="<?php echo e(asset('assets/theme/js/bootstrap.min.js')); ?>"></script>
	
	<script src="<?php echo e(asset('assets/theme/js/bootstrap-hover-dropdown.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/theme/js/owl.carousel.min.js')); ?>"></script>
	
	<script src="<?php echo e(asset('assets/theme/js/echo.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/theme/js/jquery.easing-1.3.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/theme/js/bootstrap-slider.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/theme/js/jquery.rateit.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/theme/js/lightbox.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/theme/js/bootstrap-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/theme/js/wow.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/theme/js/scripts.js')); ?>"></script>
	<!-- Sweetalert -->
	<script src="<?php echo e(asset('assets/theme/plugins/sweetalert/dist/sweetalert.min.js')); ?>"></script>
	<script>
		$( ".add-cart-button" ).click(function() {
	      var id = $(this).data('id');
	      var qty = $('#qty').val();
	      if(qty < 1) {
	      	swal({
	          title: "Üzgünüz!",
	          text: "Bu üründen en az 1 adet alabilirsiniz!",
	          timer: 1000,
	          showConfirmButton: false,
	          type: "error"
	        });
	        return false;
	      } else if(isNaN(qty)) {
	      	swal({
	          title: "Üzgünüz!",
	          text: "Geçerli bir adet seçin!",
	          timer: 1000,
	          showConfirmButton: false,
	          type: "error"
	        });
	        return false;
	      }
	      $.ajax({
	        url: 'sepete-ekle',
	        method: 'POST',
	        data:{
	          id: id,
	          qty: qty,
	          _token: "<?php echo e(csrf_token()); ?>"
	        },
	        dataType: 'html',
	        success: function(data) {
	            var obj = jQuery.parseJSON( data);
	            $(".count").html(obj.count);
	            $(".value").html(obj.total);
	            $("#cart_content").html(obj.content);
	            swal({
	              title: "Tebrikler!",
	              text: "Ürün sepetinize eklendi!",
	              timer: 1000,
	              showConfirmButton: false,
	              type: "success"
	            });
	        }
	      });
	    });
	    
	    
	</script>
	<?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>