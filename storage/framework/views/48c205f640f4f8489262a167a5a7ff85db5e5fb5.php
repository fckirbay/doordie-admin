<?php $__env->startSection('styles'); ?>
    <style>
        /* Profile container */
        .profile {
          margin: 5px 0;
        }
        
        /* Profile sidebar */
        .profile-sidebar {
          padding: 20px 0 10px 0;
          background: #fff;
        }
        
        .profile-userpic img {
          float: none;
          margin: 0 auto;
          width: 50%;
          height: 50%;
          -webkit-border-radius: 50% !important;
          -moz-border-radius: 50% !important;
          border-radius: 50% !important;
        }
        
        .profile-usertitle {
          text-align: center;
          margin-top: 20px;
        }
        
        .profile-usertitle-name {
          color: #5a7391;
          font-size: 16px;
          font-weight: 600;
          margin-bottom: 7px;
        }
        
        .profile-usertitle-job {
          text-transform: uppercase;
          color: #5b9bd1;
          font-size: 12px;
          font-weight: 600;
          margin-bottom: 15px;
        }
        
        .profile-userbuttons {
          text-align: center;
          margin-top: 10px;
        }
        
        .profile-userbuttons .btn {
          text-transform: uppercase;
          font-size: 11px;
          font-weight: 600;
          padding: 6px 15px;
          margin-right: 5px;
        }
        
        .profile-userbuttons .btn:last-child {
          margin-right: 0px;
        }
            
        .profile-usermenu {
          margin-top: 30px;
        }
        
        .profile-usermenu ul li {
          border-bottom: 1px solid #f0f4f7;
        }
        
        .profile-usermenu ul li:last-child {
          border-bottom: none;
        }
        
        .profile-usermenu ul li a {
          color: #93a3b5;
          font-size: 14px;
          font-weight: 400;
        }
        
        .profile-usermenu ul li a i {
          margin-right: 8px;
          font-size: 14px;
        }
        
        .profile-usermenu ul li a:hover {
          background-color: #fafcfd;
          color: #5b9bd1;
        }
        
        .profile-usermenu ul li.active {
          border-bottom: none;
        }
        
        .profile-usermenu ul li.active a {
          color: #5b9bd1;
          background-color: #f6f9fb;
          border-left: 2px solid #5b9bd1;
          margin-left: -2px;
        }
        
        /* Profile Content */
        .profile-content {
          padding: 20px;
          background: #fff;
          min-height: 460px;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
        <div class="row profile">
    		<div class="col-md-3" style="padding-left:0px">
    		  <?php echo $__env->make('partials.theme.sidebar-menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    		</div>
    		<div class="col-md-9" style="padding-right:0px">
                <div class="profile-content">
                    <div class="row">
                      <div class="table-responsive">
                    		<table class="table">
                    			<thead>
                    				<tr>
                    					<th class="cart-romove item">Sipariş No.</th>
                    					<th class="cart-description item">Ödeme Yöntemi</th>
                    					<th class="cart-product-name item">Sipariş Tarihi</th>
                    					<th class="cart-edit item">Durum</th>
                    					<th class="cart-edit item">#</th>
                    				</tr>
                    			</thead><!-- /thead -->
                    			<tbody>
                    			  <?php $__empty_1 = true; $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    				<tr>
                    					<td class="romove-item">#<?php echo e($val->id); ?></td>
                    					<td class="cart-product-sub-total"><?php if($val->payment_method == 1): ?> Havale/EFT <?php elseif($val->payment_method == 2): ?> Debit Kart <?php elseif($val->payment_method == 3): ?> Kredi Kartı <?php endif; ?></td>
                    					<td class="cart-product-grand-total"><?php echo e(Carbon\Carbon::parse($val->created_at)->format('d/m/Y H:i:s')); ?></td>
                    					<th class="cart-edit item">
                    					  <?php if($val->order_status == 0): ?><span class="label label-info">Onay Bekliyor</span>
                    					  <?php elseif($val->order_status == 1): ?><span class="label label-info">Hazırlanıyor</span>
                    					  <?php elseif($val->order_status == 2): ?><span class="label label-info">Kargolandı</span>
                    					  <?php elseif($val->order_status == 3): ?><span class="label label-success">Tamamlandı</span>
                    					  <?php elseif($val->order_status == 4): ?><span class="label label-danger">İptal Edildi</span>
                    					  <?php elseif($val->order_status == 5): ?><span class="label label-warning">İade Edildi</span>
                    					  <?php elseif($val->order_status == 6): ?><span class="label label-danger">Reddedildi</span>
                    					  <?php endif; ?>
                    					</th>
                    					<td class="romove-item"><button class="btn btn-xs btn-info"><i class="fa fa-search"></i> Sipariş Detayı</button></td>
                    				</tr>
                    				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    				
                    				<?php endif; ?>
                    			</tbody><!-- /tbody -->
                    		</table><!-- /table -->
                    	</div>
                    </div>
                </div>
    		</div>
    	</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>