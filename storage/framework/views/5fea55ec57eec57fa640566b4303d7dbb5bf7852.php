    <script type="text/javascript" src="<?php echo e(asset('assets/mobile/scripts/jquery.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/mobile/scripts/plugins.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/mobile/scripts/custom.js')); ?>"></script>
    <!-- Sweetalert -->
	<script src="<?php echo e(asset('assets/theme/plugins/sweetalert/dist/sweetalert.min.js')); ?>"></script>
    	<!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5b367edbe04c852f48c99204/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
	<?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>