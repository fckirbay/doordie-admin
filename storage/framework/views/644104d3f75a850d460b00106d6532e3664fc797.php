<div id="menu-2" class="menu-sidebar menu-dark menu-sidebar-right menu-sidebar-reveal">
            <div class="menu-scroll">
               <div class="sidebar-icons">
                  <?php if(Sentinel::check()): ?>
                     <!-- data-menu="menu-share" -->
                     

                     <a href="#" style="width:80%; float:left; text-align:left"><span style="margin-left:20px"><?php echo app('translator')->getFromJson('general.reference'); ?> ID: <?php echo e(Sentinel::getUser()->id); ?></span></a>
                  <?php endif; ?>
                  <a href="#" class="close-menu" style="float:right"><i class="font-10 fa-fw fa fa-times" style="color:<?php echo e(session('color', '#fff')); ?>"></i></a>
               </div>
               <div class="sidebar-separator"><?php echo app('translator')->getFromJson('general.your_informations'); ?></div>
               <div class="menu-items menu-icons">
                  
                  <?php if(Sentinel::check()): ?>
                  <?php $unreads = App\Support::where('user_id', Sentinel::getUser()->id)->where('owner', 2)->where('is_viewed', 0)->count(); ?>
                  <a href="<?php echo e(url('my-messages')); ?>" class="<?php echo e(Request::segment(1) === 'my-messages' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-envelope" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.my_messages'); ?> <?php if($unreads > 0): ?> (<?php echo e($unreads); ?>) <?php endif; ?></em></a>
                  <?php endif; ?>
                  <a href="<?php echo e(url('my-account')); ?>" class="<?php echo e(Request::segment(1) === 'my-account' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-user" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.my_account'); ?></em></a>
                  <a href="<?php echo e(url('my-safe')); ?>" class="<?php echo e(Request::segment(1) === 'my-safe' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-university" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.my_safe'); ?></em></a>
                  <!--<a href="<?php echo e(url('/withdraw-money')); ?>" class="<?php echo e(Request::segment(1) === 'withdraw-money' ? 'menu-item-active' : null); ?>"><i class="fas fa-dollar-sign" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.withdraw_money'); ?></a>-->
                  <a href="<?php echo e(url('/exchange')); ?>" class="<?php echo e(Request::segment(1) === 'exchange' ? 'menu-item-active' : null); ?>"><i class="fas fa-exchange-alt" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.exchange'); ?></a>
                  <a href="<?php echo e(url('transactions')); ?>" class="<?php echo e(Request::segment(1) === 'transactions' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-search-plus" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.transactions'); ?></em></a>
                  <a href="<?php echo e(url('my-references')); ?>" class="<?php echo e(Request::segment(1) === 'my-references' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-retweet" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.my_references'); ?></em></a>
                  <!--<a href="<?php echo e(url('opened-boxes')); ?>" class="<?php echo e(Request::segment(1) === 'opened-boxes' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-box-open" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.opened_boxes'); ?></em></a>-->
                  <a href="<?php echo e(url('my-offerwall')); ?>" class="<?php echo e(Request::segment(1) === 'my-offerwall' ? 'menu-item-active' : null); ?>"><em><i class="fas fa-check" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.my_offers'); ?></em></a>
                  
                  <!--
                  <a href="<?php echo e(url('adreslerim')); ?>"><em><i class="fas fa-map-marker-alt"></i>Adreslerim</em></a>
                  <a href="<?php echo e(url('siparislerim')); ?>"><em><i class="fas fa-box"></i>Siparişlerim</em></a>
                  <a href="<?php echo e(url('favorilerim')); ?>"><em><i class="fas fa-heart"></i>Favorilerim</em></a>
                  -->
                  <a href="<?php echo e(url('logout')); ?>"><em><i class="fas fa-sign-out-alt" style="color:<?php echo e(session('color', '#fff')); ?>"></i><?php echo app('translator')->getFromJson('general.logout'); ?></em></a>
               </div>
               <!--
               <div class="sidebar-divider"></div>
               <div class="sidebar-separator">Bize Ulaşın</div>
               <div class="menu-items menu-icons">
                  <a href="tel:+1 234 567 890"><em><i class="fa fa-phone"></i>İletişim</em></a>
                  <a href="sms:+1 234 567 890"><em><i class="fa fa-comment"></i>Mesaj</em></a>
                  <a href="mailto:info@bydukkan.com"><em><i class="fa fa-envelope"></i>E-Posta</em></a>
               </div>
               -->
               <!--
               <div class="sidebar-divider"></div>
               <p class="sidebar-copyright" style="text-align:center">Copyright <span class="copyright-year"></span> byDukkan.</p>
               -->
            </div>
         </div>
