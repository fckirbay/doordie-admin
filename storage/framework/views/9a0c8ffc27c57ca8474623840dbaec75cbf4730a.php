<div class="sidebar-filter">
   <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
   <div class="sidebar-widget wow fadeInUp">
      <h3 class="section-title">Filtrele</h3>
      <!-- ============================================== PRICE SILDER============================================== -->
      <div class="widget-header">
         <h4 class="widget-title">Fiyat Aralığı</h4>
      </div>
      <div class="sidebar-widget-body m-t-10">
         <?php echo Form::open(['url'=>url()->current(), 'method'=>'get']); ?>

         <div class="price-range-holder">
            <span class="min-max">
            <span class="pull-left">0₺</span>
            <span class="pull-right">200₺</span>
            </span>
            <input type="text" id="amount" style="border:0; color:#666666; font-weight:bold;text-align:center;">
            <input type="text" class="price-slider" name="price" value="">
         </div>
         <!-- /.price-range-holder -->
         <button type="submit" class="lnk btn btn-primary">Filtrele</button>
         <?php echo Form::close(); ?>

      </div>
      <!-- /.sidebar-widget-body -->
   </div>
   <!-- /.sidebar-widget -->
   <!-- ============================================== SIDEBAR CATEGORY : END ============================================== -->
   <!-- ============================================== PRICE SILDER : END ============================================== -->
   <div class="home-banner">
      <img src="<?php echo e(asset('assets/theme/images/banners/LHS-banner.jpg')); ?>" alt="Image">
   </div>
</div>
<!-- /.sidebar-filter -->