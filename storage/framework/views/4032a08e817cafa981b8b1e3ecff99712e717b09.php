<?php $__env->startSection('styles'); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
            <div class="content">
               <ul class="link-list bottom-0">
                   <?php $__empty_1 = true; $__currentLoopData = $addresses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <li>
                     <a class="inner-link-list" href="#"><i class="fa fa-angle-down"></i><i class="font-13 fas fa-map-marker-alt color-orange-dark"></i><span><?php echo e($val->title); ?></span></a>
                     <ul class="link-list">
                        <p><?php echo e($val->address . ' ' . $val->district . ' ' . $val->town . ' / ' . $val->city . ' ' . $val->postal_code); ?></p>
                     </ul>
                  </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <p style="text-align:center">Henüz adres eklemediniz.</p>
                  <?php endif; ?>
               </ul>
            </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.mobile.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>