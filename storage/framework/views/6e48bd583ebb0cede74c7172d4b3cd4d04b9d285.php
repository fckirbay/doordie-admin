<?php $__env->startSection('styles'); ?>
<style>
   /* Profile container */
   .profile {
   margin: 5px 0;
   }
   /* Profile sidebar */
   .profile-sidebar {
   padding: 20px 0 10px 0;
   background: #fff;
   }
   .profile-userpic img {
   float: none;
   margin: 0 auto;
   width: 50%;
   height: 50%;
   -webkit-border-radius: 50% !important;
   -moz-border-radius: 50% !important;
   border-radius: 50% !important;
   }
   .profile-usertitle {
   text-align: center;
   margin-top: 20px;
   }
   .profile-usertitle-name {
   color: #5a7391;
   font-size: 16px;
   font-weight: 600;
   margin-bottom: 7px;
   }
   .profile-usertitle-job {
   text-transform: uppercase;
   color: #5b9bd1;
   font-size: 12px;
   font-weight: 600;
   margin-bottom: 15px;
   }
   .profile-userbuttons {
   text-align: center;
   margin-top: 10px;
   }
   .profile-userbuttons .btn {
   text-transform: uppercase;
   font-size: 11px;
   font-weight: 600;
   padding: 6px 15px;
   margin-right: 5px;
   }
   .profile-userbuttons .btn:last-child {
   margin-right: 0px;
   }
   .profile-usermenu {
   margin-top: 30px;
   }
   .profile-usermenu ul li {
   border-bottom: 1px solid #f0f4f7;
   }
   .profile-usermenu ul li:last-child {
   border-bottom: none;
   }
   .profile-usermenu ul li a {
   color: #93a3b5;
   font-size: 14px;
   font-weight: 400;
   }
   .profile-usermenu ul li a i {
   margin-right: 8px;
   font-size: 14px;
   }
   .profile-usermenu ul li a:hover {
   background-color: #fafcfd;
   color: #5b9bd1;
   }
   .profile-usermenu ul li.active {
   border-bottom: none;
   }
   .profile-usermenu ul li.active a {
   color: #5b9bd1;
   background-color: #f6f9fb;
   border-left: 2px solid #5b9bd1;
   margin-left: -2px;
   }
   /* Profile Content */
   .profile-content {
   padding: 20px;
   background: #fff;
   min-height: 460px;
   }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row profile">
   <div class="col-md-3" style="padding-left:0px">
      <?php echo $__env->make('partials.theme.sidebar-menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   </div>
   <div class="col-md-9" style="padding-right:0px">
      <div class="profile-content">
         <div class="row">
            <?php echo Form::open(['url'=>'hesabim', 'method'=>'post', 'class'=>'register-form outer-top-xs']); ?>

            <div class="col-md-6">
               <div class="form-group">
                  <label class="info-title" for="exampleInputEmail1">İsim Soyisim <span>*</span></label>
                  <input type="text" name="name_surname" class="form-control unicase-form-control text-input" id="exampleInputEmail1" value="<?php echo e(Sentinel::getUser()->first_name); ?>" required>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="info-title" for="exampleInputPassword1">E-Posta Adresi <span>*</span></label>
                  <input type="email" name="email" class="form-control unicase-form-control text-input" id="exampleInputPassword1" value="<?php echo e(Sentinel::getUser()->email); ?>" required>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="info-title" for="exampleInputEmail1">Cep Telefonu</label>
                  <input type="text" name="phone" class="form-control unicase-form-control text-input" id="exampleInputEmail1" value="<?php echo e(Sentinel::getUser()->phone); ?>">
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="info-title" for="exampleInputPassword1">Cep Telefonu 2</label>
                  <input type="text" name="phone_2" class="form-control unicase-form-control text-input" id="exampleInputPassword1" value="<?php echo e(Sentinel::getUser()->phone_2); ?>">
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="info-title" for="exampleInputEmail1">Eski Şifre <span>*</span></label>
                  <input type="password" name="old_password" class="form-control unicase-form-control text-input" id="exampleInputEmail1">
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="info-title" for="exampleInputEmail1">Yeni Şifre <span>*</span></label>
                  <input type="password" name="password" class="form-control unicase-form-control text-input" minlength="6" id="exampleInputEmail1">
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="info-title" for="exampleInputPassword1">Yeni Şifre Tekrar <span>*</span></label>
                  <input type="password" name="password_confirm" class="form-control unicase-form-control text-input" minlength="6" id="exampleInputPassword1">
               </div>
            </div>
            <div class="col-md-12">
               <button type="submit" class="btn-upper btn btn-primary checkout-page-button" style="float:right">Kaydet</button>
            </div>
            <?php echo Form::close(); ?>

         </div>
      </div>
   </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.theme.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>