<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReadyAnswers extends Model
{
    protected $table = 'ready_answers';
    public $timestamps = false;
}
