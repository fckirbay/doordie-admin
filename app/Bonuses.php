<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonuses extends Model
{
    protected $table = 'bonuses';
    public $timestamps = false;
}
