<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Alert;
use App\User;
use App\UserLocations;

class SentinelAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Sentinel::check())
        {
            return response()->view('cardgame.login');
        }
        if(Sentinel::getUser()->blocked == 1) {
            Alert::error(trans('general.blocked_user'));
            Sentinel::logout();
            return redirect('/login');
        }
        
        return $next($request);
    }
}
