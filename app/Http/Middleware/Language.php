<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(Sentinel::check())
        {
            if(Sentinel::getUser()->lang != null) {
                $lang = Sentinel::getUser()->lang;
                \App::setLocale($lang);
            } else {
                $lang = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
                if($lang == "tr" || $lang == "en" || $lang == "de" || $lang == "in") {
                    //\App::setLocale($lang);
                    \App::setLocale($lang);
                } else {
                    $lang = "en";
                    \App::setLocale("en");
                }
            }
        } else {

            $lang = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
            if($lang == "tr" || $lang == "en" || $lang == "de" || $lang == "in") {
                \App::setLocale($lang);
                /*$lang = "en";
                \App::setLocale("en");*/
            } else {
                $lang = "en";
                \App::setLocale("en");
            }
        }



       //Detect special conditions devices
        $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
        $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
        $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

        if(!$iPod && !$iPhone && !$iPad && !$Android) {
            //return \Redirect::to("http://luckybox.fun");
        } else {
            //do something with this information
            if($iPod || $iPhone || $iPad) {
                session(['browser' => 'ios']);
            } elseif($Android){
                session(['browser' => 'android']);
            } else {
                session(['browser' => 'unknown']);
            }
        }
        
        /*
        if(Sentinel::check() && (Sentinel::getUser()->id == 1 || Sentinel::getUser()->id == 8 || Sentinel::getUser()->id == 18888)) {
            session(['lang' => $lang]);
            return $next($request);
        }
        abort(500);
        */

        // abort(500);

        session(['lang' => $lang]);
        return $next($request);
        
    }
}
