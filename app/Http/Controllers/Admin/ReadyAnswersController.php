<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use App\ReadyAnswers;

class ReadyAnswersController extends Controller {
    
    public function getReadyAnswers() {

    	$answers = ReadyAnswers::get();      

        return view('admin/ready-answers', compact('answers'));
        
    }

    public function setReadyAnswers(Request $request) {
        
        $new = new ReadyAnswers();
        $new->question = $request->question;
        $new->answer = $request->answer;
        $new->save();
        
        if($new) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
    public function setDeleteReadyAnswer($id) {
        
        $delete = ReadyAnswers::where('id', $id)->delete();
        
        if($delete) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

}

