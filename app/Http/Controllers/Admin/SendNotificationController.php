<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use Carbon;
use App\User2s;
use App\Notifications;
use App\Support;

class SendNotificationController extends Controller {
    
    public function getSendNotification() {        

        return view('admin/send-notification');
        
    }
    
    public function setSendNotification(Request $request) {
        
        if($request->country == 1) {
        	$users = User2s::where('firebase', '!=', null)->where('verification', 1)->where('country', 'Turkey (+90)')->get();
        } else {
        	$users = User2s::where('firebase', '!=', null)->where('verification', 1)->where('country', '!=', 'Turkey (+90)')->get();
        }

        $count = 0;

        foreach($users as $key => $val) {
        	$send = new Notifications();
	        $send->user_id = $val->id;
	        $send->title = 'LuckyBox News';
	        $send->notification = $request->notification;
	        $send->firebase = $val->firebase;
            if($count < 750) {
                $send->time = Carbon\Carbon::now();
            } elseif($count < 1500) {
                $send->time = Carbon\Carbon::now()->addMinutes(10);
            } elseif($count < 2250) {
                $send->time = Carbon\Carbon::now()->addMinutes(20);
            } elseif($count < 3000) {
                $send->time = Carbon\Carbon::now()->addMinutes(30);
            } elseif($count < 3750) {
                $send->time = Carbon\Carbon::now()->addMinutes(40);
            } elseif($count < 4500) {
                $send->time = Carbon\Carbon::now()->addMinutes(50);
            } elseif($count < 5250) {
                $send->time = Carbon\Carbon::now()->addMinutes(60);
            } elseif($count < 6000) {
                $send->time = Carbon\Carbon::now()->addMinutes(70);
            } elseif($count < 6750) {
                $send->time = Carbon\Carbon::now()->addMinutes(80);
            } elseif($count < 7500) {
                $send->time = Carbon\Carbon::now()->addMinutes(90);
            } elseif($count < 8250) {
                $send->time = Carbon\Carbon::now()->addMinutes(100);
            } elseif($count < 9000) {
                $send->time = Carbon\Carbon::now()->addMinutes(110);
            } else {
                $send->time = Carbon\Carbon::now()->addMinutes(120);
            }
	        $send->save();

            $count++;
        }
        
        if($send) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

    public function setSendMessage(Request $request) {
        
        if($request->country == 1) {
            $users = User2s::where('firebase', '!=', null)->where('verification', 1)->where('country', 'Turkey (+90)')->get();
        } else {
            $users = User2s::where('firebase', '!=', null)->where('verification', 1)->where('country', '!=', 'Turkey (+90)')->get();
        }

        if($request->date != null) {
            $date = Carbon\Carbon::createFromFormat('m/d/Y H:i', $request->date . ' ' . $request->time)->toDateTimeString();
        } else {
            $date = null;
        }

        foreach($users as $key => $val) {

            $send = new Support();
            $send->user_id = $val->id;
            $send->message = $request->message;
            $send->owner = 2;
            $send->date = Carbon\Carbon::now();
            $send->validity_date = $date;
            $send->save();

        }
        
        if($send) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }

        
        
    }

}