<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use DB;
use Sentinel;
use Carbon;
use App\User;
use App\UserReferences;
use App\Exchanges;
use App\Support;
use App\Rooms;
use App\Boxes;

class DashboardController extends Controller {

    public function getDashboard() {
        
        /*
        $boxes = Boxes::get();
        
        foreach ($boxes as $key => $value) {
            $rooms = json_decode($value->rooms , true);
            if (in_array('ankara', $rooms)) { 
                unset($rooms[$key]);
            }
            $rooms = json_encode($rooms );
            $update = Boxes::where('number', $value->number)->update(['rooms' => $rooms]);
        }
        */
        
        /*
        $boxes = Boxes::get();
        
        $all = "";
        
        foreach($boxes as $key => $val) {
            $all .= $val->rooms .' - ';
        }
        
        
        $rooms = Rooms::get();
        foreach($rooms as $key => $val) {
            $count = substr_count($all, $val->slug);
            if($count >= 9999) {
                echo $val->slug . ' -> ' . $count . '<br />';
            }
            
        }
        
        
        exit;
        */
        // Sets our destination URL
        /*
        $endpoint_url = 'https://accounts.google.com/o/oauth2/token';

        // Creates our data array that we want to post to the endpoint
        $data_to_post = [
            'code' => '4/XQA5iBrh_8Abp1XWspZtjiI1HtybOqM1wvgfxxti3j_HttNtqXMZIWtXHV32wH0kdTyh_s40WjhAtPTjeo3H0vc',
            'client_id' => '742134497315-4g333n2mpc0eaf4iabfqv0b642pb958g.apps.googleusercontent.com',
            'client_secret' => 'BneGwxJZweACU0RgnIij4G3S',
            'redirect_uri' => 'https://luckybox-6cfab.firebaseapp.com/__/auth/handler',
            'grant_type' => 'authorization_code',
        ];

        // Sets our options array so we can assign them all at once
        $options = [
            CURLOPT_URL        => $endpoint_url,
            CURLOPT_POST       => true,
            CURLOPT_POSTFIELDS => $data_to_post,
        ];

        // Initiates the cURL object
        $curl = curl_init();

        // Assigns our options
        curl_setopt_array($curl, $options);

        // Executes the cURL POST
        $results = curl_exec($curl);

        dd($results);

        // Be kind, tidy up!
        curl_close($curl);

        exit;



        
        $curl = curl_init();
        $authorization = "Authorization: Bearer ya29.GlwbBrciwoVof5AnAsLpoEZQkbSeAScFSvFupIxfgokwX_kqiEPqd5iXkTGDsGJTOK_NwJyBNO2b_Vwrn3WALFRj2bpi1WE3dnpePYlqRMpiadSjrFcesfVikOxLIw";
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization )); 
        curl_setopt($curl, CURLOPT_URL, "https://www.googleapis.com/androidpublisher/v2/applications/com.lucky.box/purchases/products/bronze_tr/tokens/fndnhdeelfpkaaomjokmcbel.AO-J1OyYyy4Mz_Ilo_JG8sf8tnX-OXj2BH8IWYeUHIVOR6TdPERCD9ZshNLah-9MpcSDNGWGT6sCT9ITwc7UhG9KcJ9XSv6fO7d_CV9t557N1lsBNG5u7Jk");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        $result=curl_exec($curl);
        $json = json_decode($result);

        print_r($json->orderId);
        curl_close($curl);

        exit;

        /*
    	$tumu = User::where('verification', 1)->get();
    	foreach($tumu as $val) {
    		$send = new Support();
            $send->user_id = $val->id;
            if($val->country == "Turkey (+90)") {
                $send->message = "Hesabınızın güvenliği ve şifre sıfırlama işlemlerini yapabilmek için 'Hesabım' menüsünden e-posta adresinizi girmeyi unutmayın!";
            } else {
                $send->message = "Do not forget to enter your email address on the 'My Account' menu in order to be able to secure your account and reset your password!";
            }
            $send->owner = 2;
            $send->date = Carbon\Carbon::now();
            $send->save();
    	}
        */


        /*$serefsizler = User::get();

        foreach($serefsizler as $val) {
            $refCount = UserReferences::where('reference_id', $val->id)->count();
            if($val->reference_count > $refCount) {
                $fark = $val->reference_count - $refCount;
                $deleteTickets = User::where('id', $val->id)->decrement('ticket', $fark);
            }
        }*/
/*
        $tumu = User::where('verification', 1)->get();
        foreach($tumu as $val) {
            $send = new Support();
            $send->user_id = $val->id;
            if($val->country == "Turkey (+90)") {
                $send->message = "3 HEDİYE BONUS bilet almak için anasayfada bulunan bonus formundaki kutuya LUCKY yazarak bonusu al butonuna basın!";
            } else {
                $send->message = "Your bonus code to receive 3 gift bonuses: LUCKY";
            }
            $send->owner = 2;
            $send->date = Carbon\Carbon::now();
            $send->save();
        }
*/

    	$users = DB::table('users2s')
             ->select('verification', DB::raw('count(*) as total'))
             ->groupBy('verification')
             ->pluck('total','verification')->all();

        $os = DB::table('users2s')
             ->select('os', DB::raw('count(*) as total'))
             ->groupBy('os')
             ->pluck('total','os')->all();

        $blockeds = User::where('blocked', 1)->count();

        $exchanges = Exchanges::count();

        $countries = DB::table('users2s')
                ->where('verification', 1)
                ->where('country', '!=', null)
                ->where('blocked', 0)
                ->select('country', DB::raw('count(*) as total'))
                ->groupBy('country')
                ->orderBy('total', 'desc')
                ->get();

            /*
            $headers = array(
                'Accept:application/json',
                'Content-Type:application/json',
                'Authorization: 27730EFD-B43B-4D11-A36B-9DBFB276DD4F'
            );
                        
            $ch = curl_init();
            // Set query data here with the URL
            curl_setopt($ch, CURLOPT_URL, 'https://api.checkmobi.com/v1/my-account'); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 3);
            $content = trim(curl_exec($ch));
            curl_close($ch);

            $json = json_decode($content, true);

            dd($json);
            */


        /*
        $array = array("kalpol@outlook.com","keiji@att.net","fhirsch@live.com","gbacon@aol.com","lamky@me.com","lbaxter@verizon.net","denism@verizon.net","szymansk@me.com","kmiller@att.net","elmer@sbcglobal.net","pedwards@aol.com","bmidd@comcast.net","reziac@comcast.net","nelson@msn.com","jpflip@msn.com","jonathan@att.net","rgarcia@yahoo.ca","pmint@yahoo.com","hauma@icloud.com","purvis@aol.com","kjetilk@live.com","benits@aol.com","danzigism@comcast.net","horrocks@mac.com","solomon@me.com","jacks@mac.com","adamk@optonline.net","esasaki@icloud.com","mhassel@me.com","gtewari@me.com","vganesh@live.com","aschmitz@comcast.net","shrapnull@yahoo.com","aukjan@comcast.net","amaranth@sbcglobal.net","jugalator@aol.com","wbarker@outlook.com","crobles@att.net","dobey@yahoo.com","penna@mac.com","ghaviv@mac.com","sblack@outlook.com","jmorris@comcast.net","munson@hotmail.com","mnemonic@optonline.net","mhouston@yahoo.ca","klaudon@verizon.net","kewley@me.com","laird@msn.com","floxy@att.net","pkplex@comcast.net","naupa@gmail.com","mrsam@yahoo.ca","sabren@att.net","heroine@msn.com","jrkorson@mac.com","jgoerzen@verizon.net","mrsam@verizon.net","mschilli@yahoo.ca","petersko@sbcglobal.net","milton@gmail.com","drolsky@mac.com","rjones@verizon.net","tedrlord@comcast.net","jginspace@att.net","noodles@msn.com","rtanter@verizon.net","ngedmond@gmail.com","ghost@aol.com","pkplex@hotmail.com","stefano@mac.com","citadel@icloud.com","psichel@icloud.com","simone@me.com","gmcgath@sbcglobal.net","retoh@optonline.net","osrin@yahoo.ca","louise@att.net","erynf@yahoo.com","vertigo@att.net","oneiros@yahoo.com","miami@sbcglobal.net","rtanter@yahoo.ca","ismail@yahoo.com","carmena@gmail.com","tubesteak@gmail.com","quantaman@gmail.com","paina@yahoo.com","djpig@live.com","malin@verizon.net","pajas@yahoo.com","msusa@sbcglobal.net","psichel@me.com","isaacson@sbcglobal.net","gtewari@yahoo.ca","sjava@yahoo.com","ylchang@msn.com","brbarret@att.net","dgatwood@verizon.net","neonatus@verizon.net","monopole@mac.com","froodian@mac.com","wilsonpm@sbcglobal.net","calin@att.net","matthijs@me.com","juliano@verizon.net","qrczak@sbcglobal.net","redingtn@outlook.com","dpitts@me.com","dburrows@yahoo.ca","fatelk@live.com","joglo@verizon.net","grinder@yahoo.ca","uqmcolyv@yahoo.ca","aprakash@me.com","muadip@msn.com","bancboy@att.net","portele@optonline.net","leviathan@gmail.com","stomv@mac.com","roesch@msn.com","moonlapse@icloud.com","drjlaw@outlook.com","gordonjcp@gmail.com","kingma@outlook.com","willg@aol.com","grinder@comcast.net","heidrich@mac.com","stevelim@att.net","drjlaw@yahoo.ca","chrwin@att.net","empathy@outlook.com","oevans@comcast.net","ryanvm@mac.com","marioph@aol.com","chunzi@verizon.net","gommix@verizon.net","janusfury@yahoo.com","tbmaddux@gmail.com","jelmer@msn.com","paley@hotmail.com","bogjobber@optonline.net","pappp@msn.com","mcsporran@att.net","cgarcia@msn.com","stomv@aol.com","dowdy@hotmail.com","joehall@mac.com","fglock@comcast.net","benanov@me.com","djupedal@mac.com","spadkins@msn.com","rtanter@yahoo.ca","jfinke@verizon.net","crandall@yahoo.com","pajas@hotmail.com","jadavis@yahoo.com","pappp@comcast.net","tangsh@hotmail.com","rsteiner@aol.com","akoblin@live.com","gravyface@gmail.com","gbacon@live.com","matty@aol.com","euice@msn.com","mcmillan@outlook.com","jsbach@live.com","lcheng@sbcglobal.net","jsbach@yahoo.ca","ngedmond@verizon.net","mailarc@comcast.net","mjewell@optonline.net","kdawson@optonline.net","biglou@att.net","monkeydo@mac.com","crandall@yahoo.ca","gmcgath@gmail.com","yumpy@yahoo.com","empathy@icloud.com","jrkorson@sbcglobal.net","mrsam@verizon.net","punkis@hotmail.com","jcholewa@optonline.net","janneh@mac.com","haddawy@yahoo.ca","rsteiner@sbcglobal.net","jbailie@yahoo.ca","ateniese@sbcglobal.net","crimsane@hotmail.com","ewaters@icloud.com","research@mac.com","raides@icloud.com","mrobshaw@gmail.com","gozer@yahoo.com","dhrakar@yahoo.ca","iamcal@verizon.net","arnold@outlook.com","monopole@live.com","dhwon@outlook.com","tezbo@me.com","webdragon@outlook.com","odlyzko@yahoo.com","skoch@me.com","bruck@verizon.net","jbailie@msn.com","ateniese@optonline.net","seano@comcast.net","novanet@comcast.net","yxing@att.net","cparis@mac.com","dobey@verizon.net","garyjb@mac.com","jorgb@me.com","giafly@yahoo.com","makarow@aol.com","wonderkid@gmail.com","sjava@att.net","zeitlin@outlook.com","hwestiii@verizon.net","helger@mac.com","hikoza@att.net","dowdy@aol.com","maradine@hotmail.com","ianbuck@mac.com","tristan@sbcglobal.net","crusader@live.com","ninenine@me.com","cvrcek@aol.com","dhrakar@aol.com","improv@sbcglobal.net","josephw@gmail.com","benits@gmail.com","jespley@yahoo.ca","report@aol.com","dartlife@yahoo.ca","sokol@live.com","gomor@mac.com","crobles@icloud.com","dkasak@comcast.net","pizza@msn.com","attwood@sbcglobal.net","trygstad@msn.com","jkegl@optonline.net","hmbrand@msn.com","zyghom@comcast.net","milton@outlook.com","eimear@hotmail.com","temmink@mac.com","muadip@hotmail.com","scato@yahoo.ca","rgiersig@sbcglobal.net","heckerman@verizon.net","thomasj@sbcglobal.net","fraser@comcast.net","karasik@comcast.net","murty@sbcglobal.net","jbearp@mac.com","arnold@comcast.net","benits@outlook.com","jfreedma@verizon.net","carreras@hotmail.com","louise@icloud.com","sethbrown@yahoo.com","chlim@mac.com","gator@me.com","dmouse@yahoo.ca","kwilliams@icloud.com","sharon@optonline.net","henkp@comcast.net","timtroyr@att.net","mlewan@sbcglobal.net","mbalazin@outlook.com","tedrlord@icloud.com","crobles@optonline.net","ryanvm@att.net","esasaki@optonline.net","nwiger@yahoo.com","ilyaz@mac.com","mchugh@msn.com","garyjb@live.com","maratb@gmail.com","boser@live.com","vmalik@att.net","webteam@me.com","guialbu@gmail.com","milton@mac.com","johndo@outlook.com","moinefou@msn.com","firstpr@att.net","vsprintf@comcast.net","marnanel@icloud.com","skajan@att.net","jfriedl@sbcglobal.net","mwitte@outlook.com","erynf@me.com","cosimo@msn.com","ahuillet@optonline.net","wsnyder@yahoo.com","bockelboy@sbcglobal.net","milton@mac.com","janusfury@gmail.com","rfisher@gmail.com","psharpe@comcast.net","granboul@mac.com","jshirley@me.com","danny@optonline.net","dbindel@optonline.net","shawnce@yahoo.ca","grdschl@gmail.com","satch@aol.com","agapow@hotmail.com","laird@yahoo.ca","makarow@yahoo.com","sassen@yahoo.ca","crypt@comcast.net","adhere@gmail.com","osrin@optonline.net","aardo@msn.com","koudas@sbcglobal.net","forsberg@att.net","yenya@icloud.com","geekgrl@sbcglobal.net","mhassel@me.com","pthomsen@yahoo.com","kdawson@gmail.com","cparis@yahoo.ca","stakasa@mac.com","druschel@yahoo.com","psichel@hotmail.com","dialworld@msn.com","skajan@yahoo.com","knorr@gmail.com","gator@me.com","jcholewa@icloud.com","heine@gmail.com","zwood@verizon.net","philen@yahoo.ca","staikos@me.com","wsnyder@yahoo.com","pontipak@comcast.net","hermes@aol.com","eidac@outlook.com","claesjac@sbcglobal.net","quantaman@hotmail.com","matsn@gmail.com","wildixon@live.com","wenzlaff@outlook.com","mfburgo@icloud.com","stomv@gmail.com","techie@gmail.com","tubesteak@me.com","jadavis@optonline.net","drolsky@live.com","danneng@sbcglobal.net","froodian@me.com","grolschie@verizon.net","kmself@live.com","ideguy@verizon.net","mdielmann@outlook.com","crusader@mac.com","vganesh@yahoo.ca","alias@gmail.com","benits@msn.com","dsowsy@verizon.net","amimojo@me.com","arandal@att.net","flakeg@comcast.net","yzheng@me.com","jkegl@yahoo.com","clkao@aol.com","mahbub@me.com","hauma@comcast.net","yruan@icloud.com","ramollin@icloud.com","esbeck@live.com","dmiller@live.com","world@comcast.net","ingolfke@att.net","nelson@me.com","hstiles@comcast.net","pplinux@icloud.com","dinther@comcast.net","isotopian@sbcglobal.net","khris@optonline.net","tellis@me.com","mastinfo@verizon.net","konst@verizon.net","lbaxter@yahoo.com","nachbaur@aol.com","kempsonc@icloud.com","froodian@hotmail.com","gemmell@me.com","leakin@yahoo.com","hauma@comcast.net","naoya@comcast.net","cyrus@aol.com","sumdumass@live.com","srour@verizon.net","alfred@msn.com","baveja@yahoo.ca","speeves@optonline.net","feamster@aol.com","bflong@outlook.com","miami@icloud.com","webteam@sbcglobal.net","hoyer@att.net","mkearl@hotmail.com","cumarana@live.com","gtewari@live.com","bartak@aol.com","daveewart@optonline.net","duncand@comcast.net","bsikdar@yahoo.com","bruck@comcast.net","bdthomas@hotmail.com","hakim@optonline.net","kassiesa@hotmail.com","hikoza@mac.com","zilla@outlook.com","subir@sbcglobal.net","teverett@sbcglobal.net","ajlitt@msn.com","pereinar@mac.com","sequin@comcast.net","sartak@me.com","solomon@aol.com","sokol@gmail.com","thrymm@live.com","noticias@mac.com","iapetus@aol.com","dougj@comcast.net","evans@verizon.net","louise@hotmail.com","denton@att.net","bruck@gmail.com","iapetus@me.com","drewf@aol.com","dkrishna@optonline.net","goldberg@outlook.com","msusa@comcast.net","openldap@aol.com","gavinls@gmail.com","overbom@icloud.com","jgoerzen@msn.com","gomor@live.com","gbacon@me.com","scotfl@gmail.com","pmint@aol.com","harryh@outlook.com","miami@icloud.com","bahwi@mac.com","dvdotnet@me.com","catalog@yahoo.com","ryanvm@hotmail.com","alastair@gmail.com","jshirley@sbcglobal.net","jfmulder@comcast.net","evilopie@yahoo.ca","mhouston@msn.com","dialworld@live.com","afifi@att.net","cantu@gmail.com","jbuchana@att.net","miturria@att.net","dgriffith@aol.com","mchugh@comcast.net","library@yahoo.com","caidaperl@hotmail.com","greear@msn.com","rsteiner@msn.com","vertigo@yahoo.ca","carreras@hotmail.com","cosimo@verizon.net","hikoza@icloud.com","papathan@gmail.com","msherr@msn.com","shrapnull@verizon.net","cparis@me.com","jaesenj@aol.com","sharon@att.net","dsowsy@gmail.com","murdocj@msn.com","geekgrl@optonline.net","daveewart@mac.com","suresh@att.net","pspoole@comcast.net","lbecchi@optonline.net","stellaau@sbcglobal.net","morain@gmail.com","falcao@comcast.net","jaffe@aol.com","bahwi@hotmail.com","johndo@mac.com","darin@optonline.net","rcwil@verizon.net","rasca@outlook.com","miami@verizon.net","ateniese@comcast.net","bastian@msn.com","andersbr@gmail.com","lcheng@optonline.net","privcan@live.com","durist@yahoo.com","howler@comcast.net","quantaman@aol.com","pspoole@yahoo.ca","madanm@outlook.com","andersbr@hotmail.com","pedwards@yahoo.com","skajan@msn.com","grothoff@optonline.net","mpiotr@att.net","drolsky@aol.com","keijser@hotmail.com","heine@aol.com","kwilliams@yahoo.ca","keiji@aol.com","mpiotr@icloud.com","jonas@live.com","murdocj@sbcglobal.net","bbirth@aol.com","ninenine@me.com","xnormal@mac.com","hahiss@optonline.net","uraeus@live.com","leviathan@yahoo.com","petersko@icloud.com","konit@outlook.com","bogjobber@yahoo.com","tangsh@aol.com","naoya@me.com","natepuri@att.net","kjetilk@yahoo.ca","mbrown@yahoo.com","dpitts@msn.com","gumpish@mac.com","dwendlan@optonline.net","rhialto@att.net","bester@att.net","ozawa@gmail.com","gslondon@icloud.com","wkrebs@msn.com","msloan@hotmail.com","madanm@outlook.com","jeteve@verizon.net","pakaste@att.net","carroll@me.com","larry@me.com","mallanmba@yahoo.ca","jsmith@optonline.net","pfitza@hotmail.com","adillon@outlook.com","ivoibs@icloud.com","temmink@me.com","kuparine@icloud.com","nichoj@hotmail.com","jusdisgi@yahoo.com","guialbu@hotmail.com","dvdotnet@hotmail.com","chaikin@yahoo.com","mkearl@msn.com","kmiller@msn.com","wikinerd@hotmail.com","moxfulder@hotmail.com","uraeus@live.com","sherzodr@hotmail.com","amaranth@aol.com","snunez@yahoo.ca","galbra@icloud.com","bockelboy@att.net","kayvonf@yahoo.ca","nicktrig@msn.com","juliano@me.com","enintend@sbcglobal.net","bryanw@gmail.com","gregh@gmail.com","chunzi@msn.com","jgmyers@optonline.net","fwiles@outlook.com","dsugal@yahoo.com","damian@mac.com","william@att.net","mwandel@verizon.net","dburrows@hotmail.com","oechslin@comcast.net","moonlapse@msn.com","bsikdar@sbcglobal.net","ngedmond@yahoo.com","overbom@att.net","giafly@me.com","marioph@live.com","bryam@att.net","donev@me.com","world@mac.com","tromey@hotmail.com","debest@sbcglobal.net","emmanuel@icloud.com","gboss@me.com","drolsky@att.net","plover@att.net","dialworld@yahoo.com","telbij@mac.com","martyloo@mac.com","conteb@hotmail.com","world@att.net","lstaf@live.com","mmccool@mac.com","kildjean@aol.com","daveed@yahoo.com","mavilar@att.net","bjornk@outlook.com","bartak@live.com","airship@aol.com","wiseb@outlook.com","fatelk@yahoo.com","airship@gmail.com","jbarta@yahoo.ca","comdig@gmail.com","wainwrig@me.com","pakaste@comcast.net","brbarret@live.com","miami@hotmail.com","studyabr@live.com","sfoskett@aol.com","comdig@att.net","scarlet@att.net","parksh@optonline.net","jelmer@yahoo.ca","rhialto@aol.com","flakeg@mac.com","forsberg@att.net","grinder@icloud.com","gastown@me.com","fglock@live.com","fatelk@att.net","report@att.net","pspoole@gmail.com","tellis@msn.com","lamky@aol.com","zeitlin@aol.com","schwaang@sbcglobal.net","gregh@att.net","joglo@sbcglobal.net","cameron@att.net","dwsauder@gmail.com","pplinux@yahoo.ca","rattenbt@optonline.net","earmstro@optonline.net","muzzy@optonline.net","kannan@mac.com","gmcgath@verizon.net","jkegl@comcast.net","jamuir@gmail.com","msherr@aol.com","hling@hotmail.com","bonmots@yahoo.ca","ebassi@me.com","skajan@verizon.net","oevans@optonline.net","dwsauder@att.net","nasor@yahoo.com","wortmanj@msn.com","bancboy@comcast.net","twoflower@outlook.com","alias@msn.com","hellfire@optonline.net","mccurley@yahoo.com","darin@live.com","philb@outlook.com","nelson@outlook.com","mcast@yahoo.com","pspoole@optonline.net","jdhedden@sbcglobal.net","lipeng@mac.com","marcs@optonline.net","tellis@comcast.net","reziac@optonline.net","michiel@att.net","agolomsh@mac.com","netsfr@me.com","heroine@icloud.com","engelen@msn.com","aibrahim@me.com","flavell@att.net","jwarren@aol.com","dsugal@mac.com","ullman@sbcglobal.net","aaribaud@comcast.net","emmanuel@aol.com","sopwith@optonline.net","frikazoyd@live.com","inico@yahoo.com","mrsam@gmail.com","airship@comcast.net","granboul@comcast.net","brainless@live.com","kodeman@msn.com","dobey@comcast.net","mhassel@me.com","uncle@live.com","ahuillet@comcast.net","dmbkiwi@comcast.net","lamky@aol.com","ninenine@verizon.net","keijser@icloud.com","chinthaka@sbcglobal.net","hakim@sbcglobal.net","jesse@yahoo.com","thurston@optonline.net","aardo@verizon.net","burns@outlook.com","stinson@optonline.net","garland@optonline.net","tbusch@verizon.net","michiel@gmail.com","carmena@yahoo.com","gospodin@yahoo.ca","uraeus@gmail.com","lbecchi@sbcglobal.net","mbalazin@me.com","ebassi@sbcglobal.net","rfisher@yahoo.ca","caronni@me.com","jmcnamara@icloud.com","gbacon@live.com","lamprecht@msn.com","breegster@comcast.net","fmerges@mac.com","spadkins@hotmail.com","frode@aol.com","heidrich@live.com","gumpish@outlook.com","campware@live.com","wayward@sbcglobal.net","noodles@mac.com","mthurn@comcast.net","pspoole@hotmail.com","eabrown@msn.com","dkasak@icloud.com","rwelty@hotmail.com","pereinar@mac.com","paina@att.net","lydia@att.net","skoch@comcast.net","isotopian@att.net","mhassel@comcast.net","atmarks@comcast.net","szymansk@live.com","hamilton@att.net","mhouston@sbcglobal.net","phizntrg@gmail.com","pmint@live.com","thrymm@yahoo.com","durist@outlook.com","hoangle@mac.com","morain@verizon.net","fraser@verizon.net","mbalazin@mac.com","dhrakar@mac.com","msroth@optonline.net","rmcfarla@optonline.net","bryam@gmail.com","corrada@me.com","madanm@yahoo.ca","peterhoeg@comcast.net","tbmaddux@live.com","parents@outlook.com","bester@icloud.com","kdawson@yahoo.com","jaxweb@comcast.net","tellis@mac.com","kronvold@gmail.com","lpalmer@aol.com","jrkorson@yahoo.com","openldap@icloud.com","bmidd@aol.com","malattia@att.net","drhyde@live.com","yangyan@att.net","jgwang@verizon.net","karasik@mac.com","sabren@me.com","flaviog@outlook.com","nimaclea@me.com","mhanoh@hotmail.com","multiplx@sbcglobal.net","aprakash@yahoo.com","kobayasi@hotmail.com","adamk@me.com","thurston@optonline.net","isorashi@icloud.com","rbarreira@aol.com","munjal@hotmail.com","bmcmahon@mac.com","boomzilla@comcast.net","bwcarty@optonline.net","ideguy@yahoo.ca","doormat@yahoo.ca","fwiles@yahoo.com","fraser@comcast.net","twoflower@yahoo.com","kevinm@me.com","gavinls@yahoo.ca","zeitlin@outlook.com","leakin@me.com","naupa@aol.com","jnolan@gmail.com","smpeters@sbcglobal.net","scotfl@live.com","staffelb@yahoo.ca","onestab@hotmail.com","gemmell@yahoo.com","atmarks@gmail.com","hampton@comcast.net","weidai@optonline.net","elmer@mac.com","wiseb@att.net","simone@live.com","lstaf@sbcglobal.net","mgreen@optonline.net","rhialto@outlook.com","emcleod@att.net","gator@yahoo.com","rgarton@sbcglobal.net","haddawy@msn.com","mdielmann@att.net","paley@att.net","stecoop@yahoo.com","rupak@optonline.net","dmath@me.com","pavel@outlook.com","njpayne@sbcglobal.net","adillon@hotmail.com","jaxweb@yahoo.com","raides@aol.com","mugwump@live.com","seanq@yahoo.ca","pgottsch@icloud.com","wojciech@me.com","miltchev@me.com","pakaste@hotmail.com","knorr@me.com","chance@gmail.com","sjava@mac.com","weazelman@gmail.com","dbrobins@icloud.com","bcevc@me.com","rasca@msn.com","jeteve@gmail.com","skajan@me.com","bmidd@gmail.com","chaffar@yahoo.com","roesch@optonline.net","choset@yahoo.com","odlyzko@hotmail.com","yxing@optonline.net","errxn@aol.com","jamuir@yahoo.ca","arachne@att.net","munson@outlook.com","hedwig@yahoo.com","cgarcia@live.com","zilla@outlook.com","timlinux@live.com","marnanel@gmail.com","graham@icloud.com","granboul@msn.com","gtewari@aol.com","janusfury@live.com","bmcmahon@yahoo.ca","euice@optonline.net","carroll@msn.com","tedrlord@att.net","rwelty@comcast.net","parksh@sbcglobal.net","ahuillet@optonline.net","dialworld@gmail.com","snunez@yahoo.com","dleconte@optonline.net","jpflip@yahoo.ca","johndo@comcast.net","uraeus@sbcglobal.net","kingma@outlook.com","msloan@outlook.com","ivoibs@gmail.com","fraser@comcast.net","ilial@yahoo.com","dmath@live.com","donev@aol.com","gumpish@optonline.net","rfisher@me.com","thaljef@msn.com","bolow@sbcglobal.net","cliffski@comcast.net","michiel@gmail.com","gslondon@yahoo.com","fviegas@mac.com","mbalazin@gmail.com","koudas@yahoo.com","hakim@me.com","carcus@att.net","campbell@sbcglobal.net","amcuri@mac.com","ccohen@gmail.com","tmaek@optonline.net","jugalator@me.com","attwood@optonline.net","rohitm@mac.com","roamer@comcast.net","padme@me.com","ullman@me.com","notaprguy@yahoo.ca","heidrich@hotmail.com","bulletin@att.net","tedrlord@outlook.com","rgiersig@comcast.net","rupak@verizon.net","seanq@comcast.net","cremonini@hotmail.com","grady@yahoo.ca","mwitte@mac.com","reeds@comcast.net","biglou@me.com","sthomas@msn.com","giafly@comcast.net","mschilli@att.net","papathan@yahoo.com","bastian@verizon.net","emcleod@mac.com","ideguy@gmail.com","jbarta@verizon.net","jdray@outlook.com","lushe@optonline.net","rsmartin@yahoo.ca","atmarks@gmail.com","fangorn@icloud.com","sokol@sbcglobal.net","wortmanj@gmail.com","pmint@msn.com","rgarton@sbcglobal.net","naoya@mac.com","kludge@att.net","rafasgj@yahoo.ca","rupak@optonline.net","seemant@hotmail.com","mastinfo@live.com","schumer@msn.com","garyjb@verizon.net","heine@hotmail.com","gozer@me.com","ninenine@live.com","gomor@verizon.net","mcmillan@yahoo.ca","hutton@att.net","william@optonline.net","curly@verizon.net","ewaters@yahoo.ca","bdthomas@live.com","jmorris@hotmail.com","hllam@hotmail.com","mhoffman@aol.com","jguyer@icloud.com","duncand@hotmail.com","lstein@aol.com","jsbach@msn.com","shaffei@mac.com","chunzi@msn.com","gomor@mac.com","ghaviv@aol.com","ryanvm@msn.com","manuals@aol.com","portele@gmail.com","kdawson@msn.com","itstatus@mac.com","wildfire@hotmail.com","ghaviv@verizon.net","bigmauler@hotmail.com","phyruxus@outlook.com","geeber@mac.com","peterhoeg@aol.com","jshirley@optonline.net","miturria@att.net","juerd@outlook.com","lydia@msn.com","maikelnai@optonline.net","drhyde@msn.com","payned@optonline.net","malattia@msn.com","dkrishna@gmail.com","sassen@live.com","matty@yahoo.ca","kassiesa@verizon.net","crowl@comcast.net","harryh@me.com","sharon@icloud.com","nwiger@gmail.com","qrczak@yahoo.ca","mugwump@icloud.com","cgreuter@gmail.com","shang@gmail.com");

        foreach($array as $val) {

            $check = User::where('email', $val)->count();
            if($check == 0) {
                $new = new User();
                $new->email = $val;
                $new->email_2 = $val;
                $new->password = "password";
                $new->first_name = explode("@", $val)[0];
                $new->phone = "999999999999";
                $new->ticket = 0;
                $new->is_fake = 1;
                $new->save();
            }
            
        }    
        */
        
        return view('admin/dashboard', compact('users', 'blockeds', 'exchanges', 'countries', 'os'));

  


    }
    

}