<?php

namespace App\Http\Controllers\Admin\ProductManagement;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use App\ProductCategories;

class ProductCategoriesController extends Controller {
    
    public function getProductCategories() {
        
        $categories = ProductCategories::orderBy('name', 'asc')->get();
        
        return view('admin/product-management/product-categories', compact('categories'));
        
    }
    
    public function setNewProductCategory(Request $request) {
        
        $slug = Illaoi::generateUnique($request->name, new ProductCategories);
        
        $add = new ProductCategories();
        if($request->id != null) {
            $add->id = $request->id;
        }
        $add->slug = $slug;
        $add->name = $request->name;
        $add->icon = $request->icon;
        if($request->parent_id) {
            $add->parent_id = $request->parent_id;
        }
        $add->order = $request->order;
        $add->is_top = $request->is_top;
        $add->is_new = $request->is_new;
        $add->is_active = $request->is_active;
        $add->save();
        
        if($add) {
            Alert::success('Kategori oluşturuldu!');
            return Redirect::back();
        } else {
            Alert::error('Kategori oluşturulamadı!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
    public function setDeleteProductCategory(Request $request) {
        
        $delete = ProductCategories::where('id', $request->category_id)->delete();
        
        if($delete) {
            Alert::success('Kategori silindi!');
            return Redirect::back();
        } else {
            Alert::error('Kategori silinemedi!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }
    
}

