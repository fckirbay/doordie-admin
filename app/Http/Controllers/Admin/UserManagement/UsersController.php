<?php

namespace App\Http\Controllers\Admin\UserManagement;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use DB;
use Input;
use Validator;
use Illaoi;
use Carbon;
use App\User;
use App\Games;
use App\Transactions;
use App\Notifications;
use App\ClickRewarded;
use App\Bonuses;
use App\Offerwall;
use App\UserLocations;
use App\Support;
use App\PremiumSales;
use App\ReadyAnswers;
use App\PasswordRecovery;
use App\UserNotes;
use App\Users2s;
use App\UserJokers;

class UsersController extends Controller {
    
    public function getUsers() {        

        if(isset($_GET['search'])) {
            if(isset($_GET['order'])) {
                $users = Users2s::where('id', $_GET['search'])->orwhere('first_name', 'LIKE', '%'.$_GET['search'].'%')->orwhere('phone', 'LIKE', '%'.$_GET['search'].'%')->orwhere('email', 'LIKE', '%'.$_GET['search'].'%')->orderBy($_GET['order'], 'desc')->paginate(50);
            } else {
                $users = Users2s::where('id', $_GET['search'])->orwhere('first_name', 'LIKE', '%'.$_GET['search'].'%')->orwhere('phone', 'LIKE', '%'.$_GET['search'].'%')->orwhere('email', 'LIKE', '%'.$_GET['search'].'%')->orderBy('id', 'desc')->paginate(50);
            }
        } elseif(isset($_GET['order'])) {
            $users = Users2s::orderBy($_GET['order'], 'desc')->paginate(100);
        } else {
            $users = Users2s::orderBy('id', 'desc')->paginate(50);
        }
        
        return view('admin/user-management/users', compact('users'));
        
    }
    
    public function getUser($id) {
        
        $user = Users2s::where('id', $id)->first();

        $jokers = UserJokers::where('user_id', $id)->first();
        
        $games = Games::where('user_id', $id)->orderBy('id', 'desc')->get();
        
        $transactions = Transactions::where('user_id', $id)->orderBy('id', 'desc')->get();

        $references = Users2s::where('reference_id', $id)->where('verification', 1)->get();

        $rewardeds = ClickRewarded::where('user_id', $id)->where('type', 1)->where('is_completed', 1)->orderBy('id', 'desc')->get();

        $bonuses = Bonuses::where('user_id', $id)->orderBy('id', 'desc')->get();

        //$exchanges = Exchanges::where('user_id', $id)->orderBy('id', 'desc')->get();

        //$scratches = ScratchWin::where('user_id', $id)->whereIn('is_completed', array(1,2))->orderBy('id', 'desc')->get();

        $offerwall = Offerwall::where('user_id', $id)->orderBy('id', 'desc')->get();
        $offerwall_total = 0;
        foreach($offerwall as $key => $val) {
            $offerwall_total += $val->usd_value;
        }

        $won = Games::where('user_id', $id)->where('is_completed', '1')->where('coins', '>', 0)->count();

        $lost = Games::where('user_id', $id)->where('is_completed', '1')->where('coins', 0)->count();

        $otherAccounts = Users2s::where('id', '!=', $user->id)->where('firebase', '!=', null)->where('firebase', $user->firebase)->get();

        $recovers = PasswordRecovery::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();

        $userLocations = UserLocations::where('user_id', $user->id)->orderBy('id', 'desc')->get();

        $messages = Support::where('user_id', $user->id)->where('validity_date', null)->orwhere('user_id', $user->id)->where('validity_date', '>=', Carbon\Carbon::now())->orderBy('id', 'desc')->limit(50)->get();

        $readyAnswers = ReadyAnswers::get();

        $notes = UserNotes::where('user_id', $user->id)->orderBy('id', 'desc')->get();

        return view('admin/user-management/user', compact('user', 'jokers', 'games', 'transactions', 'references', 'rewardeds', 'bonuses', 'offerwall', 'offerwall_total', 'otherAccounts', 'recovers', 'userLocations', 'messages', 'readyAnswers', 'notes', 'won', 'lost'));
        
    }
    
    public function setUser(Request $request) {
        
        $update = Users2s::where('id', $request->user_id)->first();
        //$update->first_name = $request->first_name;
        //$update->tc_number = $request->tc_number;
        $update->ticket = $request->ticket;
        $update->blocked = $request->blocked;
        $update->blocked_reason = $request->blocked_reason;
        $update->support = $request->support;
        $update->verification = $request->verification;
        $update->lang = $request->lang;
        //$update->currency = $request->currency;
        //$update->email_2 = $request->email_2;
        $update->balance = $request->balance;
        $update->os = $request->os;
        $update->is_winner = $request->is_winner;
        //$update->purchasing = $request->purchasing;
        $update->verification_tries = $request->verification_tries;
        //$update->app_version = $request->app_version;
        $update->firebase = $request->firebase;
        $update->save();

        if($request->clear_rewardeds == 1) {
            $clear = ClickRewarded::where('user_id', $request->user_id)->where('is_completed', 0)->delete();
        }
        // Şifre değiştirme için bir node servisine çıkılacak...
        /*
        if($request->new_password != null && strlen($request->new_password) > 4) {
            $user = Sentinel::findById($request->user_id);

            $credentials = [
                'password' => $request->new_password,
            ];

            $user = Sentinel::update($user, $credentials);
        }
        */
        
        if($update) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

    public function setUserJokers(Request $request) {
        
        $update = UserJokers::where('user_id', $request->user_id)->first();
        $update->double_earnings = $request->double_earnings;
        $update->pass = $request->pass;
        $update->show_result = $request->show_result;
        $update->continue_left = $request->continue_left;
        $update->save();
        
        if($update) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

    public function setSendMessage(Request $request) {
        
        $send = new Support();
        $send->user_id = $request->user_id;
        $send->message = $request->message;
        $send->owner = 2;
        $send->date = Carbon\Carbon::now();
        $send->save();
        
        if($send) {

            $user = Users2s::where('id', $request->user_id)->select('firebase', 'country')->first();

            $readed = Support::where('user_id', $request->user_id)->where('owner', 1)->where('is_viewed', 0)->update(['is_viewed' => 1]);

            if($user->firebase != null) {
                $sendNotify = new Notifications();
                $sendNotify->user_id = $request->user_id;
                $sendNotify->title = 'Do & Die';
                if($user->country == "TR") {
                    $sendNotify->notification = "Yeni bir mesajınız var!";
                } else {
                    $sendNotify->notification = "You have a new message!";
                }
                $sendNotify->firebase = $user->firebase;
                $sendNotify->save();
            }
            
            Alert::success('İşlem başarılı!');
            return redirect('admin/support');
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

    public function setSendNotification(Request $request) {

        $user = Users2s::where('id', $request->user_id)->first();

        if($user->firebase != null) {
            $send = new Notifications();
            $send->user_id = $request->user_id;
            $send->title = "Do & Die";
            $send->notification = $request->message;
            $send->firebase = $user->firebase;
            $send->save();
        }

        if($send) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
    }

    public function setSendTicket(Request $request) {

        $increment = Users2s::where('id', $request->user_id)->increment('ticket', $request->tickets);

        $newBonus = new Bonuses();
        $newBonus->user_id = $request->user_id;
        $newBonus->bonus_amount = $request->tickets;
        $newBonus->reason = $request->reason;
        $newBonus->date = Carbon\Carbon::now();
        $newBonus->save();
        
        if($newBonus) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
        
        
    }

    public function setPremiumMembership(Request $request) {

        $update = Users2s::where('id', $request->user_id)->update(['premium_membership' => $request->membership, 'premium_expiration' => Carbon\Carbon::now()->addDay()]);

        $newSale = new PremiumSales();
        $newSale->user_id = $request->user_id;
        $newSale->membership_id = $request->membership;
        $newSale->trans_id = $request->trans_id;
        $newSale->purchase_token = "manuel";
        $newSale->expiry_date = Carbon\Carbon::now()->addDay();
        $newSale->status = 1;
        $newSale->save();

        $fromDay = Carbon\Carbon::now()->subHours(24);
        $toDay = Carbon\Carbon::now();
        $videoCompleted = ScratchWin::where('user_id', $request->user_id)->whereIn('is_completed', array(1, 2))->whereBetween('click_date', array($fromDay, $toDay))->update(['click_date' => Carbon\Carbon::now()->subDay(), 'complete_date' => Carbon\Carbon::now()->subDay()]);

        $newTrans = new Transactions();
        $newTrans->user_id = $request->user_id;
        if($request->membership == "bronze_tr" || $request->membership == "bronze_en") {
            $newTrans->amount = 5;
        } elseif($request->membership == "silver_tr" || $request->membership == "silver_en") {
            $newTrans->amount = 10;
        } elseif($request->membership == "gold_tr" || $request->membership == "gold_en") {
            $newTrans->amount = 20;
        } elseif($request->membership == "platinum_tr" || $request->membership == "platinum_en") {
            $newTrans->amount = 30;
        } elseif($request->membership == "vip_tr" || $request->membership == "vip_en") {
            $newTrans->amount = 50;
        }
        $newTrans->type = 2;
        $newTrans->status = 1;
        $newTrans->created_date = Carbon\Carbon::now();
        $newTrans->confirm_date = Carbon\Carbon::now();
        $newTrans->save();
        
        if($newTrans) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
        
        
    }


    public function setPayment(Request $request) {

        $update = Users2s::where('id', $request->user_id)->decrement('balance', $request->amount);
        $nowinner = Users2s::where('id', $request->user_id)->update(['is_winner' => 0]);

        $newTrans = new Transactions();
        $newTrans->user_id = $request->user_id;
        $newTrans->amount = $request->amount;
        $newTrans->type = 1;
        $newTrans->status = 1;
        $newTrans->payment_type = $request->payment_type;
        $newTrans->payment_status = $request->payment_status;
        $newTrans->payment_notes = $request->payment_notes;
        $newTrans->created_date = Carbon\Carbon::now();
        $newTrans->confirm_date = Carbon\Carbon::now();
        $newTrans->save();
        
        if($newTrans) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
        
        
    }

    public function setNewNote(Request $request) {

        
        $newNote = new UserNotes();
        $newNote->admin_id = Sentinel::getUser()->id;
        $newNote->user_id = $request->user_id;
        $newNote->note = $request->note;
        $newNote->date = Carbon\Carbon::now();
        $newNote->save();
        
        if($newNote) {
            Alert::success('İşlem başarılı!');
            return Redirect::back();
        } else {
            Alert::error('İşlem başarısız!', 'Üzgünüz!');
            return Redirect::back();
        }
        
        
        
    }
    
}

