<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Carbon;
use Alert;
use App\User;
use App\ClickRewarded;
use App\Bonuses;

class FreeTicketsController extends Controller {

    public function getFreeTickets() {

        if(Sentinel::check() && Sentinel::getUser()->app_version < config('app.application_version')) {
            return redirect('update');
        }
                
        return view('mobile/free-tickets');

    }

    public function getAwardedTasks() {
                
        return view('mobile/offerwall');

    }

    
    public function setFreeBonus(Request $request) {

    	// Kurban01 yapıldı.
		// September01 yapıldı
        // Bonus01 yapıldı
        // Lucky01 yapıldı
        // Lucky02 yapıldı
        
        $check = Bonuses::where('user_id', Sentinel::getUser()->id)->where('reason', 'Lucky02')->count();

        if($check > 0) {
        	Alert::error(\Lang::get('general.you_already_have_this_bonus'), \Lang::get('general.sorry'))->autoclose(3000);
        	return Redirect::back();
        }

        if(strtolower($request->bonus_code) != "lucky") {
        	Alert::error(\Lang::get('general.invalid_bonus_code'), \Lang::get('general.sorry'))->autoclose(3000);
        	return Redirect::back();
        }

        $newSupport = new Bonuses();
        $newSupport->user_id = Sentinel::getUser()->id;
        $newSupport->bonus_amount = 1;
        $newSupport->reason = "Lucky02";
        $newSupport->date = Carbon\Carbon::now();
        $newSupport->save();

        $inc = User::where('id', Sentinel::getUser()->id)->increment('ticket', 1);

        //Alert::error(\Lang::get('general.your_tc_no_not_found'), \Lang::get('general.congratulations'))->autoclose(3000);
        Alert::success(\Lang::get('general.you_got_the_bonus'), \Lang::get('general.congratulations'))->autoclose(3000);
        return Redirect::back();
    }
    
    
}