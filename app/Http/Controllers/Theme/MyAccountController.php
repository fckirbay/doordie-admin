<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Sentinel;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Alert;

class MyAccountController extends Controller {

    public function getMyAccount() {
        
        return view('cardgame/my-account', compact('menu'));

    }
    
    public function setMyAccount(Request $request) {
        
        if($request->password != null && $request->password_confirm != null) {

            $password = $request->password;
            $passwordConfirm = $request->password_confirm;
        
            $user = Sentinel::getUser();
        
            if($password != $passwordConfirm) {
                Alert::error(\Lang::get('general.passwords_you_entered_dont_match'));
                return Redirect::back();
            }
        
            Sentinel::update($user, array('password' => $password));
        }
        
        $user = User::where('id', Sentinel::getUser()->id)->first();
        $user->email_2 = $request->email_2;
        $user->lang = $request->lang;
        $user->save();
        
        \App::setLocale(Sentinel::getUser()->lang);
        
        Alert::success(\Lang::get('general.your_informations_saved'));
        return Redirect::back();
        
    }
    

}