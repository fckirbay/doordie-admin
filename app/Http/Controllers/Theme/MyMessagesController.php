<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Redirect;
use Sentinel;
use Illuminate\Http\Request;
use App\User;
use App\Support;
use Alert;
use Carbon;

class MyMessagesController extends Controller {

    public function getMyMessages() {

    	$messages = Support::where('user_id', Sentinel::getUser()->id)->where('validity_date', null)->orwhere('user_id', Sentinel::getUser()->id)->where('validity_date', '>=', Carbon\Carbon::now())->orderBy('id', 'desc')->limit(30)->get();

    	$checkUnreaded = Support::where('user_id', Sentinel::getUser()->id)->where('owner', 2)->where('is_viewed', 0)->update(['is_viewed' => 1]);
        
        return view('mobile/my-messages', compact('messages'));

    }

}