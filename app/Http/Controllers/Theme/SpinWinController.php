<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use App\ScratchWin;
use App\User;
use Carbon;
use Sentinel;

class SpinWinController extends Controller {

    public function getSpinWin() {

        return view('mobile/spin-win');

    }
    
    public function setClickScratch(Request $request) {

        $data = [];

        $from = Carbon\Carbon::now()->subHours(24);
        $to = Carbon\Carbon::now();

        $count = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', 2)->whereBetween('complete_date', array($from, $to))->count();

        if(session('extra', 0) != 0) {
            $total_scratch = session('extra');
        } else {
            $total_scratch = 1;
        }

        if($count < $total_scratch) {
            $token = bin2hex(random_bytes(16));        
        
            $newClick = new ScratchWin();
            $newClick->user_id = Sentinel::getUser()->id;
            $newClick->token = $token;
            $newClick->is_completed = 0;
            $newClick->click_date = Carbon\Carbon::now();
            $newClick->save();

            session(['scratch_token' => $token]);

            $data['result'] = "success";
            $data['token'] = $token;
            
        } else {

            $data['result'] = "You can watch ". $total_scratch ." ads for to scratch in a day!";
            $data['token'] = "";
        }

        return $data;
        

    }


    public function setCheckWatch(Request $request) {

        $data = [];

        $from = Carbon\Carbon::now()->subHours(24);
        $to = Carbon\Carbon::now();

        $token = session('scratch_token');

        $check = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', 1)->where('token', $token)->whereBetween('click_date', array($from, $to))->count();

        if($check == 0) {
        	$data['result'] = "error";
        } else {
        	$data['result'] = "success";
        }

        return $data;
        
    }

    public function setStartScratch(Request $request) {

        $data = [];

        $from = Carbon\Carbon::now()->subHours(24);
        $to = Carbon\Carbon::now();

        $token = session('scratch_token');

        $check = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', 1)->where('token', $token)->whereBetween('click_date', array($from, $to))->first();
        

        if($check) {
        	$check->is_completed = 2;
        	$check->save();

        	$data['result'] = "success";
        } else {
        	$data['result'] = "error";
        }

        return $data;
        
    }

    public function setCompleteScratch(Request $request) {

        $data = [];

        $from = Carbon\Carbon::now()->subHours(24);
        $to = Carbon\Carbon::now();

        $token = session('scratch_token');

        $check = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', 2)->where('token', $token)->where('prize', null)->whereBetween('click_date', array($from, $to))->first();
        
        if($check) {

        	$check->prize = $request->prize;
	        $check->complete_date = Carbon\Carbon::now();
	        $check->save();

	        $inc = User::where('id', Sentinel::getUser()->id)->increment('ticket', $request->prize);

        	$data['result'] = "success";
        } else {
        	$data['result'] = "error";
        }

        return $data;
        
    }

}