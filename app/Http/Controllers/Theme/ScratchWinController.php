<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use App\ScratchWin;
use App\User;
use App\Notifications;
use Carbon;
use Sentinel;

class ScratchWinController extends Controller {

    public function getScratchWin() {

        if(Sentinel::check() && Sentinel::getUser()->app_version < config('app.application_version')) {
            return redirect('update');
        }

    	$from = Carbon\Carbon::now()->subHours(24);
    	$to = Carbon\Carbon::now();

    	$videoCompleted = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', 1)->whereBetween('complete_date', array($from, $to))->first();
        $scratchCompleted = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', 2)->whereBetween('complete_date', array($from, $to))->first();

        if($scratchCompleted == null && $videoCompleted != null && session('scratch_prize', 0) == 0) {
            session(['scratch_prize' => $videoCompleted->prize]);
            session(['scratch_token' => $videoCompleted->token]);
        }

        if($scratchCompleted != null) {
            $date = $scratchCompleted->complete_date;
        } else {
            $date = null;
        }
        
        return view('mobile/scratch-win', compact('videoCompleted', 'scratchCompleted', 'date'));

    }
    
    public function setClickScratch(Request $request) {

        $data = [];

        $from = Carbon\Carbon::now()->subHours(24);
        $to = Carbon\Carbon::now();

        $count = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', '>', 0)->whereBetween('complete_date', array($from, $to))->count();


        if($count < 1) {
            $token = bin2hex(random_bytes(16));

            $probability = rand(0, 100);

            if(session('premium', 'unknown') == "unknown") {
                if($probability < 50) {
                    $prize = 0.25;
                } else {
                    $prize = 0.5;
                }
            } elseif(session('premium', 'unknown') == "bronze_tr" || session('premium', 'unknown') == "bronze_en") {
                if($probability < 50) {
                    $prize = 4;
                } elseif($probability < 95) {
                    $prize = 5;
                } else {
                    $prize = 6;
                }
            } elseif(session('premium', 'unknown') == "silver_tr" || session('premium', 'unknown') == "silver_en") {
                if($probability < 50) {
                    $prize = 8;
                } elseif($probability < 95) {
                    $prize = 10;
                } else {
                    $prize = 12;
                }
            } elseif(session('premium', 'unknown') == "gold_tr" || session('premium', 'unknown') == "gold_en") {
                if($probability < 50) {
                    $prize = 17;
                } elseif($probability < 95) {
                    $prize = 20;
                } else {
                    $prize = 23;
                }
            } elseif(session('premium', 'unknown') == "platinum_tr" || session('premium', 'unknown') == "platinum_en") {
                if($probability < 50) {
                    $prize = 28;
                } else {
                    $prize = 30;
                }
            } elseif(session('premium', 'unknown') == "vip_en") {
                if($probability < 50) {
                    $prize = 45;
                } else {
                    $prize = 50;
                }
            }
        
            $newClick = new ScratchWin();
            $newClick->user_id = Sentinel::getUser()->id;
            $newClick->token = $token;
            if(session('premium', 'unknown') == "unknown") {
                $newClick->is_completed = 0;
            } else {
                $newClick->is_completed = 1;
                $newClick->complete_date = Carbon\Carbon::now();
            }
            $newClick->prize = $prize;
            $newClick->click_date = Carbon\Carbon::now();
            $newClick->save();

            session(['scratch_token' => $token]);
            session(['scratch_prize' => $prize]);

            $data['result'] = "success";
            $data['token'] = $token;
            
        } else {

            $data['result'] = "You can scratch 1 card in a day!";
            $data['token'] = "";
        }

        return $data;
        

    }


    public function setCheckWatch(Request $request) {
        
        $data = [];

        if(Sentinel::check()) {
            $from = Carbon\Carbon::now()->subHours(24);
            $to = Carbon\Carbon::now();

            $token = session('scratch_token');

            $check = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', 1)->where('token', $token)->whereBetween('click_date', array($from, $to))->first();

            if($check == null) {
                $data['result'] = "error";
            } else {
                $data['result'] = "success";
                
            }
        } else {
            $data['result'] = "error";
        }

        return $data;
        
    }

    public function setStartScratch(Request $request) {

        $data = [];

        $from = Carbon\Carbon::now()->subHours(24);
        $to = Carbon\Carbon::now();

        $token = session('scratch_token');

        $check = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', 1)->where('token', $token)->whereBetween('click_date', array($from, $to))->first();
        

        if($check) {
        	$check->is_completed = 2;
        	$check->save();

        	$data['result'] = "success";
        } else {
        	$data['result'] = "error";
        }

        return $data;
        
    }

    public function setCompleteScratch(Request $request) {

        $data = [];

        $from = Carbon\Carbon::now()->subHours(24);
        $to = Carbon\Carbon::now();

        $token = session('scratch_token');

        $todayControl = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', 2)->whereBetween('click_date', array($from, $to))->count();

        if($todayControl == 0) {
            //Tokeni kaldırdık
            $check = ScratchWin::where('user_id', Sentinel::getUser()->id)->where('is_completed', 1)->whereBetween('click_date', array($from, $to))->orderBy('id', 'desc')->first();
        
            if($check) {
                $check->is_completed = 2;
                $check->complete_date = Carbon\Carbon::now();
                $check->save();

                $inc = User::where('id', Sentinel::getUser()->id)->increment('ticket', $check->prize);

                $data['result'] = "success";
                if($check->prize == 0) {
                    $data['message_title'] = \Lang::get('general.sorry');
                    $data['message'] = \Lang::get('general.you_have_not_won_ticket');
                } else {
                    $data['message_title'] = \Lang::get('general.congratulations');
                    $data['message'] = \Lang::get('general.you_won_ticket');
                }

                $request->session()->forget('scratch_token');
                $request->session()->forget('scratch_prize');

                $user = User::select('firebase', 'is_notify')->where('id', Sentinel::getUser()->id)->first();
                if($user->is_notify == 1 && $user->firebase != null) {
                    $newNotify = new Notifications();
                    $newNotify->user_id = Sentinel::getUser()->id;
                    $newNotify->title = "LuckyBox News";
                    $newNotify->notification = \Lang::get('general.your_scratch_card_is_ready');
                    //"Ödüllü videonuz hazır! Hemen izleyerek hediye biletlerinizi alın.";
                    $newNotify->firebase = $user->firebase;
                    $newNotify->time = Carbon\Carbon::now()->addHours(24);
                    $newNotify->status = 0;
                    $newNotify->created_at = Carbon\Carbon::now();
                    $newNotify->save();
                }
                
            } else {
                $data['result'] = "error";
            }
        } else {
            $data['result'] = "error";
        }
        
        return $data;
        
    }

}