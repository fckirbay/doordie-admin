<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use App\Transactions;

class PremiumMembershipController extends Controller {

    public function getPremiumMembership() {

    	$totalTrans = Transactions::where('user_id', Sentinel::getUser()->id)->where('type', 2)->where('status', 1)->sum('amount');
    	$withdraws = Transactions::where('user_id', Sentinel::getUser()->id)->where('type', 1)->where('status', 1)->sum('amount');

        return view('mobile/premium-membership', compact('totalTrans', 'withdraws'));

    }

    public function getAdvantages($membership) {

    	if(Sentinel::getUser()->currency == "try") {
    		if($membership == "bronze") {
	    		$price = "5,00 ₺";
	    	} elseif($membership == "silver") {
	    		$price = "10,00 ₺";
	    	} elseif($membership == "gold") {
	    		$price = "20,00 ₺";
	    	} elseif($membership == "platinum") {
	    		$price = "50,00 ₺";
	    	} elseif($membership == "vip") {
	    		$price = "100,00 ₺";
	    	}
    	} else {
    		if($membership == "bronze") {
	    		$price = "€5.00";
	    	} elseif($membership == "silver") {
	    		$price = "€10.00";
	    	} elseif($membership == "gold") {
	    		$price = "€20.00";
	    	} elseif($membership == "platinum") {
	    		$price = "€50.00";
	    	} elseif($membership == "vip") {
	    		$price = "€100.00";
	    	} 
    	}
    	

        return view('mobile/premium-membership-advantages', compact('membership', 'price'));

    }
    
}