<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;

class WithdrawMoneyController extends Controller {

    public function getWithdrawMoney() {
                
        return view('mobile/withdraw-money');

    }
    
}