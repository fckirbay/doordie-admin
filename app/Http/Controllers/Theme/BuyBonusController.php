<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;

class BuyBonusController extends Controller {

    public function getBuyBonus() {
                
        return view('cardgame/buy-bonus');

    }
    
}