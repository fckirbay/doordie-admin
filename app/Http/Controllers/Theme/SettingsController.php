<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use App\Blog;

class SettingsController extends Controller {

    public function getSettings() {
                
        return view('cardgame/settings');

    }
    
}