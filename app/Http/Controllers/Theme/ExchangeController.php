<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use App\Exchanges;
use App\User;
use Carbon;
use Sentinel;
use Alert;

class ExchangeController extends Controller {

    public function getExchange() {

        return view('mobile/exchange');

    }
    
    public function setExchange(Request $request) {
        
        if(Sentinel::getUser()->balance < $request->amount) {
        	Alert::error(\Lang::get('general.balance_is_insufficient_for_this_exchange'));
            return Redirect::back();
        }

        $from = Carbon\Carbon::now()->subHours(24);
        $to = Carbon\Carbon::now();
        $check = Exchanges::where('user_id', Sentinel::getUser()->id)->whereBetween('date', array($from, $to))->count();

        if($check > 0) {
        	Alert::error(\Lang::get('general.exchange_can_be_done_once_in_24_hours'));
            return Redirect::back();
        }

        $newExchange = new Exchanges();
        $newExchange->user_id = Sentinel::getUser()->id;
        $newExchange->amount = $request->amount;
        $newExchange->date = Carbon\Carbon::now();
        $newExchange->save();
        
        $decBalance = User::where('id', Sentinel::getUser()->id)->decrement('balance', $request->amount);
        $incTickets = User::where('id', Sentinel::getUser()->id)->increment('ticket', $request->amount);

        Alert::success(\Lang::get('general.exchange_successful'));
        return Redirect::back();


    }
    

}