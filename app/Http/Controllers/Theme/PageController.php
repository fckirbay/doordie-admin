<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use App\Products;
use App\ProductPhotos;
use App\ProductBrands;
use App\Pages;
use App\Blog;
use DB;

class PageController extends Controller {

    public function getPage($slug) {
        
        $menu = (new FunctionsController)->getMenu();
        
        $product = Products::where('slug', $slug)->first();
        
        if($product) {
            
            $month_deals = (new FunctionsController)->getMonthDeals();
            
            $photos = ProductPhotos::where('product_id', $product->id)->orderBy('is_featured', 'desc')->get();
            
            $comments = DB::table('product_comments')
                ->leftjoin('users', 'product_comments.user_id', '=', 'users.id')
                ->select('product_comments.*', 'users.first_name')
                ->where('product_comments.status', 1)
                ->where('product_comments.product_id', $product->id)
                ->orderBy('product_comments.id', 'desc')
                ->limit(20)
                ->get();
            
            $rate = 0;
            $rating = 0;
            if(count($comments) > 0) {
                foreach($comments as $key => $val) {
                    $rating = $rating + $val->quality + $val->price + $val->shipping;
                }
                $rate = $rating / 3 / count($comments);
            }
            
            $similars = DB::table('products')
                ->leftjoin('product_photos', 'product_photos.product_id', '=', 'products.id')
                ->select('products.*', 'product_photos.photo')
                ->where('product_photos.is_featured', 1)
                ->where('products.category_id', $product->category_id)
                ->orderBy('products.id', 'asc')
                ->limit(6)
                ->get();
            
            $brand = ProductBrands::where('id', $product->brand_id)->first();
            
            $agent = new Agent();
            if($agent->isMobile()) {
                return view('mobile/product-detail', compact('menu', 'month_deals', 'product', 'photos', 'comments', 'similars', 'rate', 'brand'));
            } else {
                return view('theme/product-detail', compact('menu', 'month_deals', 'product', 'photos', 'comments', 'similars', 'rate', 'brand'));
            }
            
        } else {
            
            $page = Pages::where('slug', $slug)->first();
            
            if($page == null) {
                $page = Blog::where('slug', $slug)->first();
            }
            
            
            if($page == null) {
                return view('errors/404', compact('menu'));
            } else {
                return view('theme/page', compact('menu', 'page'));
            }
            
            
        }
        
    }
    
}