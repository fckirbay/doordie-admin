<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Carbon;
use DB;
use App\ProductCategories;
use App\Products;

class FunctionsController extends Controller {
    
    public function getMenu() {
        
        
        
    }
    
    public function getMonthDeals() {
        
        $month_deals = DB::table('products')
            ->leftjoin('product_photos', 'product_photos.product_id', '=', 'products.id')
            ->select('products.*', 'product_photos.photo')
            ->where('product_photos.is_featured', 1)
            ->limit(5)
            ->get();
        
        return $month_deals;
        
    }

}

