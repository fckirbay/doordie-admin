<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Carbon;
use App\PaymentMethods;

class PrizeListController extends Controller {

    public function getPrizeList() {

    	$methods = PaymentMethods::orderBy('id', 'asc')->get();
                
        return view('mobile/prize-list', compact('methods'));

    }
    
}