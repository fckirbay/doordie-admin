<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use App\Games;
use App\GameLaps;
use App\UserJokers;

class PlayController extends Controller {

    public function getPlay() {

    	$game = Games::where('user_id', Sentinel::getUser()->id)->where('is_completed', 0)->first();

    	if($game == null) {
    		$game = new Games();
    		$game->user_id = Sentinel::getUser()->id;
    		$game->lap = 1;
    		$game->coins = 0;
    		$game->save();

    		$cardType = rand(1,4);
    		if($cardType == 1) {
    			$type = "C";
    		} elseif($cardType == 2) {
    			$type = "D";
    		} elseif($cardType == 3) {
    			$type = "H";
    		} else {
    			$type = "S";
    		}

    		$lap = new GameLaps();
    		$lap->game_id = $game->id;
    		$lap->card_1 = rand(2,15).$type;
    		$lap->save();
    	} else {
    		$lap = GameLaps::where('game_id', $game->id)->where('result', 0)->first();
    	}

    	$jokers = UserJokers::where('user_id', Sentinel::getUser()->id)->first();
                
        return view('cardgame/play', compact('game', 'lap', 'jokers'));

    }

    public function setPlayPass(Request $request) {

    	$jokers = UserJokers::where('user_id', Sentinel::getUser()->id)->first();

    	if($jokers->pass > 0) {
    		$game = Games::where('user_id', Sentinel::getUser()->id)->where('is_completed', 0)->first();

	    	$lap = GameLaps::where('game_id', $game->id)->where('result', 0)->first();

	    	if($lap) {

	    		$decPass = UserJokers::where('user_id', Sentinel::getUser()->id)->decrement('pass', 1);

	    		$cardType = rand(1,4);
	    		if($cardType == 1) {
	    			$type = "C";
	    		} elseif($cardType == 2) {
	    			$type = "D";
	    		} elseif($cardType == 3) {
	    			$type = "H";
	    		} else {
	    			$type = "S";
	    		}

	    		$lap->card_1 = rand(2,15).$type;
	    		$lap->card_2 = null;
	    		$lap->save();

	    		return redirect('/play');

	    	} else {
	    		return Redirect::back()->withErrors(['err', 'Geçerli oyun bulunmamaktadir.']);
	    	}
    	} else {
    		return Redirect::back()->withErrors(['err', 'Jokeriniz bulunmamaktadır.']);
    	}

    }

    public function setPlayShowResult(Request $request) {

    	$jokers = UserJokers::where('user_id', Sentinel::getUser()->id)->first();

    	if($jokers->show_result > 0) {
    		$game = Games::where('user_id', Sentinel::getUser()->id)->where('is_completed', 0)->first();

	    	$lap = GameLaps::where('game_id', $game->id)->where('result', 0)->first();

	    	if($lap) {

	    		$decPass = UserJokers::where('user_id', Sentinel::getUser()->id)->decrement('show_result', 1);

	    		$cardType = rand(1,4);
	    		if($cardType == 1) {
	    			$type = "C";
	    		} elseif($cardType == 2) {
	    			$type = "D";
	    		} elseif($cardType == 3) {
	    			$type = "H";
	    		} else {
	    			$type = "S";
	    		}

	    		$lap->card_2 = rand(2,15).$type;
	    		$lap->save();

	    		return redirect('play');

	    	} else {
	    		return Redirect::back()->withErrors(['err', 'Geçerli oyun bulunmamaktadir.']);
	    	}
    	} else {
    		return Redirect::back()->withErrors(['err', 'Jokeriniz bulunmamaktadır.']);
    	}

    }

}