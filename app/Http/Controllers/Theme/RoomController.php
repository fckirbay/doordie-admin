<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Sentinel;
use Alert;
use Carbon;
use Validator;
use Reminder;
use App\User;
use App\Boxes;
use App\Prizes;
use App\Clicks;
use App\Rooms;
use App\Transactions;
use App\Notifications;

class RoomController extends Controller {

    public function getRoom($city, Request $request) {

        if(Sentinel::check() && Sentinel::getUser()->app_version < config('app.application_version')) {
            return redirect('update');
        }
        
        $lang = session('lang');
        
        $cityInfo = Rooms::where('slug', $city)->first();
        
        if(isset($_GET['show']) && $_GET['show'] == 1) {
            $boxes = Boxes::where('rooms', 'NOT LIKE', '%'.$cityInfo->slug.'%')->orWhere('rooms', null)->paginate(99);
        } else {
            if(isset($_GET['number'])) {
                $boxes = Boxes::where('id', $_GET['number'])->paginate(10);
            } else {
                $boxes = Boxes::paginate(99);
            }
        }
        
        return view('mobile/room', compact('city', 'boxes', 'cityInfo', 'lang'));

    }
    
    public function getFeelLucky(Request $request) {
        
        if(session('lang') == "tr") {
            $city = Rooms::inRandomOrder()->first();
        } else {
            $city = Rooms::whereIn('location', ['eu', 'wo'])->inRandomOrder()->first();
        }
        
        return redirect('room/'.$city->slug.'?show=1');
        
    }
    
    public function setOpenBox(Request $request) {
        
        /* Tweet gönderme
        
        $uploaded_media = \Twitter::uploadMedia(['media' => 'https://media.giphy.com/media/7rj2ZgttvgomY/giphy.gif']);

		if($image == null) {
			\Twitter::postTweet(['status' => $description . ' ' . $tags, 'format' => 'json']);
		} else {
			\Twitter::postTweet(['status' => $description . ' ' . $tags, 'media_ids' => $uploaded_media->media_id_string]);
		}
		
		*/
        
        if(Sentinel::getUser()->ticket < 1) {
            $data['result'] = 0;
            $data['status'] = \Lang::get('general.no_tickets');
            $data['status_2'] = '';
            
            return $data;
        } else {
            $box = Boxes::where('id', $request->number)->first();
        
            $json = json_decode($box->rooms);
            
            if(!isset($json->{$request->city})) {
                
                
                
                $dec_ticket = User::where('id', Sentinel::getUser()->id)->decrement('ticket', 1);
                $click = User::where('id', Sentinel::getUser()->id)->increment('clicks', 1);
                
                
                // ÖDÜLÜN BELİRLENDİĞİ ALAN
                $userBalance = Sentinel::getUser()->balance;
                
                $deposit = Transactions::where('user_id', Sentinel::getUser()->id)->where('type', 2)->where('status', 1)->sum('amount');
                $withdraw = Transactions::where('user_id', Sentinel::getUser()->id)->where('type', 1)->where('status', 1)->sum('amount');
                
                $probability = rand(0, 100);

                if($userBalance % 50 < 10) {
                    if($probability < 10) {
                        $prize = 0;
                    } elseif($probability < 20) {
                        $prize = 0.01;
                    } elseif($probability < 30) {
                        $prize = 0.05;
                    } elseif($probability < 40) {
                        $prize = 0.10;
                    } elseif($probability < 60) {
                        $prize = 0.25;
                    } elseif($probability < 80) {
                        $prize = 0.50;
                    } elseif($probability < 95) {
                        $prize = 1;
                    } else {
                        $prize = 2;
                    }
                } elseif($userBalance % 50 < 20) {
                    if($probability < 10) {
                        $prize = 0;
                    } elseif($probability < 20) {
                        $prize = 0.01;
                    } elseif($probability < 30) {
                        $prize = 0.05;
                    } elseif($probability < 45) {
                        $prize = 0.10;
                    }  elseif($probability < 60) {
                        $prize = 0.25;
                    } elseif($probability < 80) {
                        $prize = 0.50;
                    } else {
                        $prize = 1;
                    }
                } elseif($userBalance % 50 < 30) {
                    if($probability < 20) {
                        $prize = 0;
                    } elseif($probability < 30) {
                        $prize = 0.01;
                    } elseif($probability < 40) {
                        $prize = 0.05;
                    } elseif($probability < 60) {
                        $prize = 0.10;
                    } elseif($probability < 80) {
                        $prize = 0.25;
                    } else {
                        $prize = 0.5;
                    }
                } elseif($userBalance % 50 < 40) {
                    if($probability < 30) {
                        $prize = 0;
                    } elseif($probability < 45) {
                        $prize = 0.01;
                    } elseif($probability < 65) {
                        $prize = 0.05;
                    } elseif($probability < 85) {
                        $prize = 0.10;
                    } else {
                        $prize = 0.25;
                    }
                } elseif($userBalance % 50 < 45) {
                    if($probability < 30) {
                        $prize = 0;
                    } elseif($probability < 65) {
                        $prize = 0.01;
                    } elseif($probability < 85) {
                        $prize = 0.05;
                    } else {
                        $prize = 0.10;
                    }
                } elseif($userBalance % 50 < 49) {
                    if($probability < 50) {
                        $prize = 0;
                    } elseif($probability < 65) {
                        $prize = 0.01;
                    } else {
                        $prize = 0.05;
                    }
                } else {
                    $prize = 0;
                }

                if(Sentinel::getUser()->clicks <= 11 && $userBalance > 14) {
                    $prize = 0;
                }

                if(Sentinel::getUser()->id == 37742 && $userBalance < 88) { 
                    if($probability < 10) {
                        $prize = 0;
                    } elseif($probability < 17) {
                        $prize = 0.01;
                    } elseif($probability < 27) {
                        $prize = 0.5;
                    } elseif($probability < 37) {
                        $prize = 0.10;
                    } elseif($probability < 58) {
                        $prize = 0.25;
                    } elseif($probability < 78) {
                        $prize = 0.50;
                    } elseif($probability < 98) {
                        $prize = 1;
                    } else {
                        $prize = 5;
                    }
                } elseif(Sentinel::getUser()->is_winner == 1 && $userBalance < 50) {
                    if($probability < 33) {
                        $prize = 0.25;
                    } elseif($probability < 66) {
                        $prize = 0.50;
                    } else {
                        $prize = 1;
                    }
                } elseif(Sentinel::getUser()->is_winner == 1 && $userBalance >= 60) {
                    if($probability < 33) {
                        $prize = 0;
                    } elseif($probability < 66) {
                        $prize = 0.01;
                    } else {
                        $prize = 0.05;
                    }
                }
                
    

                /*
                if(Sentinel::getUser()->ticket == 1 && Sentinel::getUser()->firebase != null) {
                    $sendNotify = new Notifications();
                    $sendNotify->user_id = Sentinel::getUser()->id;
                    $sendNotify->title = "LuckyBox";
                    if(Sentinel::getUser()->lang == "en" || Sentinel::getUser()->lang == null) {
                        $sendNotify->notification = "Want more tickets to continue winning? Our live support is online now!";
                    } else {
                        $sendNotify->notification = "Kazanmaya devam etmek için daha fazla bilet ister misiniz? Canlı desteğimiz şuan çevrimiçi!";
                    }
                    $sendNotify->firebase = Sentinel::getUser()->firebase;
                    $sendNotify->time = Carbon\Carbon::now()->addMinutes(2);
                    $sendNotify->save();
                }
                */
                
                if($prize == 0) {
                    $data['status'] = \Lang::get('general.box_is_empty');
                    $data['status_2'] = '';
                    
                    $lost = User::where('id', Sentinel::getUser()->id)->increment('lost', 1);
                    
                    if($box->rooms != null) {
                        $json_decode = json_decode($box->rooms);
                        $json_decode->{$request->city} = 2;
                    } else {
                        $json_decode[$request->city] = 2;
                    }
                    
                    $update_rooms = Boxes::where('id', $request->number)->update(['rooms' => json_encode($json_decode)]);
                    $data['result'] = 3;
                } else {
                    if(session('coefficient', 1) == 1) {
                        $data['status'] = \Lang::get('general.congratulations_you_won', ['prize' => $prize, 'currency' => 'LP']);
                    } else {
                        $data['status'] = \Lang::get('general.congratulations_you_won', ['prize' => $prize .' (x'.session('coefficient') .')', 'currency' => 'LP']);
                    }
                    $data['status_2'] = \Lang::get('general.good_luck_in_your_next_election');
                    
                    $newPrize = new Prizes();
                    $newPrize->user_id = Sentinel::getUser()->id;
                    $newPrize->prize = $prize * session('coefficient', 1);
                    $newPrize->created_at = Carbon\Carbon::now();
                    $newPrize->save();
                    
                    $won = User::where('id', Sentinel::getUser()->id)->increment('won', 1);
                    $earnings = User::where('id', Sentinel::getUser()->id)->increment('earnings', $prize * session('coefficient', 1));
                    $balance = User::where('id', Sentinel::getUser()->id)->increment('balance', $prize * session('coefficient', 1));
                    
                    if($box->rooms != null) {
                        $json_decode = json_decode($box->rooms);
                        $json_decode->{$request->city} = 1;
                    } else {
                        $json_decode[$request->city] = 1;
                    }
                    
                    $update_rooms = Boxes::where('id', $request->number)->update(['rooms' => json_encode($json_decode)]);
                    
                    $data['result'] = 1;
                    
                    // Tweet gönderiliyor...
                    $name = Sentinel::getUser()->first_name;
                    $expl = explode(' ', $name);
                    $full_name = $expl[0].' '.strtoupper(substr($expl[count($expl)-1], 0, 1)). '.';
                    //\Twitter::postTweet(['status' => 'Tebrikler ' . $full_name . ' '. $prize . ' ₺ kazandınız!', 'format' => 'json']);
                    
                    /*
                    $uploaded_media = \Twitter::uploadMedia(['media' => \File::get(public_path('assets/mobile/images/giphy.gif'))]);
                    \Twitter::postTweet(['status' => 'Tebrikler ' . $full_name . ' '. $prize . ' ₺ kazandınız!', 'media_ids' => $uploaded_media->media_id_string]);
                    
                    if(Sentinel::getUser()->currency == "eur") {
                        \Twitter::postTweet(['status' => 'Congratulations ' . $name . '! You won €'. $prize . '! ('. Carbon\Carbon::now()->format('H:i:s') .')', 'format' => 'json']);
                    } else {
                        \Twitter::postTweet(['status' => 'Congratulations ' . $name . '! You won '. $prize . ' ₺! ('. Carbon\Carbon::now()->format('H:i:s') .')', 'format' => 'json']);
                    }
*/
                    
                    
                }
                
                $findCityId = Rooms::where('slug', $request->city)->first();
                
                $newClick = new Clicks();
                $newClick->user_id = Sentinel::getUser()->id;
                $newClick->city = $findCityId->id;
                $newClick->number = $request->number;
                $newClick->prize = $prize * session('coefficient', 1);
                $newClick->created_at = Carbon\Carbon::now();
                $newClick->save();
                
                $ticket = User::select('ticket')->where('id', Sentinel::getUser()->id)->first();
                
                $data['ticket'] = $ticket->ticket;
                
            } else {
                $data['result'] = 2;
                $data['status'] = \Lang::get('general.this_box_has_already_opened');
                $data['status_2'] = \Lang::get('general.please_try_another_box');
            }
            
            return $data;
        }
        
    }
    
}