<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Sentinel;
use Alert;
use Validator;
use App\User;
use App\Clicks;
use DB;

class MyReferencesController extends Controller {

    public function getMyReferences() {
        
        $references = User::where('reference_id', Sentinel::getUser()->id)->where('verification', 1)->orderBy('id', 'desc')->get();
        
        return view('cardgame/my-references', compact('references'));

    }
    
}