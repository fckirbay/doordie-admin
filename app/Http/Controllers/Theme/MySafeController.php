<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use Redirect;
use Sentinel;
use Illuminate\Http\Request;
use App\User;
use Alert;

class MySafeController extends Controller {

    public function getMySafe() {
        
        return view('mobile/my-safe');

    }

}