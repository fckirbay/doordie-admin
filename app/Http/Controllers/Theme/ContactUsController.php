<?php

namespace App\Http\Controllers\Theme;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Theme\FunctionsController;
use Redirect;
use Illuminate\Http\Request;
use Validator;
use Mail;
use Alert;

class ContactUsController extends Controller {

    public function getContactUs() {
        
        $menu = (new FunctionsController)->getMenu();
        
        return view('theme/contact-us', compact('menu'));

    }
    
    public function setContactUs(Request $request) {
        
        
        $validator = Validator::make($request->all(), [
            'name_surname' => 'required|max:150',
            'email' => 'required|email|max:255',
            'phone' => 'max:15',
            'subject' => 'max:200',
            'message' => 'max:1500',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        $name = $request->name_surname;
        $email = $request->email;
        $phone = $request->phone;
        $subject = $request->subject;
        $messageText = $request->message;
        
        Mail::send('emails.contact-us', ['name' => $name, 'email' => $email, 'phone' => $phone, 'subject' => $subject, 'messageText' => $messageText], function ($m) use ($name, $email, $phone, $subject, $messageText) {
            $m->from($email, $name);
            $m->to('info@yalisoft.com', 'ByDukkan')->subject('İletişim Formu');
        });
        
        Alert::success('Mesajınız gönderildi!');
        return Redirect::back();
        
    }
    

}