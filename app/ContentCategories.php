<?php

namespace App;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class ContentCategories extends Model
{
    use SoftDeletes;
    
    protected $table = 'content_categories';
    protected $dates = ['deleted_at'];
}
