<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Errors extends Model
{
    public $timestamps = false;
    protected $table = 'errors';
}
