<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchanges extends Model
{
    protected $table = 'exchanges';
    public $timestamps = false;
}
