<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserJokers extends Model
{
    public $timestamps = false;
    protected $table = 'user_jokers';
}
