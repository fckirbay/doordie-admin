@extends('layouts.mobile.main')
@section('styles')

@endsection
@section('content')

<div class="notification-large notification-has-icon notification-red notification-green bottom-0" style="text-align:center">
<div class="notification-icon"><i class="fa fa-exclamation-triangle notification-icon"></i></div>
<h1 class="uppercase ultrabold" style="font-size:14px">@lang('general.warning')!</h1>
<p>@lang('general.video_problem_description')</p>
<a href="#" class="close-notification"><i class="fa fa-times"></i></a>
</div>


<h3 class="uppercase bolder center-text" style="margin-top:50px">@lang('general.free') <span class="color-highlight">@lang('general.ticket')</span></h3>

<p class="center-text boxed-text-large bottom-30" style="margin-top:30px; font-size:14px">
@lang('general.watch_video_win_tickets')

<br /><br />

<!--
<strong>Video izlemeye çalışmadan önce uygulamanın son sürümünü kurduğunuzdan emin olun! Aksi takdirde videolar açılmayacaktır!</strong>
-->

@if(session('browser', 'unknown') == "android")
  <button class="uppercase ultrabold button-xs button button-blue" id="rewarded" onClick="showRewarded();" style="text-align:center"><i class="fa fa-shopping-cart color-white"></i> @lang('general.watch_video')</button>
@elseif(session('browser', 'unknown') == "ios")
  <button class="uppercase ultrabold button-xs button button-blue" id="rewarded" onClick="showInterstitial()" style="text-align:center"><i class="fa fa-shopping-cart color-white"></i> @lang('general.watch_video')(ios)</button>

  <a id="show_interstitial" class="btn btn-primary" onClick="showInterstitial()" >Show Ad</a>
  
@endif
<span id="rewarded-countdown" style="text-align:center; font-weight:bold"></span>



</p>

<div class="container content-padding" id="warning" style="display:none">
<div class="above-overlay">
<h4 class="uppercase ultrabold small-top color-white top-10" id="warning-text"></h4>
</div>
<div class="overlay bg-green2-dark opacity-60"></div>
</div>


@if(Sentinel::check() && session('browser') == "ios")
        <script>
          function showInterstitial() {
            window.bridge.post('show_interstitial', {})
        }
        </script>
        @endif

@endsection
@section('scripts')

  @if(Sentinel::check() )

            <script type="text/javascript">
              function showRewarded() {
                var user_id = "{{ Sentinel::getUser()->id }}";
                $("#rewarded").hide();
                $("#rewarded-countdown").show();
                
                    $.ajax({
                        url: 'click-rewarded',
                        method: 'POST',
                        data:{
                          type: 1,
                          _token: "{{csrf_token()}}"
                        },
                        dataType: 'json',
                        success: function(data) {

                          JSON.stringify(data); //to string
                          if(data.result === "success") {

                            android.showLoader();
                            android.showRewarded(user_id, data.token);
                            setTimeout(
                              function() 
                              {
                                  android.hideLoader();
                                  $('#rewarded').show();
                              }, 3000);

                            
                          } else {
                            $('#warning-text').html(data.result);
                            $('#warning').show();
                            //alert(data.result);
                          }
                          
                        }
                      });
                    
              }
            </script>
        @endif


@endsection