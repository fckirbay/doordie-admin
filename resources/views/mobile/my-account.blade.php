@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
            <div class="content" style="margin-top:10px">
               {!! Form::open(['url'=>'my-account', 'method'=>'post', 'class'=>'register-form outer-top-xs'])  !!}
                   <div class="select-box select-box-1">
                        <em>@lang('general.language')</em>
                        <select name="lang">
                            <option value="en" @if(Sentinel::getUser()->lang === "en") selected @endif>@lang('general.english')</option>
                            <option value="de" @if(Sentinel::getUser()->lang === "de") selected @endif>@lang('general.deutsch')</option>
                            <option value="in" @if(Sentinel::getUser()->lang === "in") selected @endif>@lang('general.hindi')</option>
                            <!--
                            <option value="in" @if(Sentinel::getUser()->lang === "in") selected @endif>@lang('general.french')</option>
                            <option value="fr" @if(Sentinel::getUser()->lang === "fr") selected @endif>@lang('general.french')</option>
                            <option value="it" @if(Sentinel::getUser()->lang === "it") selected @endif>@lang('general.italian')</option>
                            <option value="es" @if(Sentinel::getUser()->lang === "es") selected @endif>@lang('general.spanish')</option>
                            <option value="ru" @if(Sentinel::getUser()->lang === "ru") selected @endif>@lang('general.russian')</option>
                            <option value="zh" @if(Sentinel::getUser()->lang === "zh") selected @endif>@lang('general.chinese')</option>
                            -->
                            <option value="tr" @if(Sentinel::getUser()->lang === "tr") selected @endif>@lang('general.turkish')</option>
                        </select>
                   </div>
                   <div class="select-box select-box-1">
                        <em>@lang('general.rewarded_ad_notifications')</em>
                        <select name="is_notify">
                            <option value="1" @if(Sentinel::getUser()->is_notify == 1) selected @endif>@lang('general.yes')</option>
                            <option value="0" @if(Sentinel::getUser()->is_notify == 0) selected @endif>@lang('general.no')</option>
                        </select>
                   </div>
                   <div class="input-simple-1 has-icon input-red bottom-30"><em>@lang('general.email_address')</em><i class="fa fa-at"></i><input type="email" name="email_2" placeholder="@lang('general.email_address')" value="{{ Sentinel::getUser()->email_2 }}"></div>
                   <div class="input-simple-1 has-icon input-green bottom-30"><em>@lang('general.username')</em><i class="fa fa-user"></i><input type="text" name="name_surname" placeholder="Jonh Doe" value="{{ Sentinel::getUser()->first_name }}" disabled></div>
                   <div class="input-simple-1 has-icon input-red bottom-30"><em>@lang('general.mobile_phone')</em><i class="fa fa-mobile"></i><input type="text" name="phone" class="phone" placeholder="" minlength="13" value="{{ Sentinel::getUser()->phone }}" disabled></div>
                   
                   <div class="input-simple-1 has-icon input-red bottom-30"><em>@lang('general.password')</em><i class="fas fa-asterisk"></i><input type="password" name="password" class="phone" placeholder="@lang('general.password')" minlength="6" maxlength="15"></div>
                   <div class="input-simple-1 has-icon input-red bottom-30"><em>@lang('general.password_again')</em><i class="fas fa-asterisk"></i><input type="password" name="password_confirm" class="phone" placeholder="@lang('general.password_again')" minlength="6" maxlength="15"></div>
                   <button type="submit" class="button button-green" style="width:100%">@lang('general.save')</button>
               {!! Form::close() !!}
               <div class="clear"></div>
            </div>
@endsection
@section('scripts')
    
@endsection