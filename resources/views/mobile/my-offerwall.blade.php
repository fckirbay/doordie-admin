@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
    <table class="table-borders-dark">
        <tr>
            <th>@lang('general.offer_title')</th>
            <th>@lang('general.prize_ticket')</th>
            <th>@lang('general.complete_date')</th>
        </tr>
        @forelse($tasks as $key => $val)
        <tr>
            <td>{{ $val->offer_title }}</td>
            <td>{{ $val->point_value }}</td>
            <td>{{ Carbon\Carbon::parse($val->complete_date)->format('d M H:i') }}</td>
        </tr>
        @empty
        <tr>
            <td colspan ="4">@lang('general.you_have_no_completed_offer')</td>
        </tr>
        @endforelse
    </table>
@endsection
@section('scripts')
    
@endsection