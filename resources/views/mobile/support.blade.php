@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
    	<div class="page-content header-clear">
            <div class="content">
               <div class="container heading-style">
                  <h4 class="heading-title uppercase bolder">@lang('general.support')</h4>
                  <i class="fa fa-envelope heading-icon font-17"></i>
                  <div class="line bg-black"></div>
                  <p class="heading-subtitle">
                     @lang('general.you_can_tell_us_question')
                  </p>
               </div>
               <div class="container bottom-0">
                  <div class="contact-form">
                     <div class="formSuccessMessageWrap" id="formSuccessMessageWrap">
                        <div class="notification-large notification-has-icon notification-green">
                           <div class="notification-icon"><i class="fa fa-check notification-icon"></i></div>
                           <h1 class="uppercase ultrabold">@lang('general.you_can_tell_us_question')</h1>
                           <p>@lang('general.your_question_will_be_answered')</p>
                           <a href="#" class="close-notification"><i class="fa fa-times"></i></a>
                        </div>
                     </div>
                	{!! Form::open(['url'=>'support', 'method'=>'post', 'class'=>'contactForm', 'id'=>'contactForm'])  !!}
                        <fieldset>
                           <div class="formValidationError bg-red-dark" id="contactMessageTextareaError">
                              <p class="center-text uppercase small-text color-white">@lang('general.message_is_empty')</p>
                           </div>
                           <div class="formTextareaWrap">
                              <label class="field-title contactMessageTextarea" for="contactMessageTextarea">@lang('general.your_message'): <span>(required)</span>
                              </label>
                              <textarea name="message" class="contactTextarea requiredField" id="contactMessageTextarea"></textarea>
                           </div>
                           <div class="formSubmitButtonErrorsWrap contactFormButton">
                              <input type="submit" class="buttonWrap button bg-highlight button-sm button-rounded uppercase ultrabold contactSubmitButton top-30" id="contactSubmitButton" value="@lang('general.send')" data-formId="contactForm" />
                           </div>
                        </fieldset>
                	{!! Form::close() !!}
                  </div>
               </div>
               <div class="decoration"></div>
               <div class="contact-information last-column">
                  <div class="container no-bottom">
                     <h4 class="uppercase bolder bottom-15">@lang('general.other_contact_informations')</h4>
                     <p class="contact-information">
                     	@if(Sentinel::check() && Sentinel::getUser()->lang == "tr")
                        	<a href="mailto:name@domain.com"><i class="fa fa-envelope-square color-blue-dark"></i>sanslikutu2018@hotmail.com</a>
                        @else
                        	<a href="mailto:name@domain.com"><i class="fa fa-envelope-square color-blue-dark"></i>luckybox2018@gmail.com</a>
                        @endif
                        <a href="#"><i class="fab fa-twitter-square twitter-color"></i>@LuckyBoxFun</a>
                        <a href="#"><i class="fab fa-instagram instagram-color"></i>@LuckyBoxFun</a>
                     </p>
                  </div>
               </div>
            </div>
         </div>
@endsection
@section('scripts')

@endsection