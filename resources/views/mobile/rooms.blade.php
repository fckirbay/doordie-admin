@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
            <div class="content-full">
               <div class="decoration decoration-margins"></div>
               <div class="content">
                  @if($rooms[0]->location == "tr")
                     <h3 class="uppercase bolder center-text">@lang('general.turkey') <span class="color-highlight">@lang('general.rooms')</span></h3>
                  @elseif($rooms[0]->location == "eu")
                     <h3 class="uppercase bolder center-text">@lang('general.europe') <span class="color-highlight">@lang('general.rooms')</span></h3>
                  @else
                     <h3 class="uppercase bolder center-text">@lang('general.world') <span class="color-highlight">@lang('general.rooms')</span></h3>
                  @endif
               </div>
               
               <div class="blog-categories blog-categories-3 bottom-20">
                  @foreach($rooms as $val)
                     <a href="{{ url('room', $val->slug) }}?show=1"><strong></strong><em>@if($lang == "tr"){{ $val->name }}@else{{ $val->name_en }}@endif</em><span class="bg-red-dark opacity-50"></span><img src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset($val->photo) }}" class="preload-image responsive-image" alt="img"></a>
                  @endforeach
                  <div class="clear"></div>
               </div>
               
            </div>
            <div class="decoration decoration-margins"></div>
@endsection
@section('scripts')

@endsection