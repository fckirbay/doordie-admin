@extends('layouts.mobile.main')
@section('styles')

@endsection
@section('content')

<!--
<h3 class="uppercase bolder center-text" style="margin-top:50px">@lang('general.coming') <span class="color-highlight">@lang('general.soon')</span></h3>
-->

<h3 class="uppercase bolder center-text" style="margin-top:50px">Offer<span class="color-highlight">Wall</span></h3>

<p class="center-text boxed-text-large bottom-30" style="margin-top:30px; font-size:14px">
@lang('general.complete_tasks_win_ticket')
</p>

@if(Sentinel::check() && Sentinel::getUser()->lang == "tr")
<!--
<p class="center-text boxed-text-large bottom-30" style="margin-top:30px; font-size:14px">
  Görevler reklam sağlayıcısı tarafından belirlenir ve yönetilir. Görevleri eksiksiz tamamladığınızda otomatik olarak ödüllendirilirsiniz. Tamamladığınızı düşündüğünüz görev için ödüllendirilmediyseniz göreviniz eksiktir. Kontrol edip tekrar deneyin. Bu konuda atılan destek mesajlarına <u>kesinlikle</u> cevap verilmeyecektir.
</p>
-->
@endif

<p class="center-text boxed-text-large bottom-30" style="margin-top:30px; font-size:14px">

<button class="uppercase ultrabold button-xs button button-blue" id="rewarded" onClick="showOfferWall();" style="text-align:center; width:60%"><i class="fas fa-tasks color-white"></i> OfferWall</button>
</p>

<!--
<p class="center-text boxed-text-large bottom-30" style="margin-top:30px; font-size:10px">
<br />
<span style="font-size:12px; font-weight:bold">BİLGİLENDİRME</span>
<br/>
Eğer görevinizi tamamladığınıza eminseniz, OfferWall'u açarak en altta bulunan destek menüsüne gidiniz. Tamamladığınızdan emin olduğunuz görevi seçerek onunla ilgili destek bildirimi gönderebilirsiniz. Yaptığınız görevleri ya da tamamlayıp tamamlamadığınızı <u>KESİNLİKLE</u> göremiyoruz. Gerçekten eminseniz reklam sağlayıcısıyla destek için iletişim kurmanız gerekmektedir..
</p>
-->

@if(Sentinel::check() && Carbon\Carbon::now() < '2018-09-04 12:00:00')

<div class="content" style="margin-top:10px">
{!! Form::open(['url'=>'free-bonus', 'method'=>'post', 'class'=>'register-form outer-top-xs'])  !!}
                  <div class="input-simple-1 has-icon input-green bottom-30"><em>@lang('general.enter_code_to_win_ticket')</em><i class="fa fa-user"></i><input type="text" name="bonus_code" placeholder="@lang('general.bonus_code')"></div>

                   <button type="submit" class="button button-green" style="width:100%">@lang('general.get_bonus')</button>
               {!! Form::close() !!}
             </div>
@endif

@endsection
@section('scripts')
  @if(session('browser') == "android")
  <script>
    if("{{ session('ads', 'yes') }}" == "yes" && Math.random() >= 0.50) {
        android.showInterstitial();
    }
  </script>
  @endif
	@if(Sentinel::check() && session('browser') == "android")
    <script>
      function showOfferWall() {
      	android.showLoader();
        android.showOfferWall("{{ Sentinel::getUser()->id }}");
        setTimeout(
		  function() 
		  {
        	android.hideLoader();
		  }, 3000);
      }
    </script>
  @endif
@endsection