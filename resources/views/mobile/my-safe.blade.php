@extends('layouts.mobile.main')
@section('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endsection
@section('content')
            <div class="content" style="margin-top:20px">
               <h4 class="uppercase bolder" style="text-align:center">@lang('general.safe')</h4>
               <p style="text-align:center">
                  @lang('general.you_can_only_withdraw_50')
               </p>
               <div class="checkout-total">
                  <strong class="font-14 regularbold">@lang('general.remaining')</strong>
                  <span class="font-14">{{ Sentinel::getUser()->ticket }}</span>
                  <div class="clear"></div>
                  <strong class="font-14 regularbold">@lang('general.spent')</strong>
                  <span class="font-14">{{ Sentinel::getUser()->won + Sentinel::getUser()->lost }}</span>
                  <div class="clear"></div>
                  <strong class="font-14 regularbold">@lang('general.won')</strong>
                  <span class="font-14">{{ Sentinel::getUser()->won }}</span>
                  <div class="clear"></div>
                  <strong class="font-14 regularbold color-highlight">@lang('general.lost')</strong>
                  <span class="font-14 color-highlight">{{ Sentinel::getUser()->lost }}</span>
                  <!--
                  <div class="clear"></div>
                  <strong class="font-14 regularbold">@lang('general.total_earnings')</strong>
                  <span class="font-14">{{ number_format(Sentinel::getUser()->earnings, 2, ',', '.') }} LP</span>
                  -->
                  <div class="clear"></div>
                  <strong class="font-16 half-top">@lang('general.balance') </strong>
                  <span class="font-16 color-highlight ultrabold half-top">{{ number_format(Sentinel::getUser()->balance, 2, ',', '.') }} LP</span>
                  <div class="clear"></div>
               </div>

               <p style="text-align:center; font-weight:bold; font-size:18px">
                  1,00 LP = @if(Sentinel::getUser()->currency == "eur") €1.00 @endif @if(Sentinel::getUser()->currency == "try") 1,00 ₺ @endif
                  <!--@lang('general.your_withdrawal_requests_will_be_processed')-->
               </p>
               
               <a href="{{ url('prize-list') }}" class="button button-green button-full button-rounded button-sm uppercase ultrabold">@lang('general.get_your_prize')</a>
               
               
            </div>
@endsection
@section('scripts')
   
@endsection