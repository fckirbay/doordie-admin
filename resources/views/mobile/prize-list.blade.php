@extends('layouts.mobile.main')
@section('styles')
     <style>
          .preload-image {
               margin-top:-20px;
          }
     </style>
@endsection
@section('content')
          @if(Sentinel::getUser()->country == "Turkey (+90)")
          <div class="decoration bottom-0"></div>
            @forelse($methods as $key => $val)
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="{{ asset($val->logo) }}" data-src="{{ asset($val->logo) }}" alt="img">
               </a>
               <div class="store-slide-title" id="bronze">
                  <strong>{{ $val->title }}</strong>
                  <em class="color-gray-dark">{{ $val->description }}</em>
               </div>
               <div class="store-slide-button">
                  <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> {{ $val->prize }}</strong>
                     <button class="uppercase ultrabold button-xxs button button-blue withdraw-money" style="float:right"><i class="fa fa-shopping-cart color-white"></i> @lang('general.get_now')</button>
               </div>
            </div>
            <div class="decoration bottom-0"></div>
            @empty

            @endforelse
          @else
            <div class="decoration bottom-0"></div>
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="{{ asset('assets/mobile/images/prizes/paypal.jpg') }}" data-src="{{ asset('assets/mobile/images/prizes/paypal.jpg') }}" alt="img">
               </a>
               <div class="store-slide-title" id="bronze">
                  <strong>Paypal Gift</strong>
                  <em class="color-gray-dark">Minimum voucher is LP50.00</em>
               </div>
               <div class="store-slide-button">
                  <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> LP50.00</strong>
                     <button class="uppercase ultrabold button-xxs button button-blue withdraw-money" style="float:right"><i class="fa fa-shopping-cart color-white"></i> @lang('general.get_now')</button>
               </div>
            </div>
            <div class="decoration bottom-0"></div>
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="{{ asset('assets/mobile/images/prizes/westernunion.png') }}" data-src="{{ asset('assets/mobile/images/prizes/westernunion.png') }}" alt="img">
               </a>
               <div class="store-slide-title" id="bronze">
                  <strong>WesternUnion Gift</strong>
                  <em class="color-gray-dark">Minimum voucher is LP50.00</em>
               </div>
               <div class="store-slide-button">
                  <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> LP50.00</strong>
                     <button class="uppercase ultrabold button-xxs button button-blue withdraw-money" style="float:right"><i class="fa fa-shopping-cart color-white"></i> @lang('general.get_now')</button>
               </div>
            </div>
            <div class="decoration bottom-0"></div>
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="{{ asset('assets/mobile/images/prizes/ecopayz.png') }}" data-src="{{ asset('assets/mobile/images/prizes/ecopayz.png') }}" alt="img">
               </a>
               <div class="store-slide-title" id="bronze">
                  <strong>EcoPayz Gift</strong>
                  <em class="color-gray-dark">Minimum voucher is LP50.00</em>
               </div>
               <div class="store-slide-button">
                  <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> LP50.00</strong>
                     <button class="uppercase ultrabold button-xxs button button-blue withdraw-money" style="float:right"><i class="fa fa-shopping-cart color-white"></i> @lang('general.get_now')</button>
               </div>
            </div>
            <div class="decoration bottom-0"></div>
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="{{ asset('assets/mobile/images/prizes/aliexpress.png') }}" data-src="{{ asset('assets/mobile/images/prizes/aliexpress.png') }}" alt="img">
               </a>
               <div class="store-slide-title" id="bronze">
                  <strong>Aliexpress Gift Card</strong>
                  <em class="color-gray-dark">Minimum gift voucher is LP50.00</em>
               </div>
               <div class="store-slide-button">
                  <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> LP50.00</strong>
                     <button class="uppercase ultrabold button-xxs button button-blue withdraw-money" style="float:right"><i class="fa fa-shopping-cart color-white"></i> @lang('general.get_now')</button>
               </div>
            </div>
            <div class="decoration bottom-0"></div>
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="{{ asset('assets/mobile/images/prizes/googleplay.jpg') }}" data-src="{{ asset('assets/mobile/images/prizes/googleplay.jpg') }}" alt="img">
               </a>
               <div class="store-slide-title" id="bronze">
                  <strong>GooglePlay Gift Card</strong>
                  <em class="color-gray-dark">Minimum gift voucher is LP50.00</em>
               </div>
               <div class="store-slide-button">
                  <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> LP50.00</strong>
                     <button class="uppercase ultrabold button-xxs button button-blue withdraw-money" style="float:right"><i class="fa fa-shopping-cart color-white"></i> @lang('general.get_now')</button>
               </div>
            </div>
             <div class="decoration bottom-0"></div>
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="{{ asset('assets/mobile/images/prizes/playstationstore.png') }}" data-src="{{ asset('assets/mobile/images/prizes/playstationstore.png') }}" alt="img">
               </a>
               <div class="store-slide-title" id="bronze">
                  <strong>Playstation Store Gift Card</strong>
                  <em class="color-gray-dark">Minimum gift voucher is LP50.00</em>
               </div>
               <div class="store-slide-button">
                  <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> LP50.00</strong>
                     <button class="uppercase ultrabold button-xxs button button-blue withdraw-money" style="float:right"><i class="fa fa-shopping-cart color-white"></i> @lang('general.get_now')</button>
               </div>
            </div>
            <div class="decoration bottom-0"></div>
            <div class="store-slide-2">
               <a href="#" class="store-slide-image">
               <img class="preload-image" src="{{ asset('assets/mobile/images/prizes/spotify.jpg') }}" data-src="{{ asset('assets/mobile/images/prizes/spotify.jpg') }}" alt="img">
               </a>
               <div class="store-slide-title" id="bronze">
                  <strong>Spotify Gift Card</strong>
                  <em class="color-gray-dark">Minimum gift voucher is LP50.00</em>
               </div>
               <div class="store-slide-button">
                  <strong><!--<del class="color-red-light font-10">@lang('general.100_free_tickets')</del>--> LP50.00</strong>
                     <button class="uppercase ultrabold button-xxs button button-blue withdraw-money" style="float:right"><i class="fa fa-shopping-cart color-white"></i> @lang('general.get_now')</button>
               </div>
            </div>
          @endif
@endsection
@section('scripts')
   @if(Sentinel::check() && Sentinel::getUser()->balance < 50)
   <script>
       $( ".withdraw-money" ).click(function() {
            swal("@lang('general.sorry')! :(", "@lang('general.your_balance_are_inadequate')", "error");
        });
   </script>
   @else
   <script>
       $( ".withdraw-money" ).click(function() {
            swal("Tebrikler!", "Ödüllerle ilgili detaylı bilgi için bizimle iletişime geçiniz.", "success");
        });
   </script>
   @endif
@endsection