@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
            <div class="content">
               <ul class="link-list bottom-0">
                   @forelse($addresses as $key => $val)
                  <li>
                     <a class="inner-link-list" href="#"><i class="fa fa-angle-down"></i><i class="font-13 fas fa-map-marker-alt color-orange-dark"></i><span>{{ $val->title }}</span></a>
                     <ul class="link-list">
                        <p>{{ $val->address . ' ' . $val->district . ' ' . $val->town . ' / ' . $val->city . ' ' . $val->postal_code }}</p>
                     </ul>
                  </li>
                  @empty
                  <p style="text-align:center">Henüz adres eklemediniz.</p>
                  @endforelse
               </ul>
            </div>
@endsection
@section('scripts')
   
@endsection