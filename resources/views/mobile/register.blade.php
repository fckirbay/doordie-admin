@extends('layouts.mobile.main')
@section('styles')
    <style>
        .inlineinput div {
    display: inline;
}

#menu-hider {
        z-index:-11 !important;
    }
    </style>
@endsection
@section('content')

<div class="notification-large notification-has-icon notification-red notification-green bottom-0" style="text-align:center">
<div class="notification-icon"><i class="fa fa-exclamation-triangle notification-icon"></i></div>
<h1 class="uppercase ultrabold" style="font-size:14px">@lang('general.warning')!</h1>
<p>@lang('general.accounts_that_are')</p>
<a href="#" class="close-notification"><i class="fa fa-times"></i></a>
</div>

            <div class="page-login bottom-20">


<!--
                <h3 class="uppercase ultrabold top-30 bottom-0 center-text">@lang('general.signup')</h3>
                
                
                <br /><br />
                <p class="center-text bottom-30">
                  Our membership system was closed for 1 hours. After a short while we will be with you again.
               </p>
               <p class="center-text bottom-30">
                  Üyelik sistemimiz yaklaşık 1 saat süreyle kapatılmıştır. Yapılacak çalışmanın ardından kısa bir süre sonra yeniden sizlerle olacağız.
               </p>
               <a href="{{ url('/') }}" class="button bg-highlight button-s button-center button-rounded uppercase ultrabold">Home</a>
               
               
               -->
               
               


                <p class="smaller-text bottom-30 center-text" style="margin-bottom:10px !important">@lang('general.login_for_get_10_free_box')</p>
                {!! Form::open(['url'=>'signup', 'method'=>'post', 'class'=>'register-form outer-top-xs', 'id'=>'reg'])  !!}
                    <div class="page-login-field top-15">
                        <i class="fa fa-at"></i>
                        <input type="email" name="email" id="email" placeholder="@lang('general.email')" style="text-transform: lowercase;" required>
                        <em>(required)</em>
                    </div>
                    <div class="page-login-field top-15">
                        <i class="fa fa-user"></i>
                        <input type="text" name="username" id="username" placeholder="@lang('general.username')" style="text-transform: lowercase;" required>
                        <em>(required)</em>
                    </div>
                    <div class="page-login-field">
                        <i class="fa fa-lock"></i>
                        <input type="password" name="password" placeholder="@lang('general.password')" maxlength="15" required>
                        <em>(required)</em>
                    </div>
                    <div class="page-login-field">
                        <i class="fa fa-lock"></i>
                        <input type="password" name="password_confirm" placeholder="@lang('general.password_again')" maxlength="15" required>
                        <em>(required)</em>
                    </div>
                    <div class="page-login-field bottom-30">
                        <i class="fa fa-user-plus"></i>
                        <input type="number" name="reference_id" placeholder="@lang('general.reference_member_id')" maxlength="6">
                    </div>
                    <button type="submit" class="button bg-highlight button-full button-rounded button-s uppercase ultrabold" id="reg-button">@lang('general.signup')</button>
                    <div class="page-login-links bottom-10">
                        <a class="forgot center-text" href="{{ url('login') }}"><i class="fa fa-user float-right"></i>@lang('general.already_a_member')</a>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="page-login-links bottom-10">
                        
                        <a class="forgot center-text" href="{{ url('privacy-policy') }}"><i class="fa fa-user-secret float-right"></i>Privacy Policy</a>
                        <div class="clear"></div>
                    </div>
                {!! Form::close() !!}
                
                
<!--
           <h3 class="uppercase bolder center-text" style="margin-top:50px">ŞUAN <span class="color-highlight">BAKIMDAYIZ</span></h3>

          <p class="center-text boxed-text-large bottom-30" style="margin-top:30px">
               Saat 18:00'a kadar sistem bakımı nedeniyle üye alımı durdurulmuştur. Daha sonra tekrar deneyerek üye olabilirsiniz.
          </p>
-->

<div id="menu-share2" class="menu-flyin menu-share active-touch" style="transition: all 350ms ease 0s;">
<div class="menu-flyin-content">
<h1 class="uppercase bolder center-text bottom-10">Privacy Policy</h1>
<p class="center-text"></p>
<div class="menu-share-socials">
<iframe src="http://app.luckybox.fun/privacy-policy" width="100%" height="200" scrolling="yes">
</iframe>
</div>
</div>
</div>


            </div>
            
            
            
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/mobile/scripts/jquery.mask.js') }}"></script>
    <script>
       $(document).ready(function(){
            $('.code').mask('000');
            $('.phone').mask('000000000000');
       });
       
       
       

       
$('#reg').submit(function(){
          $('#reg-button').hide();
       });
       
       $(function() {

            $("input#username").on({
                 keydown: function(e) {
                   if (e.which === 32)
                     return false;
                 },
                 change: function() {
                   this.value = this.value.replace(/\s/g, "");
                 }
               });
        });
    </script>
@endsection