@extends('layouts.mobile.main')
@section('styles')
  <style>
        .box-scratch {
            width: 100%;
            height: 200px;
            border: 1px solid;
            position: relative;
            background: #c8c8c8;
        }

        #canvas {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
        }

        .box-content {
            font: 100px/200px sans-serif;
            text-align: center;
            color: #fff;
        }
  </style>
@endsection
@section('content')

      <h3 class="uppercase bolder center-text" style="margin-top:50px">@lang('general.scratch') &<span class="color-highlight">@lang('general.win')</span></h3>


  <div id="watchVideo" @if($videoCompleted != null || $scratchCompleted != null) style="display:none" @endif>

      <p class="center-text boxed-text-large bottom-30" style="margin-top:30px; font-size:14px">
      @lang('general.watch_video_for_scratch')

      <br /><br />

      <button class="uppercase ultrabold button-xs button button-blue" id="rewarded" onClick="showRewarded();" style="text-align:center"><i class="fa fa-shopping-cart color-white"></i> @lang('general.watch_video')</button>

      

      <span id="rewarded-countdown" style="text-align:center; font-weight:bold"></span>
  </div>
  
  <div class="content" id="scratchwin" style="margin-top:10px; @if(($videoCompleted == null && $scratchCompleted == null) || $scratchCompleted != null) display:none @endif">
    <div class="box-scratch">
        <div class="box-content"></div>
        <canvas id="canvas" width="600" height="200"></canvas>
    </div>
  </div>
  

  <div @if($scratchCompleted == null) style="display:none" @endif>
      <p class="center-text boxed-text-large bottom-30" style="margin-top:30px; font-size:14px">
      @lang('general.you_already_scratch')

      <p id="demo" style="font-size:20px; text-align: center"></p>
  </div>
@endsection
@section('scripts')
<script src="{{ asset('assets/mobile/scripts/countdown/dist/jquery.countdown.js') }}"></script>
<script src="{{ asset('assets/mobile/scripts/scratchwin/scratchCard.js') }}"></script>
  
            <script type="text/javascript">
              
              function showRewarded() {
                var user_id = "{{ Sentinel::getUser()->id }}";
                $("#rewarded").hide();
                $("#rewarded-countdown").show();
                
                    $.ajax({
                        url: 'click-scratch',
                        method: 'POST',
                        data:{
                          type: 2,
                          _token: "{{csrf_token()}}"
                        },
                        dataType: 'json',
                        success: function(data) {

                          

                          JSON.stringify(data); //to string
                          if(data.result === "success") {

                            android.showLoader();
                            android.showRewarded(user_id, data.token);
                            setTimeout(
                              function() 
                              {
                                  android.hideLoader();
                                  $("#rewarded").show();
                              }, 3000);

                          } else {
                            $('#warning-text').html(data.result);
                            //$('.box').show();
                            //alert(data.result);
                          }
                          
                        }
                      });
                    
              }

              

            </script>

            @if($videoCompleted == null)
            <script>
              var interval = setInterval(function() {
                      $.ajax({
                        url: 'check-watch',
                        method: 'GET',
                        data:{
                          _token: "{{csrf_token()}}"
                        },
                        dataType: 'json',
                        success: function(data) {
                          JSON.stringify(data); //to string
                          if(data.result === "success") {
                            location.reload();
                            clearInterval(interval);
                          }
                        }
                      });
              }, 3000);
          </script>
          @endif



      

<script>

  var scratchCard = new ScratchCard({
        id: '#canvas',//canvas query string
        //maskColor : '#ccc' ,//背景色（如果遮罩图片是jpg可以不写）
        maskImage: './assets/mobile/images/scratchwin/bg.jpg',   // 遮罩图片路径（推荐使用jpg，防止出现透明部分）(如果只需要纯色，可以只设置maskColor)
        maskBack: function () {

            //业务逻辑
            console.log('去取数据。。。');
            let num = Math.floor(Math.random() * 10);
            let txt = "{{ session('scratch_prize') }}";
            document.querySelector('.box-content').innerHTML = txt;

            let _this = scratchCard;

            
            // 画文字
            _this.paintText({
                font: 'bold 60px Hanzipen SC',
                textAlign: 'center',
                textBaseline: 'middle',
                text: "{{ \Lang::get('general.scratch_card') }}",
                fillStyle: '#fff',
                x: _this.canvasWidth / 2 / _this.dpi,
                y: _this.canvasHeight / 2 / _this.dpi,
            });



        },    //画完遮罩后，可以执行其他内容，如：从后台取数据、在遮罩上画其他内容
        //openPercent: 0.6,  // 刮开多少后全部显示 范围：0< x < 1,默认0.6
        progressBack : function(progress){
            console.log('进度：',progress);
        },
        endBack: function () {
            $.ajax({
                        url: 'complete-scratch',
                        method: 'POST',
                        data:{
                          _token: "{{csrf_token()}}"
                        },
                        dataType: 'json',
                        success: function(data) {
                          JSON.stringify(data); //to string
                          if(data.result === "success") {
                            swal(data.message_title, data.message, "success");
                          }
                        }
                      });
        }, //完全刮开后的回调
        //endHide: true,    //画完后 canvas 是否会 display:none; 默认true

    });
  </script>
  @if($date != null)
      <script>
        // Set the date we're counting down to
        //var countDownDate = new Date("Jan 5, 2019 15:37:25").getTime();
        var countDownDate = new Date("{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->addDay()->format('M d, Y H:i:s') }}").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

          // Get todays date and time
          var now = new Date("{{ Carbon\Carbon::now()->format('M d, Y H:i:s') }}").getTime();

          // Find the distance between now and the count down date
          var distance = countDownDate - now;

          // Time calculations for days, hours, minutes and seconds
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);

          // Display the result in the element with id="demo"
          document.getElementById("demo").innerHTML = hours + "h "
          + minutes + "m " + seconds + "s ";

          // If the count down is finished, write some text 
          if (distance < 0) {
            clearInterval(x);
            //document.getElementById("demo").innerHTML = "EXPIRED";
          }
        }, 1000);
        </script>
    @endif
@endsection