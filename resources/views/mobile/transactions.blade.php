@extends('layouts.mobile.main')
@section('styles')
    
@endsection
@section('content')
            <div class="content" style="margin-top:10px">
                @forelse($transactions as $key => $val)
                    <div class="system-box"><strong>@if($val->type == 1) @lang('general.withdraw') (@if(Sentinel::getUser()->currency == "eur")€@endif{{ number_format($val->amount, 2, ',', '.') }} @if(Sentinel::getUser()->currency == "try")₺@endif)@else @lang('general.premium_membership') (@if(Sentinel::getUser()->currency == "eur")€@endif{{ number_format($val->amount, 2, ',', '.') }} @if(Sentinel::getUser()->currency == "try")₺@endif)@endif </strong>@if($val->status == 0)<em class="color-yellow-dark">@lang('general.pending') <i class="fa fa-circle"></i> </em>@elseif($val->status == 1)<em class="color-green-dark">@lang('general.confirmed') <i class="fa fa-circle"></i> </em>@elseif($val->status == 2)<em class="color-orange-dark">@lang('general.rejected') <i class="fa fa-circle"></i> </em>@else<em class="color-blue-dark">@lang('general.error') <i class="fa fa-circle"></i> </em>@endif<br /> <span style="font-size:10px">@lang('general.transaction_date'): {{ Carbon\Carbon::parse($val->created_at)->format('H:i d M') }} @if($val->confirm_date != null)@lang('general.confirmation_date'): {{ Carbon\Carbon::parse($val->confirm_date)->format('H:i d M') }}@endif</span></div>
                @empty
                    <div style="text-align:center; margin-top:25px; font-size:16px; font-weight:bold">@lang('general.you_dont_have_any_transaction')</div>
                @endforelse
                <div class="clear"></div>
            </div>
@endsection
@section('scripts')
    
@endsection