@extends('layouts.mobile.main')
@section('styles')
    <style>
       @-webkit-keyframes ticker {
  0% {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    visibility: visible;
  }
  100% {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}
@keyframes ticker {
  0% {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    visibility: visible;
  }
  100% {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}
.ticker-wrap {
  width: 100%;
  overflow: scroll;
  height: 30px;
  background-color: rgba(0, 0, 0, 0.9);
  padding-left: 100%;
}
.ticker-wrap .ticker {
  display: inline-block;
  height: 30px;
  line-height: 30px;
  white-space: nowrap;
  padding-right: 100%;
  box-sizing: content-box;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-name: ticker;
  animation-name: ticker;
  -webkit-animation-duration: 30s;
  animation-duration: 150s;
}
.ticker-wrap .ticker__item {
  display: inline-block;
  padding: 0 2rem;
  font-size: 16px;
  color: white;
}
    </style>
@endsection
@section('content')
            <!--<a data-menu="menu-3" href="#" class="demo-settings demo-settings-fixed bg-highlight">Settings</a>-->
            <div class="single-slider owl-carousel owl-no-dots bottom-30" style="margin-bottom:0px !important">
               @if(session('lang') == "gb")
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="{{ asset('assets/mobile/images/slider/31.jpg') }}" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="{{ asset('assets/mobile/images/slider/41.png') }}" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="{{ asset('assets/mobile/images/slider/21.jpg') }}" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="{{ asset('assets/mobile/images/slider/11.jpg') }}" alt="" style="width:100%; height:150px"></a>
               </div>
               @else
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="{{ asset('assets/mobile/images/slider/32.jpg') }}" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="{{ asset('assets/mobile/images/slider/42.png') }}" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="{{ asset('assets/mobile/images/slider/22.jpg') }}" alt="" style="width:100%; height:150px"></a>
               </div>
               <div class="store-slide-1">
                  <a href="#" class="store-slide-image" style="margin: 0px auto 30px !important; max-width:100% !important"><img src="{{ asset('assets/mobile/images/slider/12.jpg') }}" alt="" style="width:100%; height:150px"></a>
               </div>
               @endif
            </div>
            <div class="content-full">
               <!--<p class="center-text boxed-text-large bottom-30">
                  Plenty of categories to choose from. You can also use icon lists like the main menu for this section.
               </p>-->
               @if(Sentinel::check() && (Sentinel::getUser()->id == 1 || Sentinel::getUser()->id == 8 || Sentinel::getUser()->id == 18888))
               <!--
               <button class="uppercase ultrabold button-xs button button-blue" id="rewarded" onClick="showOfferWall();" style="text-align:center"><i class="fa fa-shopping-cart color-white"></i> OfferWall</button>
                <button class="uppercase ultrabold button-xxs button button-bl覺e" onClick="showInterstitial();" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Show Interstitial</button>
                <button class="uppercase ultrabold button-xxs button button-bl覺e" onClick="showRewarded();" style="float:right"><i class="fa fa-shopping-cart color-white"></i> Show Rewarded</button>
              -->
               @endif
               
               <div class="ticker-wrap" style="margin-top:-10px">
                  <div class="ticker">
                    <!--
                     @foreach($prizes as $key => $val)
                     <?php 
                        if($val->currency == "try") {
                            $curr = "LP";
                        } else {
                            $curr = "LP";
                        }
                     ?>
                        <div class="ticker__item">@lang('general.win_x_prize', [ 'user' => $val->first_name, 'prize' => number_format($val->prize, 2, ',', '.') . ' ' . $curr ])</div>
                    @endforeach
                  -->
                  @foreach($completedTasks as $key => $val)
                  <div class="ticker__item">@lang('general.user_completed_task', [ 'user' => $val->first_name, 'prize' => $val->point_value ])</div>
                  @endforeach
                  </div>
               </div>
               

               
               



@if(Sentinel::check() && Carbon\Carbon::now() < '2018-10-31 21:00:00')

<div class="content">
                  <h3 class="uppercase bolder center-text"><span class="color-highlight">Bonus</span></h3>
               </div>

<div class="content" style="margin-top:10px">
{!! Form::open(['url'=>'free-bonus', 'method'=>'post', 'class'=>'register-form outer-top-xs'])  !!}
                  <div class="input-simple-1 has-icon input-green bottom-30" style="text-align:center"><em>@lang('general.enter_code_to_win_ticket')</em><i class="fa fa-user"></i><input type="text" name="bonus_code" placeholder="@lang('general.bonus_code')" value="LUCKY"></div>

                   <button type="submit" class="button button-green" style="width:100%">@lang('general.get_bonus')</button>
               {!! Form::close() !!}
             </div>

<div class="decoration decoration-margins"></div>

@else
<!--
<div class="content">
                  <h3 class="uppercase bolder center-text"><span class="color-highlight">Bonus</span></h3>
               </div>
<div class="content" style="text-align:center">
@lang('general.no_active_bonus')
             </div>

<div class="decoration decoration-margins"></div>
-->
@endif

@if(Sentinel::check() && Sentinel::getUser()->verification == 1)
<div class="card card-red" style="padding: 8px 0px; margin: 20px 0px; height: 80px">
@if(Sentinel::check() && Sentinel::getUser()->currency == "try")
<h1 class="color-white center-text uppercase bolder top-10" style="font-size:40px">{{ number_format($earnings, 2, ',', '.') }} LP</h1>
@else
<h1 class="color-white center-text uppercase bolder top-10" style="font-size:40px">LP{{ number_format($earnings / 7, 2, '.', ',') }}</h1>


@endif
<p class="small-text center-text color-white bottom-15 opacity-50" style="margin-top:5px; font-size:16px !important">@lang('general.total_earnings_homepage')</p>
</div>
<div class="decoration decoration-margins" style="margin-bottom:10px"></div>
@endif
               
                <p class="center-text boxed-text-large">
                    @lang('general.you_can_earn_5_extra_tickets')
                    @if(!Sentinel::check())
                    <br /><br />
                    <a href="{{ url('signup') }}" class="button button-rounded button-dark" style="font-weight:bold; font-size:18px">@lang('general.signup')</a>
                    @endif
                </p>
                
                <div class="decoration decoration-margins" style="margin-top:10px"></div>


                
               <div class="content">
                  <h3 class="uppercase bolder center-text">@lang('general.active') <span class="color-highlight">@lang('general.rooms')</span></h3>
               </div>
               
               <div class="blog-categories blog-categories-3 bottom-20">
                  @foreach($rooms as $val)
                     <a href="{{ url('room', $val->slug) }}?show=1"><strong></strong><em>@if(Lang::locale() == "tr"){{ $val->name }}@else{{ $val->name_en }}@endif</em><span class="bg-red-dark opacity-50"></span><img src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset($val->photo) }}" class="preload-image responsive-image" alt="img"></a>
                  @endforeach
                  <div class="clear"></div>
               </div>
               
            </div>
            <div class="decoration decoration-margins"></div>
<!--
            <div class="double-slider owl-carousel owl-has-dots bottom-15">
                  <div class="store-featured-1">
                     <a href="#" style="width:100%; max-width:500px"><img src="{{ asset('assets/mobile/images/payment_methods/ecopayz.png') }}" alt="img" style="width:100%; max-width:500px"></a>
                  </div>
                  <div class="store-featured-1">
                     <a href="#" style="width:100%; max-width:500px"><img src="{{ asset('assets/mobile/images/payment_methods/skrill.png') }}" alt="img" style="width:100%; max-width:500px"></a>
                  </div>
                  <div class="store-featured-1">
                     <a href="#" style="width:100%; max-width:500px"><img src="{{ asset('assets/mobile/images/payment_methods/neteller.png') }}" alt="img" style="width:100%; max-width:500px"></a>
                  </div>
                  <div class="store-featured-1">
                     <a href="#" style="width:100%; max-width:500px"><img src="{{ asset('assets/mobile/images/payment_methods/jeton.png') }}" alt="img" style="width:100%; max-width:500px"></a>
                  </div>
                  <div class="store-featured-1">
                     <a href="#" style="width:100%; max-width:500px"><img src="{{ asset('assets/mobile/images/payment_methods/payoneer.png') }}" alt="img" style="width:100%; max-width:500px"></a>
                  </div>
               </div>
-->
@endsection
@section('scripts')
  
@endsection