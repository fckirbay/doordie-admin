@extends('layouts.mobile.main')
@section('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endsection
@section('content')
            <div class="content" style="margin-top:20px">
                 <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">1 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>25.000 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">10 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>10.000 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">25 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>5.000 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">50 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>2.500 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">75 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>1.000 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">100 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>500 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">250 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>250 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">500 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>100 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">750 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>50 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">1000 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>25 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">2500 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>10 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">10000 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>5 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">25000 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>3 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">50000 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>2 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">100000 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>1 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">100000 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>0.50 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">100000 x</strong>
                    <span style="padding-left:60px" style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>0.25 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">100000 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>0.10 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">200000 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>0.05 LP <!--<del> Was $500</del>--></em>
               </div>
               <div class="store-cart-2" style="min-height:0px; margin-bottom:20px">
                    <img class="preload-image" src="{{ asset('assets/mobile/images/empty.png') }}" data-src="{{ asset('assets/mobile/images/lp.png') }}" alt="img" style="width:35px; height:35px">
                    <strong style="padding-left:60px">300000 x</strong>
                    <span style="padding-left:60px">@lang('general.europe-world')</span>
                    <em>0.01 LP <!--<del> Was $500</del>--></em>
               </div>
            </div>
@endsection
@section('scripts')
     
@endsection