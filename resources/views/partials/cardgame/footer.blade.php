			<!-- class="active-nav" -->
			<div id="footer-menu" class="footer-menu-5-icons footer-menu-style-1">
				<a href="{{ url('/') }}"><i class="fas fa-bars"></i><span>@lang('general.home')</span></a>
				<a href="{{ url('/bank') }}"><i class="fas fa-dollar-sign"></i><span>@lang('general.safe')</span></a>
				<a href="{{ url('/play') }}"><i class="far fa-play-circle"></i><span>@lang('general.new_game')</span></a>
				<a href="{{ url('opportunities') }}"><i class="fas fa-dice"></i><span>@lang('general.opportunities')</span></a>
				<a href="{{ url('/prizes') }}" data-menu="menu-settings"><i class="fas fa-gift"></i><span>@lang('general.prizes')</span></a>
				<div class="clear"></div>
			</div>