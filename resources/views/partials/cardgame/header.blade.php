<div class="header header-fixed header-logo-center">
	@if(URL::previous() !== URL::current())
		<a href="{{ URL::previous() }}" class="back-button header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>
	@endif
	<a href="{{ url('/') }}" class="header-title">Do <span style="opacity:0.3">or</span> Die</a>
	<!--<a href="#" class="back-button header-icon header-icon-1"><i class="fas fa-arrow-left"></i></a>-->
	@if(Sentinel::check())
	<!-- data-toggle-theme-switch -->
	<a href="#" class="header-icon header-icon-4" data-menu="menu-list">
		<i class="fa fa-user"></i>
	</a>
	@else
	<a href="{{ url('login') }}" class="header-icon header-icon-4"><i class="fas fa-sign-in-alt"></i></a>
	@endif
</div>
<div id="menu-list" class="menu menu-box-modal round-medium" data-menu-width="310" data-menu-height="257" data-menu-effect="menu-over" style="border-radius: 20px !important">
	<div class="link-list link-list-1 content bottom-0">
		<a href="{{ url('my-account') }}">
			<i class="fas fa-user color-facebook"></i>
			<span class="font-13">@lang('general.my_account')</span>
			<i class="fa fa-angle-right"></i>
		</a>
		<a href="{{ url('history') }}">
			<i class="fas fa-history color-twitter"></i>
			<span class="font-13">@lang('general.game_history')</span>
			<i class="fa fa-angle-right"></i>
		</a>
		<!--
		<a href="#" class="shareToLinkedIn">
			<i class="font-18 fab fa-linkedin color-linkedin"></i>
			<span class="font-13">Takipçilerim</span>
			<i class="fa fa-angle-right"></i>
		</a>
		<a href="#" class="shareToGooglePlus">
			<i class="font-18 fab fa-google-plus-square color-google"></i>
			<span class="font-13">Takip Ettiklerim</span>
			<i class="fa fa-angle-right"></i>
		</a>
		-->
		<a href="{{ url('my-references') }}">
			<i class="fas fa-user-plus color-whatsapp"></i>
			<span class="font-13">@lang('general.references')</span>
			<i class="fa fa-angle-right"></i>
		</a>
		<a href="{{ url('support') }}">
			<i class="fas fa-comment-dots color-mail"></i>
			<span class="font-13">@lang('general.support')</span>
			<i class="fa fa-angle-right"></i>
		</a>
		<a href="{{ url('logout') }}" class="no-border">
			<i class="fas fa-sign-out-alt color-google"></i>
			<span class="font-13">@lang('general.logout')</span>
			<i class="fa fa-angle-right"></i>
		</a>
	</div>
</div>