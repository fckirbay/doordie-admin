@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tamamlanan Görevler</h3>
              {!! Form::open(['url'=>'admin/offerwall', 'method' => 'get', 'style' => 'float:right', 'id'=>'search-form'])  !!}
              <div class="box-tools">

                <div class="row">
                  <div class="col-xs-5">
                    <select class="form-control" name="order">
                      <option value="sum">Kazanç</option>
                      <option value="total">Sayı</option>
                      <option value="complete_date">Tarih</option>
                    </select>
                  </div>
                  <div class="col-xs-5">
                    <select class="form-control" name="days">
                      <option value="1095">Tüm Zamanlar</option>
                      <option value="3">Son 3 Gün</option>
                      <option value="7" selected>Son 1 Hafta</option>
                      <option value="14">Son 2 Hafta</option>
                      <option value="30">Son 1 Ay</option>
                      <option value="60">Son 2 Ay</option>
                      <option value="90">Son 3 Ay</option>
                      <option value="180">Son 6 Ay</option>
                      <option value="365">Son 1 Yıl</option>
                    </select>
                  </div>
                  <div class="col-xs-2">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
              {!! Form::close() !!}
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:25%">Kullanıcı ID</th>
                  <th class="orta" style="width:10%">Görevler</th>
                  <th class="orta" style="width:10%">Toplam ($)</th>
                </tr>
                @forelse($offers as $key => $val)
                <tr>
                  <td><a href="{{ url('admin/user-management/user', $val->user_id) }}">{{ $val->username }}</a></td>
                  <td class="orta">{{ $val->total }}</td>
                  <td class="orta">${{ $val->sum }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="3" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            {!! $offers->appends(Request::capture()->except('page'))->render() !!}
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  @include('sweet::alert')
@endsection