@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-android"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="#" style="color:#000">Android</a></span>
              <span class="info-box-number">@if(isset($os['android'])){{ $os['android'] }}@else 0 @endif</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-apple"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="#" style="color:#000">iOS</a></span>
              <span class="info-box-number">@if(isset($os['ios'])){{ $os['ios'] }}@else 0 @endif</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-question"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="#" style="color:#000">Unknown</a></span>
              <span class="info-box-number">@if(isset($os[''])){{ $os[''] }}@else 0 @endif</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="{{ url('panel/users') }}" style="color:#000">Onaylı</a></span>
              <span class="info-box-number">@if(isset($users[1])){{ $users[1] }}@else 0 @endif</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-times"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="{{ url('panel/application/bets') }}" style="color:#000">Onaysız</a></span>
              <span class="info-box-number">@if(isset($users[0])){{ $users[0] }}@else 0 @endif</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-black"><i class="fa fa-ban"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="#" style="color:#000">Engellenen</a></span>
              <span class="info-box-number">{{ $blockeds }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ülkeler</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>#</th>
                  <th style="width:40%">Ülke</th>
                  <th class="orta" style="width:50%">Üye Sayısı</th>
                </tr>
                @forelse($countries as $key => $val)
                <tr>
                  <td>@if($val->country != null)<img src="{{ asset('assets/cardgame/images/flags/'.strtolower($val->country).'.svg') }}" style="width:30px; height:20px"/>@else<img src="{{ asset('assets/mobile/images/flags/un.png') }}"/>@endif</td>
                  <td>{{ $val->country }}</td>
                  <td class="orta">{{ $val->total }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

      
    </section>
    <!-- /.content -->

@endsection
@section('scripts')

<!-- Sparkline -->
<script src="{{ asset('assets/admin/plugins/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap  -->
<script src="{{ asset('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('assets/admin/plugins/chart.js/Chart.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('assets/admin/js/pages/dashboard2.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/admin/js/demo.js') }}"></script>
@endsection