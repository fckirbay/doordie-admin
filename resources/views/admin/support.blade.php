@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Destek</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:5%">#</th>
                  <th style="width:5%">Kullanıcı ID</th>
                  <th style="width:20%">Mesaj</th>
                  <th class="orta" style="width:10%">Tarih</th>
                  <th class="orta" style="width:10%">#</th>
                </tr>
                @forelse($supports as $key => $val)
                <tr @if($val->is_viewed == 1) style="color:gray" @endif>
                  <td>@if($val->country != null)<img src="{{ asset('assets/cardgame/images/flags/'.strtolower($val->country).'.svg') }}" style="width:30px; height:20px"/>@endif</td>
                  <td><a href="{{ url('admin/user-management/user/'.$val->user_id.'?chat=1') }}" @if($val->is_viewed == 1) style="color:gray" @else style="color:black" @endif>{{ $val->username }}</a></td>
                  <td>{{ $val->message}}</td>
                  <td class="orta">{{ Carbon\Carbon::parse($val->date)->format('d M - H:i:s') }}</td>
                  <td class="orta">@if($val->is_viewed == 0)<a class="btn btn-danger btn-xs" href="{{ url('admin/delete-support', $val->id) }}">Sil</a>@endif</td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            {{ $supports->links() }}
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  @include('sweet::alert')
@endsection