@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hazır Cevaplar</h3>
              <div class="box-tools">

                <div class="row">
                  <div class="col-xs-12">
                    <button type="button" data-toggle="modal" data-target="#addnew" class="btn btn-default"><i class="fa fa-plus"></i> Yeni Ekle</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th class="orta" style="width:30%">Soru</th>
                  <th class="orta" style="width:55%">Cevap</th>
                  <th class="orta" style="width:5%">#</th>
                </tr>
                @forelse($answers as $key => $val)
                <tr>
                  <td>{{ $val->question }}</td>
                  <td>{{ $val->answer }}</td>
                  <td class="orta"><a href="{{ url('admin/delete-ready-answer', $val->id) }}" class="btn btn-danger btn-xs">Sil</a></td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
    <div class="modal fade" id="addnew">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Yeni Hazır Cevap</h4>
              </div>
              {!! Form::open(['url'=>'admin/ready-answers', 'method'=>'post', 'autocomplete' => 'off'])  !!}
              <div class="modal-body">
	              <div class="box-body">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="form-group">
	                          <label>Soru</label>
	                          <textarea class="form-control" name="question"></textarea>
	                        </div>
	                    </div>
	                    <div class="col-md-12">
	                        <div class="form-group">
	                          <label>Cevap</label>
	                          <textarea class="form-control" name="answer"></textarea>
	                        </div>
	                    </div>
	                 </div>
	              </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Kapat</button>
                <button type="submit" class="btn btn-success">Kaydet</button>
              </div>
              {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
@endsection
@section('scripts')
  @include('sweet::alert')
@endsection