@extends('layouts.admin.main')
@section('styles')
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{ asset('assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset('assets/admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{ asset('assets/admin/plugins/timepicker/bootstrap-timepicker.min.css') }}">
@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Ayarlar</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              {!! Form::open(['url'=>'admin/send-notification', 'method'=>'post', 'autocomplete' => 'off'])  !!}
              <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>Ülke Seçiniz</label>
                          <select class="form-control" name="country" required>
                            <option value="1">Türkiye</option>
                            <option value="2">Uluslararası</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>Bildirim Metni</label>
                          <textarea class="form-control" name="notification"></textarea>
                        </div>
                    </div>

                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Kaydet</button>
              </div>
            {!! Form::close() !!}


            <br /><br />

            {!! Form::open(['url'=>'admin/send-message', 'method'=>'post', 'autocomplete' => 'off', 'style'=>'margin-bottom:100px'])  !!}
              <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>Ülke Seçiniz</label>
                          <select class="form-control" name="country" required>
                            <option value="1">Türkiye</option>
                            <option value="2">Uluslararası</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>Mesaj Metni</label>
                          <textarea class="form-control" name="message" minlength="10" required></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <!-- Date and time range -->
                        <div class="form-group">
                          <label>Tarih</label>

                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" name="date" id="datepicker">
                          </div>
                          <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <div class="col-md-12">
                        <!-- time Picker -->
                        <div class="bootstrap-timepicker">
                          <div class="form-group">
                            <label>Saat</label>

                            <div class="input-group">
                              <input type="text" class="form-control timepicker" name="time">

                              <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                              </div>
                            </div>
                            <!-- /.input group -->
                          </div>
                          <!-- /.form group -->
                        </div>
                    </div>
                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Kaydet</button>
              </div>
            {!! Form::close() !!}

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  <!-- date-range-picker -->
  <script src="{{ asset('assets/admin/plugins/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- bootstrap datepicker -->
  <script src="{{ asset('assets/admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <!-- bootstrap color picker -->
  <script src="{{ asset('assets/admin/plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
  <!-- bootstrap time picker -->
  <script src="{{ asset('assets/admin/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
  <script>
    $(function () {
      //Date picker
      $('#datepicker').datepicker({
        autoclose: true
      })
      //Timepicker
      $('.timepicker').timepicker({
        showInputs: false,
        showMeridian: false
      })
    });
  </script>
  @include('sweet::alert')
@endsection