@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hatalar <a class="btn btn-danger btn-xs" href="{{ url('admin/delete-errors') }}">Tümünü Sil</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:50%">Hata Mesajı</th>
                  <th class="orta" style="width:10%">Hata Kodu</th>
                  <th class="orta" style="width:20%">Dosya</th>
                  <th class="orta" style="width:10%">Satır</th>
                  <th class="orta" style="width:10%">Tarih</th>
                </tr>
                @forelse($errors as $key => $val)
                <tr>
                  <td>{{ $val->message }}</td>
                  <td class="orta">{{ $val->code }}</td>
                  <td class="orta">{{ $val->file }}</td>
                  <td class="orta">{{ $val->row }}</td>
                  <td class="orta">{{ Carbon\Carbon::parse($val->date)->format('d/m/Y H:i:s') }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            {{ $errors->links() }}
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  @include('sweet::alert')
@endsection