@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
	<!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Kullanıcılar</h3>
                
              {!! Form::open(['url'=>'admin/user-management/users', 'method' => 'get', 'style' => 'float:right', 'id'=>'search-form'])  !!}
              <div class="box-tools">

                <div class="row">
                  <div class="col-xs-5">
                    <select class="form-control" name="order">
                      <option value="">Seçiniz</option>
                      <option value="balance">Bakiye</option>
                      <option value="ticket">Bilet Sayısı</option>
                      <option value="reference_count">Referans Sayısı</option>
                      <option value="clicks">Tıklamalar</option>
                    </select>
                  </div>
                  <div class="col-xs-5">
                    <input type="text" name="search" class="form-control pull-right" placeholder="Ara">
                  </div>
                  <div class="col-xs-2">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
              {!! Form::close() !!}
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th style="width:5%">Ülke</th>
                  <th style="width:3%">OS</th>
                  <th class="orta" style="width:5%">E/D</th>
                  <th style="width:22%">Kullanıcı Adı</th>
                  <th class="orta" style="width:10%">Telefon Numarası</th>
                  <th class="orta" style="width:10%">Bilet Sayısı</th>
                  <th class="orta" style="width:10%">Referans Sayısı</th>
                  <th class="orta" style="width:10%">Bakiye</th>
                  <th class="orta" style="width:5%">Deneme Sayısı</th>
                  <th class="orta" style="width:5%">#</th>
                </tr>
                @forelse($users as $key => $val)
                <tr>
                  <td>@if($val->country != null)<img src="{{ asset('assets/cardgame/images/flags/'.strtolower($val->country).'.svg') }}" style="width:30px; height:20px"/>@else<img src="{{ asset('assets/cardgame/images/flags/unknown.png') }}" style="width:30px; height:20px"/>@endif</td>
                  <td>@if($val->os == 'android')<i class="fa fa-android" style="color: #99CC00"></i>@elseif($val->os == 'ios')<i class="fa fa-apple" style="color: #A6B1B7"></i>@endif</td>
                  <td class="orta">@if($val->blocked == 1)<i class="fa fa-times" style="color:red"></i>@else<i class="fa fa-check" style="color:green"></i>@endif @if($val->verification == 0)<i class="fa fa-times" style="color:red"></i>@else<i class="fa fa-check" style="color:green"></i>@endif</td>
                  <td><a href="{{ url('admin/user-management/user', $val->id) }}">{{ $val->username }}</a></td>
                  <!--<td>@if($val->country != null)<img src="{{ asset('assets/mobile/images/flags/'.trim(strstr($val->country, '(', true)).'.png') }}" style="width:30px; height:20px"/>@endif {{ strstr($val->country, '(', true) }}</td>-->
                  <td class="orta">{{ $val->phone }}</td>
                  <td class="orta">{{ $val->ticket }}</td>
                  <td class="orta">{{ $val->reference_count }}</td>
                  <td class="orta">{{ number_format($val->balance / 100, 2, ',', '.') }} @if($val->currency == "eur")€@else₺@endif</td>
                  <td class="orta">@if($val->verification_tries >= 3)<span style="font-weight:bold; color:red">{{ $val->verification_tries }}</span>@else{{ $val->verification_tries }}@endif</td>
                  <td class="orta"><a href="{{ url('admin/user-management/user', $val->id) }}" class="btn btn-primary btn-xs">Detay</a></td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" style="text-align:center">Hiç kayıt bulunamadı.</td>
                </tr>
                @endforelse
              </table>
            </div>
            {!! $users->appends(Request::capture()->except('page'))->render() !!}
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  <script>
    $('#search-form').submit(function() {
        $(this).find(":input").filter(function(){return !this.value;}).attr("disabled", "disabled");
    });
    // Un-disable form fields when page loads, in case they click back after submission or client side validation
    $('#search-form').find(":input").prop("disabled", false);
  </script>
  @include('sweet::alert')
@endsection