@extends('layouts.admin.main')
@section('styles')

@endsection
@section('content')
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <p class="text-center" style="text-align:center">@if($user->country != null)<img src="{{ asset('assets/cardgame/images/flags/'.strtolower($user->country).'.svg') }}" style="width:30px; height:20px"/>@endif</p>
              
              <h3 class="profile-username text-center">{{ $user->username }}</h3>

              <p class="text-muted text-center">{{ $user->country }} <br /> @if($user->ip_address != null)IP Adresi: <a href="https://tools.keycdn.com/geo?host={{ $user->ip_address }}">{{ $user->ip_address }}</a> <br />@if(isset($userLocations[0])){{ $userLocations[0]->city . ' / ' . $userLocations[0]->country_name }}@endif @endif</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Bakiye</b> <a class="pull-right">@if($user->country_code != "90")€@endif{{ number_format($user->balance/100, 2, ',', '.') }} @if($user->country_code == "90")₺@endif</a>
                </li>
                <li class="list-group-item">
                  <b>Toplam Kazanç</b> <a class="pull-right">@if($user->country_code != "90")€@endif{{ number_format($user->earnings, 2, ',', '.') }} @if($user->country_code == "90")₺@endif</a>
                </li>
                <li class="list-group-item">
                  <b>Bilet</b> <a class="pull-right">{{ $user->ticket }}</a>
                </li>
                <!--<li class="list-group-item">
                  <b>Açtığı Kutu</b> <a class="pull-right">{{ $user->clicks }}</a>
                </li>-->
                <li class="list-group-item">
                  <b>Kazanan</b> <a class="pull-right">{{ $won }}</a>
                </li>
                <li class="list-group-item">
                  <b>Kaybeden</b> <a class="pull-right">{{ $lost }}</a>
                </li>
                <li class="list-group-item">
                  <b>Ref. Sayısı</b> <a class="pull-right">{{ count($references) }}</a>
                </li>
                <li class="list-group-item">
                  <b>Video İzleme</b> <a class="pull-right">{{ count($rewardeds) }}</a>
                </li>
                <li class="list-group-item">
                  <b>Offerwall</b> <a class="pull-right">${{ $offerwall_total }}</a>
                </li>
                <li class="list-group-item">
                  <b>Ref. ID</b> <a href="{{ url('admin/user-management/user', $user->reference_id) }}" class="pull-right">{{ $user->reference_id }}</a>
                </li>
                <li class="list-group-item">
                  <b>İşletim Sistemi</b> <a class="pull-right">{{ $user->os }}</a>
                </li>
                <li class="list-group-item">
                  <b>Versiyon</b> <a class="pull-right">{{ $user->app_version }}</a>
                </li>
                <li class="list-group-item">
                  <b>Üyelik Tarihi</b> <a class="pull-right">{{ Carbon\Carbon::parse($user->createdAt)->format('d M Y - H:i:s') }}</a>
                </li>
                
              </ul>

              
              @if($user->blocked == 1)
                <span style="text-align:center">ENGELLENME NEDENİ:<br />
                @if($user->bloked_reason == null)
                  Belirsiz
                @else
                  {{ $user->blocked_reason }}
                @endif
                </span>
              @endif
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li><a href="#activity" data-toggle="tab"><i class="fa fa-info"></i></a></li>
              <li><a href="#jokers" data-toggle="tab"><i class="fa fa-diamond"></i></a></li>
              <li @if(isset($_GET['chat'])) class="active" @endif><a href="#message" data-toggle="tab"><i class="fa fa-envelope"></i></a></li>
              <li><a href="#references" data-toggle="tab"><i class="fa fa-user-plus"></i></a></li>
              <li><a href="#rewardeds" data-toggle="tab"><i class="fa fa-video-camera"></i></a></li>
              <li><a href="#offerwall" data-toggle="tab"><i class="fa fa-th"></i></a></li>
              <li><a href="#timeline" data-toggle="tab"><i class="fa fa-chevron-up"></i></a></li>
              <!--<li><a href="#locations" data-toggle="tab"><i class="fa fa-map-marker"></i></a></li>-->
              <li><a href="#notification" data-toggle="tab"><i class="fa fa-bell"></i></a></li>
              <li><a href="#settings" data-toggle="tab"><i class="fa fa-th-list"></i></a></li>
              <!--<li><a href="#scratches" data-toggle="tab"><i class="fa fa-pencil-square"></i></a></li>-->
              <li><a href="#bonuses" data-toggle="tab"><i class="fa fa-life-ring"></i></a></li>
              <!--<li><a href="#exchanges" data-toggle="tab"><i class="fa fa-exchange"></i></a></li>-->
              <li><a href="#sendticket" data-toggle="tab"><i class="fa fa-ticket"></i></a></li>
              <li><a href="#otheraccounts" data-toggle="tab"><i class="fa fa-users"></i></a></li>
              <!--<li><a href="#premiummembership" data-toggle="tab"><i class="fa fa-usd"></i></a></li>-->
              <li><a href="#payment" data-toggle="tab"><i class="fa fa-money"></i></a></li>
              <!--<li><a href="#recovers" data-toggle="tab"><i class="fa fa-asterisk"></i></a></li>-->
              <li><a href="#notes" data-toggle="tab"><i class="fa fa-edit"></i></a></li>
            </ul>
            <div class="tab-content">

              <div class="@if(isset($_GET['chat'])) active @endif tab-pane" id="message">


            <!-- DIRECT CHAT -->
                <!-- /.box-header -->
                  <!-- Conversations are loaded here -->
                  <!-- /.box-body -->
                <div class="box-footer">
                  {!! Form::open(['url'=>'admin/user-management/send-message', 'method'=>'post', 'autocomplete' => 'off'])  !!}

                  <select class="form-control" id="drafts">
                        <option>Bir hazır mesaj seçin...</option>
                        @forelse($readyAnswers as $val)
                          <option value="{{ $val->answer }}">{{ $val->question }}</option>
                        @empty
                        @endforelse
                      </select>
                      <br />
                    <div class="input-group">
                      <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                      
                      <textarea type="text" name="message" placeholder="Mesaj yazın..." id="message-text" class="form-control" rows="3"></textarea>
                      <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning btn-flat" style="height:75px">Gönder</button>
                          </span>
                    </div>
                  {!! Form::close() !!}
                </div>
                <!-- /.box-footer-->
                  <div class="direct-chat-messages">
                    <!-- Message. Default to the left -->

                    @forelse($messages as $key => $val)
                      @if($val->owner == 1)
                        <div class="direct-chat-msg">
                          <div class="direct-chat-info clearfix">
                            <span class="direct-chat-timestamp pull-right">{{ Carbon\Carbon::parse($val->date)->format('d M - H:i') }}</span>
                          </div>
                          <!-- /.direct-chat-info -->
                          <div class="direct-chat-text">
                            {{ $val->message }} <span style="float:right"><i class="fa fa-check"></i>@if($val->is_viewed == 1)<i class="fa fa-check" style="margin-left:-7px"></i>@endif</span>
                          </div>
                          <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->
                      @else
                        <!-- Message to the right -->
                        <div class="direct-chat-msg right">
                          <div class="direct-chat-info clearfix">
                            <span class="direct-chat-timestamp pull-left">{{ Carbon\Carbon::parse($val->date)->format('d M H:i') }}</span>
                          </div>
                          <!-- /.direct-chat-info -->
                          <div class="direct-chat-text" style="background-color:#f39c12; background:#f39c12; color:#fff">
                            {{ $val->message }} <span style="float:right"><i class="fa fa-check"></i>@if($val->is_viewed == 1)<i class="fa fa-check" style="margin-left:-7px"></i>@endif
                          </div>
                          <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->
                      @endif
                    @empty

                    @endforelse
                    

                  </div>
                  <!--/.direct-chat-messages-->
                
              <!--/.direct-chat -->







              </div>
              <!-- /.tab-pane -->


              <div class="tab-pane" id="notification">
                <!-- /.box-header -->
                  <!-- Conversations are loaded here -->
                  <!-- /.box-body -->
                <div class="box-footer">
                  {!! Form::open(['url'=>'admin/user-management/send-notification', 'method'=>'post', 'autocomplete' => 'off'])  !!}
                    <div class="input-group">
                      <input type="text" name="firebase" value="{{ $user->firebase }}" class="form-control" disabled>
                      <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                      <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                      <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning btn-flat">Gönder</button>
                          </span>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
              <!-- /.tab-pane -->


              <div class="@if(!isset($_GET['chat'])) active @endif tab-pane" id="activity">
                {!! Form::open(['url'=>'admin/user-management/user', 'method'=>'post', 'autocomplete' => 'off'])  !!}
              <div class="box-body">
                <div class="row">
                    <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                    <!--<div class="col-md-6">
                        <div class="form-group">
                          <label>İsim Soyisim</label>
                          <input type="text" class="form-control" id="inputName" placeholder="İsim Soyisim" name="first_name" value="{{ $user->first_name }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>TC No.</label>
                          <input type="text" class="form-control" id="inputName" placeholder="TC No." name="tc_number" value="{{ $user->tc_number }}" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Doğum Tarihi</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Doğum Tarihi" name="birthday" value="{{ Carbon\Carbon::parse($user->birthday)->format('d.m.Y') }}" disabled>
                        </div>
                    </div>-->
                    <!--<div class="col-md-6">
                        <div class="form-group">
                          <label>Yeni Şifre</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Yeni Şifre" name="new_password">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>E-Posta Adresi</label>
                          <input type="text" class="form-control" id="inputName" placeholder="E-Posta Adresi" name="email_2" value="{{ $user->email_2 }}">
                        </div>
                    </div>-->
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Telefon</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Telefon" name="phone" value="{{ $user->phone }}">
                        </div>
                    </div>
                    <!--<div class="col-md-6">
                        <div class="form-group">
                          <label>Telefon 2</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Telefon 2" name="phone_2" value="{{ $user->phone_2 }}">
                        </div>
                    </div>-->
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Bilet</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Bilet" name="ticket" value="{{ $user->ticket }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Bakiye</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Bakiye" name="balance" value="{{ $user->balance }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>İşletim Sistemi</label>
                          <input type="text" class="form-control" id="inputName" placeholder="İşletim Sistemi" name="os" value="{{ $user->os }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label><strong>WİNNER</strong></label>
                          <select class="form-control" name="is_winner" required>
                            <option value="0" @if($user->is_winner == 0) selected @endif>Hayır</option>
                            <option value="1" @if($user->is_winner == 1) selected @endif>Evet</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Durum</label>
                          <select class="form-control" name="blocked" required>
                            <option value="0" @if($user->blocked == 0) selected @endif>Aktif</option>
                            <option value="1" @if($user->blocked == 1) selected @endif>Engelli</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Destek Durumu</label>
                          <select class="form-control" name="support" required>
                            <option value="0" @if($user->support == 0) selected @endif>Kapalı</option>
                            <option value="1" @if($user->support == 1) selected @endif>Açık</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Engellenme Nedeni</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Engellenme Nedeni" name="blocked_reason" value="{{ $user->blocked_reason }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Doğrulama Denemesi</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Doğrulama Denemesi" name="verification_tries" value="{{ $user->verification_tries }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Onaylı Üye</label>
                          <select class="form-control" name="verification" required>
                            <option value="0" @if($user->verification == 0) selected @endif>Onaysız</option>
                            <option value="1" @if($user->verification == 1) selected @endif>Onaylı</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Dil</label>
                          <select class="form-control" name="lang" required>
                            <option value="en" @if($user->verification == 0 || $user->lang == "en") selected @endif>İngilizce</option>
                            <option value="tr" @if($user->lang == "tr") selected @endif>Türkçe</option>
                            <option value="it" @if($user->lang == "it") selected @endif>İtalyanca</option>
                            <option value="fr" @if($user->lang == "fr") selected @endif>Fransızca</option>
                            <option value="de" @if($user->lang == "de") selected @endif>Almanca</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Para Birimi</label>
                          <select class="form-control" name="currency" required>
                            <option value="eur" @if($user->verification == 0 || $user->country_code != "90") selected @endif>Euro</option>
                            <option value="try" @if($user->country_code == "90") selected @endif>Lira</option>
                          </select>
                        </div>
                    </div>
                    <!--<div class="col-md-6">
                        <div class="form-group">
                          <label>Satın Alma</label>
                          <select class="form-control" name="purchasing" required>
                            <option value="0" @if($user->purchasing == 0) selected @endif>Kapalı</option>
                            <option value="1" @if($user->purchasing == 1) selected @endif>Açık</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Video İzleme Sıfırla</label>
                          <select class="form-control" name="clear_rewardeds" required>
                            <option value="0">Hayır</option>
                            <option value="1">Evet</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Versiyon</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Version" name="app_version" value="{{ $user->app_version }}">
                        </div>
                    </div>-->
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>OneSignal</label>
                          <input type="text" class="form-control" id="inputName" placeholder="OneSignal" name="firebase" value="{{ $user->firebase }}">
                        </div>
                    </div>
                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Kaydet</button>
              </div>
            {!! Form::close() !!}
              </div>

              <div class="tab-pane" id="jokers">
              {!! Form::open(['url'=>'admin/user-management/user-jokers', 'method'=>'post', 'autocomplete' => 'off'])  !!}
              <div class="box-body">
                <div class="row">
                    <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Çifte Kazanç</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Çifte Kazanç" name="double_earnings" value="{{ $jokers->double_earnings }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Pas</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Pas" name="pass" value="{{ $jokers->pass }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Kartı Göster</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Kartı Göster" name="show_result" value="{{ $jokers->show_result }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Devam Et</label>
                          <input type="text" class="form-control" id="inputName" placeholder="Devam Et" name="continue_left" value="{{ $jokers->continue_left }}">
                        </div>
                    </div>
                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Kaydet</button>
              </div>
            {!! Form::close() !!}
              </div>

              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                <table class="table table-striped">
                  <tr>
                      <th>ID</th>
                      <th>Tur</th>
                      <th>Coin</th>
                      <th>Durum</th>
                      <th>Tarih</th>
                  </tr>
                  @forelse($games as $key => $val)
                  <tr>
                      <td>{{ $val->id }}</td>
                      <td>{{ $val->lap }}</td>
                      <td>{{ $val->coins }}</td>
                      <td>@if($val->is_completed == 1) @if($val->coins > 0) <i class="fa fa-check" style="color: green"></i> @else <i class="fa fa-times" style="color: red"></i> @endif @else <i class="fa fa-spinner fa-pulse" style="color: gray"></i> @endif</td>
                      <td>{{ Carbon\Carbon::parse($val->createdAt)->format('d M Y - H:i:s') }}</td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç kutu açmadı!</td>
                  </tr>
                  @endforelse
                </table>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <table class="table table-striped">
                  <tr>
                      <th>İşlem Türü</th>
                      <th>Tutar</th>
                      <th>Durum</th>
                      <th>İşlem Tarihi</th>
                  </tr>
                  @forelse($transactions as $key => $val)
                  <tr>
                      <td>@if($val->type == 1) Para Çekme @else Para Yatırma @endif</td>
                      <td>@if($user->currency == "eur")€@endif{{ number_format($val->amount, 2, ',', '.') }} @if($user->currency == "try")₺@endif</td>
                      <td>@if($val->status == 0)<em class="color-yellow-dark">Bekliyor <i class="fa fa-circle"></i> </em>@elseif($val->status == 1)<em class="color-green-dark">Onaylandı <i class="fa fa-circle"></i> </em>@elseif($val->status == 2)<em class="color-orange-dark">Reddedildi <i class="fa fa-circle"></i> </em>@else<em class="color-blue-dark">Hatalı <i class="fa fa-circle"></i> </em>@endif</td>
                      <td>İşlem Tarihi: {{ Carbon\Carbon::parse($val->created_date)->format('H:i d M') }} @if($val->confirm_date != null) <br />Onay Tarihi: {{ Carbon\Carbon::parse($val->confirm_date)->format('H:i d M') }}@endif</td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç işlem yok!</td>
                  </tr>
                  @endforelse
                </table>
              </div>

              <div class="tab-pane" id="references">
                <table class="table table-striped">
                  <tr>
                      <th>Referans ID</th>
                      <th>İsim Soyisim</th>
                      <th>Telefon Numarası</th>
                      <th>Durum</th>
                      <th>Engel</th>
                      <th>İşlem Tarihi</th>
                  </tr>
                  @forelse($references as $key => $val)
                  <tr>
                      <td><a href="{{ url('admin/user-management/user', $val->id) }}">{{ $val->id }}</a></td>
                      <td>{{ $val->username }}</td>
                      <td>{{ $val->phone }}</td>
                      <td>@if($val->verification == 0)<i class="fa fa-times" style="color:red"></i>@elseif($val->verification == 1)<i class="fa fa-check" style="color:green"></i>@endif</td>
                      <td>@if($val->blocked == 1)<i class="fa fa-times" style="color:red"></i>@else<i class="fa fa-check" style="color:green"></i>@endif</td>
                      <td>{{ Carbon\Carbon::parse($val->createdAt)->format('d M Y - H:i:s')}}</td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="6" style="text-align:center">Hiç referans yok!</td>
                  </tr>
                  @endforelse
                </table>
              </div>

              <div class="tab-pane" id="rewardeds">
                <table class="table table-striped">
                  <tr>
                      <th>Tıklama Tarihi</th>
                      <th>Tamamlama Tarihi</th>
                  </tr>
                  @forelse($rewardeds as $key => $val)
                  <tr>
                      <td>{{ Carbon\Carbon::parse($val->click_date)->format('d M Y - H:i:s')}}</td>
                      <td>{{ Carbon\Carbon::parse($val->complete_date)->format('d M Y - H:i:s')}}</td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç izleme yok!</td>
                  </tr>
                  @endforelse
                </table>
              </div>

              <div class="tab-pane" id="bonuses">
                <table class="table table-striped">
                  <tr>
                      <th>Bonus Miktarı</th>
                      <th>Nedeni</th>
                      <th>Tarih</th>
                  </tr>
                  @forelse($bonuses as $key => $val)
                  <tr>
                      <td>{{ $val->bonus_amount }}</td>
                      <td>{{ $val->reason }}</td>
                      <td>{{ Carbon\Carbon::parse($val->date)->format('d/m/Y H:i:s')}}</td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç izleme yok!</td>
                  </tr>
                  @endforelse
                </table>
              </div>

              
              <div class="tab-pane" id="offerwall">
                <table class="table table-striped">
                  <tr>
                      <th>Transaction ID</th>
                      <th>Kazanılan Bilet</th>
                      <th>Kâr ($)</th>
                      <th>Görev Başlığı</th>
                      <th>Tarih</th>
                  </tr>
                  @forelse($offerwall as $key => $val)
                  <tr>
                      <td>{{ $val->tx_id }}</td>
                      <td>{{ $val->point_value }}</td>
                      <td>{{ $val->usd_value }}</td>
                      <td>{{ $val->offer_title }}</td>
                      <td>{{ Carbon\Carbon::parse($val->complete_date)->format('d M Y - H:i:s')}}</td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" style="text-align:center">Hiç görev yok!</td>
                  </tr>
                  @endforelse
                </table>
              </div>

              <div class="tab-pane" id="otheraccounts">
                <table class="table table-striped">
                  <tr>
                      <th>User ID</th>
                      <th>Kullanıcı Adı</th>
                      <th>Üyelik Tarihi</th>
                  </tr>
                  @forelse($otherAccounts as $key => $val)
                  <tr>
                      <td><a href="{{ url('admin/user-management/user', $val->id) }}">{{ $val->id }}</a></td>
                      <td>{{ $val->first_name }}</td>
                      <td>{{ Carbon\Carbon::parse($val->createdAt)->format('d/m/Y H:i:s')}}</td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" style="text-align:center">Başka bir üyeliği yok!</td>
                  </tr>
                  @endforelse
                </table>
              </div>

              <div class="tab-pane" id="locations">
                <table class="table table-striped">
                  <tr>
                      <th>IP Address</th>
                      <th>Ülke</th>
                      <th>Bölge Adı</th>
                      <th>Şehir</th>
                      <th>Tarih</th>
                  </tr>
                  @forelse($userLocations as $key => $val)
                  <tr>
                      <td>{{ $val->ip }}</td>
                      <td>{{ $val->country_name }}</td>
                      <td>{{ $val->region_name }}</td>
                      <td>{{ $val->city }}</td>
                      <td>{{ Carbon\Carbon::parse($val->checkDate)->format('d/m/Y H:i:s')}}</td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" style="text-align:center">Konum bilgisi yok!</td>
                  </tr>
                  @endforelse
                </table>
              </div>

              <div class="tab-pane" id="recovers">
                <table class="table table-striped">
                  <tr>
                      <th>Tarih</th>
                  </tr>
                  @forelse($recovers as $key => $val)
                  <tr>
                      <td>{{ Carbon\Carbon::parse($val->created_at)->format('d/m/Y H:i:s')}}</td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="1" style="text-align:center">Şifre sıfırlama bilgisi yok!</td>
                  </tr>
                  @endforelse
                </table>
              </div>

              <div class="tab-pane" id="sendticket">
                {!! Form::open(['url'=>'admin/user-management/send-ticket', 'method'=>'post', 'autocomplete' => 'off'])  !!}
                  <div class="box-body">
                    <div class="row">
                        <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Bilet Miktarı</label>
                              <textarea type="text" class="form-control" id="inputName" placeholder="Bilet Miktarı" name="tickets"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Veriliş Nedeni</label>
                              <textarea type="text" class="form-control" id="inputName" placeholder="Veriliş Nedeni" name="reason"></textarea>
                            </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Gönder</button>
                  </div>
                {!! Form::close() !!}
              </div>

              <div class="tab-pane" id="premiummembership">
                {!! Form::open(['url'=>'admin/user-management/premium-membership', 'method'=>'post', 'autocomplete' => 'off'])  !!}
                  <div class="box-body">
                    <div class="row">
                        <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Üyelik Türünü Seçin</label>
                              <select class="form-control" name="membership" required>
                                <option value="bronze_tr">Bronze (TR)</option>
                                <option value="bronze_tr">Bronze (EN)</option>
                                <option value="silver_tr">Silver (TR)</option>
                                <option value="silver_en">Silver (EN)</option>
                                <option value="gold_tr">Gold (TR)</option>
                                <option value="gold_en">Gold (EN)</option>
                                <option value="platinum_tr">Platinum (TR)</option>
                                <option value="platinum_en">Platinum (EN)</option>
                                <option value="vip_tr">VIP(TR)</option>
                                <option value="vip_en">VIP(EN)</option>
                              </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Transaction ID</label>
                              <input type="text" class="form-control" id="inputName" placeholder="GPA-..." name="trans_id">
                            </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Gönder</button>
                  </div>
                {!! Form::close() !!}
              </div>

              <div class="tab-pane" id="payment">
                {!! Form::open(['url'=>'admin/user-management/payment', 'method'=>'post', 'autocomplete' => 'off'])  !!}
                  <div class="box-body">
                    <div class="row">
                        <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Ödeme Tutarı</label>
                              <input type="number" class="form-control" id="inputName" placeholder="Ödeme Tutarı" name="amount" value="50">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Ödeme Türü</label>
                              <input type="text" class="form-control" id="inputName" placeholder="Papara, Turkcell, Vodafone, Migros..." name="payment_type" maxlength="50">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Ödeme Notu</label>
                              <input type="text" class="form-control" id="inputName" placeholder="Ödeme bilgilerini girin..." name="payment_notes">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Üyelik Durumu</label>
                              <select class="form-control" name="payment_status" required>
                                <option value="0">Ödeme Bekliyor</option>
                                <option value="1">Ödeme Yapıldı</option>
                              </select>
                            </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Gönder</button>
                  </div>
                {!! Form::close() !!}
              </div>


              <div class="tab-pane" id="notes">

                {!! Form::open(['url'=>'admin/user-management/new-note', 'method'=>'post', 'autocomplete' => 'off'])  !!}
                  <div class="box-body">
                    <div class="row">
                        <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label>Not</label>
                              <input type="text" class="form-control" id="inputName" placeholder="Bir not girin..." name="note">
                            </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Gönder</button>
                  </div>
                {!! Form::close() !!}


                <table class="table table-striped">
                  <tr>
                      <th>Yönetici</th>
                      <th>Not</th>
                      <th>Tarih</th>
                  </tr>
                  @forelse($notes as $key => $val)
                  <tr>
                      <td>{{ $val->admin_id }}</td>
                      <td>{{ $val->note }}</td>
                      <td>{{ Carbon\Carbon::parse($val->created_at)->format('d/m/Y H:i:s')}}</td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="3" style="text-align:center">Hiç not yok!</td>
                  </tr>
                  @endforelse
                </table>
              </div>

              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
@section('scripts')
  <script>
    $("#drafts").change(function() {
      $("#message-text").val($(this).val());
    });
    $("#message-text").keypress(function (e) {
    if(e.which == 13 && !e.shiftKey) {        
        $(this).closest("form").submit();
        e.preventDefault();
        return false;
    }
});
  </script>
  @include('sweet::alert')
@endsection