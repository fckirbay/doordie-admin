@extends('layouts.cardgame.main')
@section('styles')

@endsection
@section('content')
         <div class="page-content-black"></div>
         <div class="page-content">
            <div class="cover-wrapper cover-no-buttons">
               <div data-height="cover" class="caption bottom-0">
                  <div class="caption-center">
                     <h1 class="center-text"><i class="fa fa-exclamation-triangle color-red2-dark fa-3x bottom-20"></i></h1>
                     <h1 class="color-white center-text fa-5x uppercase bolder bottom-10 top-20">ERROR</h1>
                     <h2 class="color-white center-text uppercase bolder bottom-30 font-15">404 - Page not found. Woops!</h2>
                     <p class="boxed-text-large">
                        Üzgünüz, aradığınız sayfayı bulamadık. Biz onu araken siz anasayfaya dönmek ister misiniz?
                     </p>
                     <div class="left-30 right-30">
                        <div class="one-half">
                           <a href="{{ url('/') }}" class="back-button left-10 right-10 button button-s bg-highlight button-round-medium button-full shadow-large">ANASAYFA</a>
                        </div>
                        <div class="one-half last-column">
                           <a href="{{ url('support') }}" class="back-button left-10 right-10 button button-s bg-highlight button-round-medium button-full shadow-large">DESTEK</a>
                        </div>
                        <div class="clear"></div>
                     </div>
                  </div>
                  <div class="caption-overlay bg-black opacity-80"></div>
                  <div class="caption-bg" style="background-image:url(assets/cardgame/images/pictures/18t.jpg)"></div>
               </div>
            </div>
         </div>
@endsection
@section('scripts')

@endsection