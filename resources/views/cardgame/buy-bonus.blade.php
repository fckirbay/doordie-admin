@extends('layouts.cardgame.main')
@section('styles')

@endsection
@section('content')
			<div class="page-content header-clear-medium">
					<div class="content bottom-5">
						<div class="link-list link-list-2 link-list-long-border">
							<a href="{{ url('buy-bonus-extra', 1) }}">
								<i class="fa fa-heart color-red2-dark"></i>
								<span>@lang('general.double_earnings')</span>
								<em class="bg-blue2-dark">4 @lang('general.coin')<br /></em>
								<strong>Tur sonundaki kazançlarınızı ikiye katlar.</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
						<div class="link-list link-list-2 link-list-long-border">
							<a href="{{ url('buy-bonus-extra', 2) }}">
								<i class="fa fa-heart color-red2-dark"></i>
								<span>@lang('general.pass')</span>
								<em class="bg-blue2-dark">2 @lang('general.coin')<br /></em>
								<strong>Oyun sırasında bir kez pas hakkı tanır.</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
						<div class="link-list link-list-2 link-list-long-border">
							<a href="{{ url('buy-bonus-extra', 3) }}">
								<i class="fa fa-heart color-red2-dark"></i>
								<span>@lang('general.show_result')</span>
								<em class="bg-blue2-dark">3 @lang('general.coin')<br /></em>
								<strong>Bir turdaki kapalı kartı görmenizi sağlar.</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
						<div class="link-list link-list-2 link-list-long-border">
							<a href="{{ url('buy-bonus-extra', 4) }}" class="no-border">
								<i class="fa fa-heart color-red2-dark"></i>
								<span>@lang('general.continue_left')</span>
								<em class="bg-blue2-dark">4 @lang('general.coin')</em>
								<strong>Bir önceki turda kaldığınız yerden başlatır.</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
			</div>
@endsection
@section('scripts')
  
@endsection