@extends('layouts.cardgame.main')
@section('styles')
<style>
	.page-bg {
		background-color: rgba(249,249,249,.98) !important;
	}
</style>
@endsection
@section('content')
<div class="snackbars snackbars-boxed snackbar-round">
	<a href="#" class="bg-blue2-dark" id="snackbar-manual-1"><i class="fa fa-sync fa-spin"></i>Just a second...</a>
</div>
<div class="page-content header-clear-medium" style="padding-top: 50px; box-shadow: 0 0px 0px 0 rgba(0,0,0,.11)">
	<div id="footer-menu" class="footer-menu-5-icons footer-menu-style-1" style="margin-top: 0px; position: inherit">
		<div class="one-half" style="width: 29%; margin-left: 3%;">
			<h1 class="center-text bolder top-20">{{ $game->id }}</h1>
			<span class="center-text under-heading color-highlight bottom-20">Oyun No.</span>
		</div>
		<div class="one-half" style="width: 29%; margin-left: 3%;">
			<h1 class="center-text bolder top-20">{{ $game->lap }}</h1>
			<span class="center-text under-heading color-highlight bottom-20">Tur</span>
		</div>
		<div class="one-half last-column" style="width: 29%">
			<h1 class="center-text bolder top-20">{{ $game->coins }}</h1>
			<span class="center-text under-heading color-highlight bottom-20">Coin</span>
		</div>
	</div>
	<div class="one-half" style="width: 45%; margin-left: 3%; margin-top: 20px">
		<img data-src="{{ asset('assets/cardgame/images/cards/'.$lap->card_1.'.png') }}" src="{{ asset('assets/cardsgame/images/empty.png') }}" class="round-tiny preload-image responsive-image" alt="img">
	</div>
	<div class="one-half last-column" style="width: 45%; margin-top: 20px">
		@if($lap->card_2 == null)
			<img data-src="{{ asset('assets/cardgame/images/cards/empty.png') }}" src="{{ asset('assets/cardsgame/images/empty.png') }}" class="round-tiny preload-image responsive-image" alt="img">
		@else
			<img data-src="{{ asset('assets/cardgame/images/cards/'.$lap->card_2.'.png') }}" src="{{ asset('assets/cardsgame/images/empty.png') }}" class="round-tiny preload-image responsive-image" alt="img">
		@endif
	</div>
	
	<div class="one-half" style="width: 45%; margin-left: 3%; margin-top: 15px">
		<a href="#" @if(!$jokers || ($jokers && $jokers->pass == 0)) data-snack-id="snack-1" data-snack-color="bg-red2-dark" data-snack-text="Pas geçmek için jokeriniz bulunmamaktadır!" data-snack-icon="fa fa-times" @else data-menu="pass-confirm" @endif class="button button-xs shadow-small button-round-small bg-teal-dark" style="width: 100%; text-align: center"><i class="fas fa-times"></i> Pas (@if($jokers){{ $jokers->pass }}@else{{0}}@endif)</a>
	</div>
	<div class="one-half last-column" style="width: 45%; margin-top: 15px">
		<a href="#" @if(!$jokers || ($jokers && $jokers->show_result == 0)) href="#" data-snack-id="snack-2" data-snack-color="bg-red2-dark" data-snack-text="Kartı görmek için jokeriniz bulunmamaktadır!" data-snack-icon="fa fa-times" @elseif($lap->card_2 != null) href="#" data-snack-id="snack-2" data-snack-color="bg-red2-dark" data-snack-text="Bu jokeri zaten kullandınız!" data-snack-icon="fa fa-times" @else data-menu="show-result-confirm" @endif class="button button-xs shadow-small button-round-small bg-teal-dark" style="width: 100%; text-align: center"><i class="fas fa-search"></i> Kartı Göster  (@if($jokers){{ $jokers->show_result }}@else{{0}}@endif)</a>
	</div>
	<div class="one-half" style="width: 45%; margin-left: 3%; margin-top: -10px">
		<a href="#" class="button button-xs shadow-small button-round-small bg-dark1-dark" style="width: 100%; text-align: center"><i class="fas fa-angle-double-down"></i> Küçüktür</a>
	</div>
	<div class="one-half last-column" style="width: 45%; margin-top: -10px">
		<a href="#" class="button button-xs shadow-small button-round-small bg-dark1-dark" style="width: 100%; text-align: center"><i class="fas fa-angle-double-up"></i> Büyüktür</a>
	</div>
	@if($errors->any())
	<h4>{{$errors->first()}}</h4>
	@endif
</div>
<div id="pass-confirm" class="menu menu-box-bottom menu-box-detached round-medium" data-menu-height="200" data-menu-effect="menu-over">
	<h1 class="center-text ultrabold top-20">Emin misiniz?</h1>
	<p class="boxed-text-large">
		Bu tur için pas geçme hakkınızı kullanıyorsunuz.<br>Kartınız değiştirilerek oyuna devam edilecek.
	</p>
	<div class="content left-50 right-50">
		<div class="one-half">
			{!! Form::open(['url'=>'play-pass', 'method'=>'post'])  !!}
				<input type="submit" class="button button-center-large button-s shadow-large button-round-small bg-green1-dark" value="Evet"/>
			{!! Form::close() !!}
		</div>
		<div class="one-half last-column">
			<input class="close-menu button button-center-large button-s shadow-large button-round-small bg-red1-dark" value="Hayır"/>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div id="show-result-confirm" class="menu menu-box-bottom menu-box-detached round-medium" data-menu-height="200" data-menu-effect="menu-over">
	<h1 class="center-text ultrabold top-20">Emin misiniz?</h1>
	<p class="boxed-text-large">
		Bu tur için kartı görme jokerinizi kullanacaksınız ve kapalı olan kart açılacaktır.
	</p>
	<div class="content left-50 right-50">
		<div class="one-half">
			{!! Form::open(['url'=>'play-show-result', 'method'=>'post'])  !!}
				<input type="submit" class="button button-center-large button-s shadow-large button-round-small bg-green1-dark" value="Evet"/>
			{!! Form::close() !!}
		</div>
		<div class="one-half last-column">
			<input class="close-menu button button-center-large button-s shadow-large button-round-small bg-red1-dark" value="Hayır"/>
		</div>
		<div class="clear"></div>
	</div>
</div>
@endsection
@section('scripts')
@endsection