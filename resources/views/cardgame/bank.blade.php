@extends('layouts.cardgame.main')
@section('styles')
@endsection
@section('content')
			<div class="page-content header-clear-medium" style="padding-top: 50px">
				<div class="content" style="margin: 0px 0px 0px">
					<div class="pricing-4 round-medium shadow-small" style="max-width: 1920px; border-radius: 0px !important; box-shadow: 0 0 0 0 rgba(0,0,0,0) !important;">
						<h3 class="pricing-value center-text bg-blue2-light bottom-10 color-white">{{ explode('.',Sentinel::getUser()->balance)[0] }}<sup>.{{ explode('.',Sentinel::getUser()->balance)[1] }}</sup></h3>
						<h2 class="pricing-subtitle center-text bg-blue2-light" style="margin-bottom: 0px; margin-top: -11px">@lang('general.your_total_points')</h2>
						<h1 class="pricing-title center-text bg-blue2-dark uppercase">@lang('general.your_jokers')</h1>
						<ul class="pricing-list bottom-30">
							<li style="font-weight: 500; float: left; width: 96%">@lang('general.double_earnings') <span style="float: right; font-weight: bold"><em class="@if($jokers && $jokers->double_chance) bg-green2-dark @else bg-red2-dark @endif" style="font-size: 15px;
	    font-weight: 700;
	    border-radius: 3px;
	    top: 47px;
	    line-height: 23px;
	    text-align: center;
	    font-style: normal;
	    color: #fff;
	    padding: 0 8px;">@if($jokers){{ $jokers->double_earnings }}@else 0 @endif</em></span></li>

							<li style="font-weight: 500; float: left; width: 96%">@lang('general.pass') <span style="float: right; font-weight: bold"><em class="@if($jokers && $jokers->pass) bg-green2-dark @else bg-red2-dark @endif" style="font-size: 15px;
	    font-weight: 700;
	    border-radius: 3px;
	    top: 47px;
	    line-height: 23px;
	    text-align: center;
	    font-style: normal;
	    color: #fff;
	    padding: 0 8px;">@if($jokers){{ $jokers->pass }}@else 0 @endif</em></span></li>

							<li style="font-weight: 500; float: left; width: 96%">@lang('general.show_result') <span style="float: right; font-weight: bold"><em class="@if($jokers && $jokers->show_result) bg-green2-dark @else bg-red2-dark @endif" style="font-size: 15px;
	    font-weight: 700;
	    border-radius: 3px;
	    top: 47px;
	    line-height: 23px;
	    text-align: center;
	    font-style: normal;
	    color: #fff;
	    padding: 0 8px;">@if($jokers){{ $jokers->show_result }}@else 0 @endif</em></span></li>

							<li style="font-weight: 500; float: left; width: 96%">@lang('general.continue_left') <span style="float: right; font-weight: bold"><em class="@if($jokers && $jokers->continue_left) bg-green2-dark @else bg-red2-dark @endif" style="font-size: 15px;
	    font-weight: 700;
	    border-radius: 3px;
	    top: 47px;
	    line-height: 23px;
	    text-align: center;
	    font-style: normal;
	    color: #fff;
	    padding: 0 8px;">@if($jokers){{ $jokers->continue_left }}@else 0 @endif</em></span></li>
						</ul>
						<a href="{{ url('buy-bonus') }}" class="button button-xs bg-blue2-dark button-center-large button-round-large uppercase">@lang('general.buy_joker')</a>
						<span class="center-text color-gray-dark small-text font-10 uppercase top-10 bottom-0">@lang('general.25_off_all_jokers_now')</span>
					</div>
					<div class="clear"></div>
				</div>
			</div>
@endsection
@section('scripts')
  
@endsection