@extends('layouts.cardgame.main')
@section('styles')
@endsection
@section('content')
			<div class="page-content header-clear-medium" style="padding-top: 50px">
				<div data-height="175" class="caption caption-margins round-medium shadow-large" style="margin: 0 0px 3px; border-radius: 0px !important">
					<div class="caption-center text-center">
						<span class="text-center color-blue1-dark"><i class="fa fa-video fa-3x"></i></span>
						<h1 class="color-white bolder">@lang('general.watch_win')</h1>
						<p class="under-heading color-white opacity-80 bottom-0">
							@lang('general.earn_extra_rights_by_watching_award_winning_videos')
						</p>
					</div>
					<div class="caption-overlay bg-black opacity-80"></div>
					<div class="caption-bg bg-20"></div>
				</div>

				<div data-height="175" class="caption caption-margins round-medium shadow-large" style="margin: 0 0px 3px; border-radius: 0px !important">
					<div class="caption-center text-center">
						<span class="text-center color-blue1-dark"><i class="fa fa-thumbtack fa-3x"></i></span>
						<h1 class="color-white bolder">@lang('general.complete_mission')</h1>
						<p class="under-heading color-white opacity-80 bottom-0">
							@lang('general.earn_extra_rights_by_completing_award_winning_quests')
						</p>
					</div>
					<div class="caption-overlay bg-black opacity-80"></div>
					<div class="caption-bg bg-20"></div>
				</div>

				<div data-height="175" class="caption caption-margins round-medium shadow-large" style="margin: 0 0px 0px; border-radius: 0px !important">
					<div class="caption-center text-center">
						<span class="text-center color-blue1-dark"><i class="fa fa-award fa-3x"></i></span>
						<h1 class="color-white bolder">@lang('general.bonus')</h1>
						<p class="under-heading color-white opacity-80 bottom-0">
							@lang('general.earn_extra_rights_by_collecting_special_bonuses')
						</p>
					</div>
					<div class="caption-overlay bg-black opacity-80"></div>
					<div class="caption-bg bg-20"></div>
				</div>
			</div>
@endsection
@section('scripts')
@endsection