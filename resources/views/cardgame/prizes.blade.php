@extends('layouts.cardgame.main')
@section('styles')
@endsection
@section('content')
	<div class="page-content header-clear-medium">
		<br />
		@forelse($paymentMethods as $key => $val)
		<a href="{{ url('prize', $val->url) }}">
			<div class="content-boxed content-boxed-full bottom-20">
				<img data-src="{{ asset($val->photo) }}" src="{{ asset($val->photo) }}" class="preload-image responsive-image shadow-small bottom-15" alt="img">
				<div class="content" style="text-align: center">
					<p style="font-weight: bold; font-size: 20px; margin-bottom: 10px">{{ $val->name }}</p>
					<p style="margin-bottom: 0px; font-size: 16px">{{ $val->coin }} @lang('general.coin')</p>
				</div>
			</div>
		</a>
		@empty
		@endforelse
	</div>
@endsection
@section('scripts')
@endsection