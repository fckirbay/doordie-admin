<?php

return [

    'your_jokers' => 'JOKERLERİNİZ',
    'your_total_points' => 'Toplam kazandığınız puan',
    'double_earnings' => 'İki kat kazanç',
    'pass' => 'Pas',
    'show_result' => 'Sonuç göster',
    'continue_left' => 'Kaldığın yerden devam',
    'buy_joker' => 'Joker Satın Al',
    '25_off_all_jokers_now' => 'Şimdi tüm Jokerler %25 indirimli',
    'lost' => 'Kaybetti',
    'won' => 'Kazandı',
    'lap' => 'Tur',
    'coin' => 'Coin',
    'login' => 'GİRİŞ',
    'if_you_are_a_member_login' => 'Eğer üye iseniz, giriş yapın!',
    'username' => 'Kullanıcı Adı',
    'password' => 'Password',
    'create_an_account' => 'Hesap oluştur',
    'forgot_password' => 'Şifremi Unuttum',
    'sign_in' => 'Giriş Yap',
    'sign_in_with_facebook' => 'Facebook ile Giriş Yap',
    'sign_in_with_twitter' => 'Twitter ile Giriş Yap',
    'my_account' => 'Hesabım',
    'you_can_edit_your_account_information' => 'Güncel fırsatlardan faydalanabilmek için hesap bilgilerinizi buradan düzenleyebilirsiniz.',
    'english' => 'İngilizce',
    'german' => 'Almanca',
    'turkish' => 'Türkçe',
    'email' => 'E-Posta',
    'phone' => 'Telefon',
    'again' => 'Tekrar',
    'save' => 'Kaydet',
    'watch_win' => 'İzle Kazan',
    'complete_mission' => 'Görev Yap',
    'bonus' => 'Bonus',
    'earn_extra_rights_by_watching_award_winning_videos' => 'Ödüllü videolar izleyerek ekstra hak kazanın.',
    'earn_extra_rights_by_completing_award_winning_quests' => 'Ödüllü görevleri tamamlayarak ekstra hak kazanın.',
    'earn_extra_rights_by_collecting_special_bonuses' => 'Özel bonusları toplayarak ekstra hak kazanın.',
    'home' => 'Anasayfa',
    'safe' => 'Kasa',
    'new_game' => 'Oyna',
    'opportunities' => 'Fırsatlar',
    'prizes' => 'Ödüller',
    'cardgame' => 'CardGame',
    'game_history' => 'Oyun Geçmişi',
    'references' => 'Referanslar',
    'support' => 'Destek',
    'logout' => 'Çıkış',
    'send' => 'Gönder',
    'your_balance_is_insufficient' => 'Bu işlem için bakiyeniz yetersiz!',
    'sorry' => 'Üzgünüz',
    'sign_up_for_free' => 'Ücretsiz Kaydolun',
    'play_completely_free' => 'Tamamen ücretsiz oynayın',
    'get_free_access_to_all_content' => 'Uygulama içerisindeki tüm içeriklere ücretsiz erişin ve her gün onlarca kez şansınızı deneyin.',
    'try_your_chance' => 'Şansınızı Deneyin',
    'earn_extra_rights_by_performing_daily_tasks' => 'Günlük görevleri yaparak oyun hakkı kazanın',
    'you_can_win_up_to_5000_coins_in_a_single_game' => 'Sadece tahminlerinizi yaparak ve şansınıza güvenerek, tek oyunda 5.000 Coin\'e kadar kazanma şansı yakalayın.',
    'higher_or_lower' => 'Büyük mü, küçük mü?',
    'all_you_have_to_do_is_guess' => 'Tek yapacağınız tahmin etmek',
    'collect_coins_and_play_your_coins_as_we_know_from_our_childhood' => 'Çocukluğumuzdan bildiğimiz büyüktür küçüktür oyununu oynayarak Coin toplayın ve Coin\'lerinizi ödüle çevirin.',
    'win_a_prize' => 'Ödül Kazanın',
    'get_your_prize_instantly' => 'İstediğiniz ödülü anında alın',
    'with_the_coins_you_have_collected_in_the_game' => 'Oyun içerisinde biriktirdiğiniz Coin\'lerle istediğiniz ödülü seçin ve 24 saat içinde size gönderelim.'



    







];
